# Package

version       = "0.1.0"
author        = "J0eCool"
description   = "Shiv programming language"
license       = "MIT"
srcDir        = "src"
bin           = @["shivc"]
binDir        = "bin"


# Dependencies

requires "nim >= 0.19.0"
