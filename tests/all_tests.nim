import
  macros,
  os

proc filesInDirWithExtension(dir, ext: string): seq[string] =
  result = @[]
  for path in os.walkDir(dir):
    if path.kind == pcFile:
      let split = os.splitFile(path.path)
      if split.ext == ext:
        result.add path.path

macro importAllTests(folderPath: string): untyped =
  result = newNimNode(nnkStmtList)
  const testPrefix = "test_"
  for f in filesInDirWithExtension(folderPath.strVal, ".nim"):
    let split = os.splitFile(f)
    if split.name[0..<testPrefix.len] == testPrefix:
      result.add newTree(nnkImportStmt, ident(split.name & split.ext))

importAllTests("tests")
