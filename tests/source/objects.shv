; Copyright 2019 Google LLC
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     https://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

include stdlib

let foo = object {
  one: 1
  two: '2
  three: "fish"
}

print foo.one " : " foo.two " : " foo.three

var bar = object {
  a: 7
  c: 9
}

print (hasKey bar "a") " : " (hasKey bar "b")
print bar.a " : " bar.c

set bar "b" 8
; TODO: support deleting items from dict?
; del bar "a"
set bar "c" 12

print (hasKey bar "a") " : " (hasKey bar "b")
print bar.b " : " bar.c

;{ EXPECTED
1 : 2 : fish
true : false
7 : 9
true : true
8 : 12
;}
