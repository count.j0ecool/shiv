import
  unittest,

  sexpr_parser

func a(name: string): Sexpr =
  Sexpr(kind: sAtom, name: name)
func a(x: Sexpr): Sexpr =
  x
func s(elems: varargs[Sexpr, a]): Sexpr =
  Sexpr(kind: sList, elems: @elems)

proc sexp(input: string): seq[Sexpr] =
  parseSexprs(input)
suite "Sexpr Parser":
  test "ToString":
    check:
      $(a("foo")) == "foo"
      $(s("foo", "bar")) == "(foo bar)"
      $(s("foo", s("bar", "baz"), "feef")) == "(foo (bar baz) feef)"
      $(a("foo bar")) == "`foo bar`"
      $(a("\"foo bar\"")) == "\"foo bar\""
      $(s()) == "()"
      $(s(s(), s())) == "(() ())"
      $(a("'f")) == "'f"
      $(a("foo\nbar")) == "foo\\nbar"
      $(a("foo\\bar")) == "foo\\bar"

  test "Single atoms":
    check:
      sexp("foo") == @[s("foo")]
      sexp("123") == @[s("123")]

  test "String literals":
    check:
      sexp("\"foo\"") == @[s("\"foo\"")]
      sexp("\"foo bar\"") == @[s("\"foo bar\"")]
      sexp("\"\"") == @[s("\"\"")]
      sexp("\"\\\"foo\\\"\"") == @[(s("\"\"foo\"\""))]
      sexp("\"foo\\nbar\"") == @[(s("\"foo\nbar\""))]
      sexp("\"foo\\\\bar\"") == @[(s("\"foo\\bar\""))]

  test "Char literals":
    check:
      sexp("'a") == @[s("'a")]
      sexp("'a 'b") == @[s("'a", "'b")]
      sexp("'a'b'c") == @[s("'a", "'b", "'c")]
      sexp("'\\n") == @[s("'\n")]
      sexp("foo'a") == @[s("foo", "'a")]

  test "Flat lists":
    check:
      sexp("()") == @[s()]
      sexp("(foo)") == @[s("foo")]
      sexp("(foo bar)") == @[s("foo", "bar")]
      sexp("(foo bar baz)") == @[s("foo", "bar", "baz")]

  test "Whitespace":
    check:
      sexp("( foo bar )") == @[s("foo", "bar")]
      sexp("(\n  foo\n  bar\n)") == @[s("foo", "bar")]

  test "Nested lists":
    check:
      sexp("( foo (bar seek) baz )") == @[s(
        "foo", s("bar", "seek"), "baz")]


suite "Flat-sexpr Parser":
  test "Single line":
    check:
      sexp("foo") == @[s("foo")]
      sexp("foo bar baz") == @[s("foo", "bar", "baz")]
      sexp("  foo  ") == @[s("foo")]

  test "Newlines":
    check:
      sexp("foo\nbar") == @[s("foo"), s("bar")]
      sexp("foo\nbar\n") == @[s("foo"), s("bar")]
      sexp("foo bar\nbar bar baz\n") == @[
        s("foo", "bar"),
        s("bar", "bar", "baz"),
      ]
      sexp("\nfoo\n\n\nbar\n") == @[s("foo"), s("bar")]

  test "Sub-sexprs":
    check:
      sexp("foo (bar baz)") == @[s("foo", s("bar", "baz"))]
      sexp("(foo bar) baz") == @[s(s("foo", "bar"), "baz")]
      sexp("foo (bar\n  baz)") == @[s("foo", s("bar", "baz"))]
      sexp("foo\n(bar baz)") == @[s("foo"), s("bar", "baz")]
      sexp("(foo bar)\nbaz") == @[s("foo", "bar"), s("baz")]
      sexp("foo(bar)") == @[s("foo", s("bar"))]

  test "Sub-flats":
    check:
      sexp("{}") == @[s()]
      sexp("{foo bar}") == @[s(s("foo", "bar"))]
      sexp("{\nfoo bar\n}") == @[s(s("foo", "bar"))]
      sexp("{\nfoo\nbar\n}") == @[s(s("foo"), s("bar"))]
      sexp("{\n  foo\n  bar\n}") == @[s(s("foo"), s("bar"))]
      sexp("foo{bar}") == @[s("foo", s(s("bar")))]

  test "If stmt":
    check:
      sexp("""
        if (x > 5) {
          print x
          print "hi"
        }
        if (x < 10) {
          print x
        } else {
          print "Too big"
        }
      """) == @[
        s("if", s("x", ">", "5"), s(
          s("print", "x"),
          s("print", "\"hi\""),
        )),
        s("if", s("x", "<", "10"), s(
          s("print", "x"),
        ), "else", s(
          s("print", "\"Too big\""),
        ))
      ]

  test "Empty if-clauses":
    check:
      sexp("if (foo) {} else { print bar }") == @[
        s("if", s("foo"), s(), "else", s(s("print", "bar")))]
      sexp("if (foo) {} else { }") == @[
        s("if", s("foo"), s(), "else", s())]
      sexp("if (foo) () else ()") == @[
        s("if", s("foo"), s(), "else", s())]
      sexp("foo () (bar baz)\n") == @[s("foo", s(), s("bar", "baz"))]
      sexp("foo {} {bar baz}\n") == @[s("foo", s(), s(s("bar", "baz")))]
      sexp("foo () ()\n") == @[s("foo", s(), s())]
      sexp("fee {\nfoo {} {\nbar baz\n}}\n") == @[s("fee", s(s("foo", s(), s(s("bar", "baz")))))]

  test "Array literals":
    check:
      sexp("[array literal]") == @[s("__list", "array", "literal")]
      sexp("[foo [bar baz]]") == @[s("__list", "foo", s("__list", "bar", "baz"))]
      sexp("[]") == @[s("__list")]
      sexp("[[foo bar]]") == @[s("__list", s("__list", "foo", "bar"))]

  test "Array indexing":
    check:
      sexp("foo[idx]") == @[s("__index", "foo", "idx")]
      sexp("foo[bar[baz]]") == @[s("__index", "foo", s("__index", "bar", "baz"))]
      sexp("foo[bar baz]") == @[s("__index", "foo", s("bar", "baz"))]
      sexp("foo[]") == @[s("__index", "foo", s())]
      sexp("foo[1][2]") == @[s("__index", s("__index", "foo", "1"), "2")]
      sexp("foo[[x]]") == @[s("__index", "foo", s("__list", "x"))]
      sexp("(foo)[2]") == @[s("__index", s("foo"), "2")]
      sexp("\"foo\"[2]") == @[s("__index", "\"foo\"", "2")]

  test "Split atoms on symbols":
    check:
      sexp("foo + bar") == @[s("foo", "+", "bar")]
      sexp("foo+bar") == @[s("foo", "+", "bar")]
      sexp("foo +bar") == @[s("foo", "+", "bar")]
      sexp("a.b") == @[s("a", ".", "b")]

      # Multiple symbols are one atom
      sexp("foo->bar") == @[s("foo", "->", "bar")]
      sexp("<=~~*") == @[s("<=~~*")]

  test "Negative numeric literals":
    check:
      sexp("-1") == @[s("-1")]
      sexp("-10") == @[s("-10")]
      sexp("-x") == @[s("-", "x")]
      sexp("2-1") == @[s("2", "-", "1")]
      sexp("a-10") == @[s("a", "-", "10")]
      sexp("f -3") == @[s("f", "-3")]
      sexp("[1 2 -3 4 -5]") == @[s("__list", "1", "2", "-3", "4", "-5")]
