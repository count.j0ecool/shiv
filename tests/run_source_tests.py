# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import difflib
import filecmp
import os
import subprocess
import sys

TEST_DIR = os.path.dirname(os.path.abspath(__file__))
SHIV_DIR = os.path.dirname(TEST_DIR)
BIN_DIR = os.path.join(SHIV_DIR, 'bin')
SRC_DIR = os.path.join(TEST_DIR, 'source')

CANONICAL_SHIVC = os.path.join(SHIV_DIR, 'shivc.js')
SHIVC = os.path.join(BIN_DIR, 'shivc.js')
SHIVC_SRC = 'src/shivc.shv'

def run(cmd):
  try:
    return subprocess.check_output(cmd, universal_newlines=True)
  except subprocess.CalledProcessError as e:
    print(e.output)
    raise

num_tests = 0
num_fail = 0
num_err = 0
expected_str = ';{ EXPECTED\n'

bootstrap_initial = os.path.join(BIN_DIR, 'bootstrap_shivc_initial.js')
bootstrap_test = os.path.join(BIN_DIR, 'bootstrap_shivc_test.js')

print("Building...")
sys.stdout.flush()
bootstrap_initial_contents = run(['node', CANONICAL_SHIVC, SHIVC_SRC])
with open(bootstrap_initial, 'w') as f:
  f.write(bootstrap_initial_contents)

print("Can rebuild shivc...")
sys.stdout.flush()
shivc_contents = run(['node', bootstrap_initial, SHIVC_SRC])
with open(SHIVC, 'w') as f:
  f.write(shivc_contents)
print("Rebuilt shivc can build shivc...")
sys.stdout.flush()
bootstrap_test_contents = run(['node', SHIVC, SHIVC_SRC])
with open(bootstrap_test, 'w') as f:
  f.write(bootstrap_test_contents)
assert filecmp.cmp(SHIVC, bootstrap_test), 'Bootstrap does not converge'

for filename in os.listdir(SRC_DIR):
  if not filename.endswith('.shv'):
    continue
  path = os.path.join(SRC_DIR, filename)

  contents = open(path).read()
  if expected_str not in contents:
    continue

  num_tests += 1
  expected = contents.split(expected_str)[1].split(';}')[0].strip()
  print("Testing:", filename)
  sys.stdout.flush()
  try:
    actual = run(['node', SHIVC, path]).strip()
  except Exception as e:
    num_err += 1
    print('ERROR:', filename)
    sys.stdout.flush()
    continue

  if actual != expected:
    num_fail += 1
    print('FAIL:', filename)

    diff = difflib.unified_diff(
      expected.split('\n'),
      actual.split('\n'),
      fromfile='expected',
      tofile='actual')
    print(''.join([a.rstrip() + '\n' for a in diff]))

  sys.stdout.flush()

print('\n-- Finished running tests --')
print(num_tests, 'tests')
print(num_fail, 'failues')
print(num_err, 'errors')
if num_fail == num_err == 0:
  print('SUCCESS')
