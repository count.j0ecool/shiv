import unittest

import
  util

suite "Utils":
  test "toString":
    check:
      @['a', 'b', 'c'].toString() == "abc"
      @['a', 's', 'd', 'f'].toString() == "asdf"
      @[].toString() == ""
