Some translations

Standard Shiv
```shiv
a * b + c * d
```

Canonical Shiv
```shiv
(+ (* a b) (* c d))
```


Primordial Shiv
```shiv
(register _r1 = (* a b))
(register _r2 = (* c d))
(register _r3 = (+ _r1 _r2))
```

JS Codegen
```js
var _r1 = (a * b);
var _r2 = (c * d);
var _r3 = (_r1 + _r2);
```

Wasm Codegen
```wasm
(local.set $r1 ((* (local.get $a) (local.get $b))))
(local.set $r2 ((* (local.get $c) (local.get $d))))
(local.set $r3 ((+ (local.get $r1) (local.get $r2))))
```

LLVM
```llvm
%r1 = mul i32 %a, %b
%r2 = mul i32 %c, %d
%r3 = add i32 %r1, %r2
```

x86
```x86
mov esi, eax
imul esi, ebx
mov edi, ecx
imul edi, edx
add esi, edi
```
