rem Workaround for copying files being really hard on windows I guess
rmdir "%APPDATA%\Sublime Text 3\Packages\User\Shiv" /S /Q
mkdir "%APPDATA%\Sublime Text 3\Packages\User\Shiv"
copy Shiv.sublime-syntax "%APPDATA%\Sublime Text 3\Packages\User\Shiv\Shiv.sublime-syntax"
copy Comments.tmPreferences "%APPDATA%\Sublime Text 3\Packages\User\Shiv\Comments.tmPreferences"
