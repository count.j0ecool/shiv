// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SHIV_STDLIB_H__
#define __SHIV_STDLIB_H__

#include <assert.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef int Int;
typedef float Float;
typedef unsigned char Char;
typedef void Void;

Int _Sv_copy_Int_1VarInt(Int* x) {
  return *x;
}
Int _Sv_A2B_Int_2IntInt(Int lhs, Int rhs) {
  return lhs + rhs;
}
Int _Sv_A2D_Int_2IntInt(Int lhs, Int rhs) {
  return lhs - rhs;
}
Int _Sv_A2A_Int_2IntInt(Int lhs, Int rhs) {
  return lhs * rhs;
}
Int _Sv_A2F_Int_2IntInt(Int lhs, Int rhs) {
  return lhs / rhs;
}
Int _Sv_A3C_Int_2IntInt(Int lhs, Int rhs) {
  return lhs < rhs;
}
Int _Sv_A3CA3D_Int_2IntInt(Int lhs, Int rhs) {
  return lhs <= rhs;
}
Int _Sv_A3DA3D_Int_2IntInt(Int lhs, Int rhs) {
  return lhs == rhs;
}
Int _Sv_A3EA3D_Int_2IntInt(Int lhs, Int rhs) {
  return lhs >= rhs;
}
Int _Sv_A3E_Int_2IntInt(Int lhs, Int rhs) {
  return lhs > rhs;
}
Int _Sv_A2D_Int_1Int(Int x) {
  return -x;
}
Int _Sv_mod_Int_2IntInt(Int lhs, Int rhs) {
  if (lhs < 0) { lhs += INT_MAX; }
  return lhs % rhs;
}
Int _Sv_A26_Int_2IntInt(Int lhs, Int rhs) {
  return lhs & rhs;
}
Int _Sv_A7C_Int_2IntInt(Int lhs, Int rhs) {
  return lhs | rhs;
}
Int _Sv_A3CA3C_Int_2IntInt(Int lhs, Int rhs) {
  return lhs << rhs;
}
Int _Sv_A3EA3E_Int_2IntInt(Int lhs, Int rhs) {
  return lhs >> rhs;
}

Int _Sv_and_Int_2IntInt(Int lhs, Int rhs) {
  return lhs && rhs;
}
Int _Sv_or_Int_2IntInt(Int lhs, Int rhs) {
  return lhs || rhs;
}
Int _Sv_not_Int_1Int(Int x) {
  return !x;
}

Float _Sv_copy_Float_1VarFloat(Float* x) {
  return *x;
}
Float _Sv_A2B_Float_2FloatFloat(Float lhs, Float rhs) {
  return lhs + rhs;
}
Float _Sv_A2D_Float_2FloatFloat(Float lhs, Float rhs) {
  return lhs - rhs;
}
Float _Sv_A2A_Float_2FloatFloat(Float lhs, Float rhs) {
  return lhs * rhs;
}
Float _Sv_A2F_Float_2FloatFloat(Float lhs, Float rhs) {
  return lhs / rhs;
}
Int _Sv_A3C_Int_2FloatFloat(Float lhs, Float rhs) {
  return lhs < rhs;
}
Int _Sv_A3CA3D_Int_2FloatFloat(Float lhs, Float rhs) {
  return lhs <= rhs;
}
Int _Sv_A3DA3D_Int_2FloatFloat(Float lhs, Float rhs) {
  return lhs == rhs;
}
Int _Sv_A3EA3D_Int_2FloatFloat(Float lhs, Float rhs) {
  return lhs >= rhs;
}
Int _Sv_A3E_Int_2FloatFloat(Float lhs, Float rhs) {
  return lhs > rhs;
}
Float _Sv_A2D_Float_1Float(Float x) {
  return -x;
}
Float _Sv_toFloat_Float_1Int(Int x) {
  return (Float)x;
}
Float _Sv_toFloat_Float_1Char(Char x) {
  return (Float)x;
}
Int _Sv_toInt_Int_1Float(Float x) {
  return (Int)x;
}
Float _Sv_sin_Float_1Float(Float x) {
  return sin(x);
}
Float _Sv_cos_Float_1Float(Float x) {
  return cos(x);
}
Float _Sv_sqrt_Float_1Float(Float x) {
  return sqrt(x);
}

Float _Sv_rand_Float_0() {
  return rand() / (float)RAND_MAX;
}
Void _Sv_srand_Void_0() {
  srand(time(NULL));
}
Void _Sv_srand_Void_1Int(Int seed) {
  srand(seed);
}

typedef struct String_t {
  char* buffer;
  int len;
  bool didAlloc;
} String;

String _makeString(char* literal) {
  return (String){literal, strlen(literal), false};
}
Void _Sv_dealloc_Void_1String(String str) {
  if (str.didAlloc) {
    free(str.buffer);
    str.buffer = NULL;
  }
}

String _Sv_copy_String_1VarString(String* str) {
  String ret;
  ret.buffer = malloc(str->len + 1);
  strcpy(ret.buffer, str->buffer);
  ret.len = str->len;
  ret.didAlloc = true;
  return ret;
}

Char _Sv_copy_Char_1VarChar(Char* c) {
  return *c;
}
Char _Sv_toChar_Char_1Int(Int x) {
  return (Char)x;
}
Char _Sv_toChar_Char_1Float(Float x) {
  return (Char)x;
}
Int _Sv_toInt_Int_1Char(Char c) {
  return (Int)c;
}
Char _Sv_A2B_Char_2CharChar(Char lhs, Char rhs) {
  return lhs + rhs;
}

String _Sv_toString_String_1Int(Int x) {
  String str;
  str.buffer = calloc(16, sizeof(char));
  sprintf(str.buffer, "%d", x);
  str.len = strlen(str.buffer);
  str.didAlloc = true;
  return str;
}
String _Sv_toString_String_1Float(Float x) {
  String str;
  str.buffer = calloc(16, sizeof(char));
  sprintf(str.buffer, "%g", x);
  str.len = strlen(str.buffer);
  str.didAlloc = true;
  return str;
}
String _Sv_toString_String_1Char(Char x) {
  String str;
  str.buffer = calloc(8, sizeof(char));
  sprintf(str.buffer, "%c", x);
  str.len = strlen(str.buffer);
  str.didAlloc = true;
  return str;
}
String _Sv_toString_String_1String(String x) {
  String str;
  str.buffer = calloc(x.len + 1, sizeof(char));
  sprintf(str.buffer, "%s", x.buffer);
  str.len = strlen(str.buffer);
  str.didAlloc = true;
  return str;
}

String _Sv_A2B_String_2StringString(String lhs, String rhs) {
  String str;
  str.len = lhs.len + rhs.len;
  str.buffer = malloc(str.len + 1);
  sprintf(str.buffer, "%s", lhs.buffer);
  sprintf(str.buffer + lhs.len, "%s", rhs.buffer);
  str.didAlloc = true;
  return str;
}

Int _Sv_len_Int_1String(String str) {
  return str.len;
}

Char _Sv___index_Char_2StringInt(String str, Int idx) {
  assert(idx >= 0 && idx < str.len);
  return str.buffer[idx];
}

Void _Sv_putString_Void_1String(String str) {
  printf("%s", str.buffer);
}
Void _Sv_putLine_Void_0() {
  printf("\n");
  fflush(stdout);
}

#define _SV_DECL_LIST(Ty) \
  typedef struct List##Ty##_t { \
    Ty* buffer; \
    int capacity; \
    int len; \
  } List##Ty; \
\
  List##Ty _makeList##Ty(int capacity) { \
    List##Ty list; \
    list.buffer = malloc(capacity * sizeof(Ty)); \
    list.capacity = capacity; \
    list.len = 0; \
    return list; \
  } \
\
  Ty _Sv_copy_##Ty##_1Var##Ty(Ty*); \
  List##Ty _Sv_copy_List##Ty##_1VarList##Ty(List##Ty* list) { \
    List##Ty ret; \
    ret.capacity = list->len; \
    if (ret.capacity == 0) { \
      ret.capacity = 16; \
    } \
    ret.len = list->len; \
    ret.buffer = malloc(ret.capacity * sizeof(Ty)); \
    for (Int i = 0; i < list->len; ++i) { \
      ret.buffer[i] = _Sv_copy_##Ty##_1Var##Ty(&list->buffer[i]); \
    } \
    return ret; \
  } \
\
  Void _Sv_initList_Void_1VarList##Ty(List##Ty* list) { \
    *list = _makeList##Ty(16); \
  } \
  Void _Sv_dealloc_Void_1List##Ty(List##Ty list) { \
    if (list.buffer) { \
      free(list.buffer); \
    } \
  } \
\
  Int _Sv_len_Int_1List##Ty(List##Ty list) { \
    return list.len; \
  } \
  Int _Sv_len_Int_1VarList##Ty(List##Ty* list) { \
    return list->len; \
  } \
\
  Ty _Sv___index_##Ty##_2List##Ty##Int(List##Ty list, Int idx) { \
    assert(idx >= 0 && idx < list.len); \
    return list.buffer[idx]; \
  } \
  Ty* _Sv___index_Var##Ty##_2VarList##Ty##Int(List##Ty* list, Int idx) { \
    assert(idx >= 0 && idx < list->len); \
    return &list->buffer[idx]; \
  } \
\
  Void _Sv_add_Void_2VarList##Ty##Ty(List##Ty* list, Ty item) { \
    if (list->len + 1 > list->capacity) { \
      assert(list->capacity > 0); \
      list->capacity *= 2; \
      Ty* newBuffer = malloc(list->capacity * sizeof(Ty)); \
      memcpy(newBuffer, list->buffer, list->len * sizeof(Ty)); \
      free(list->buffer); \
      list->buffer = newBuffer; \
    } \
    list->buffer[list->len] = _Sv_copy_##Ty##_1Var##Ty(&item); \
    list->len++; \
  } \
  Void _Sv_delete_Void_2VarList##Ty##Int(List##Ty* list, Int idx) { \
    assert(idx >= 0 && idx < list->len); \
    list->len--; \
    for (int i = idx; i < list->len; ++i) { \
      list->buffer[i] = list->buffer[i + 1]; \
    } \
  } \
\
  String _Sv_toString_String_1List##Ty(List##Ty list) { \
    int num = list.len; \
    String str = _makeString("["); \
    String sep = _makeString(", "); \
    String temp; \
    for (int i = 0; i < list.len; ++i) { \
      if (i != 0) { \
        temp = _Sv_A2B_String_2StringString(str, sep); \
        free(str.buffer); \
        str = temp; \
      } \
      temp = _Sv_A2B_String_2StringString( \
        str, _Sv_toString_String_1##Ty(list.buffer[i])); \
      free(str.buffer); \
      str = temp; \
    } \
    temp = _Sv_A2B_String_2StringString(str, _makeString("]")); \
    free(str.buffer); \
    str = temp; \
    return str; \
  } \

_SV_DECL_LIST(Char)
_SV_DECL_LIST(Int)

#endif // __SHIV_STDLIB_H__
