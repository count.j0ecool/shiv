// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SHIV_SDL2_H__
#define __SHIV_SDL2_H__

#ifdef _SHIV_USE_SDL

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_ttf.h>

#include "shiv_stdlib.h"

typedef struct {
  SDL_Event event;
} SdlEvent;

SDL_Window* _Sv_globalWindow;

Void _Sv_sdlInit_Void_1String(String title) {
  SDL_Init(SDL_INIT_EVERYTHING);
  IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
  TTF_Init();

  _Sv_globalWindow = SDL_CreateWindow(title.buffer,
    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
    900, 900,
    SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
  SDL_GL_CreateContext(_Sv_globalWindow);

  // Init GL
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);

  glShadeModel(GL_SMOOTH);
  glClearColor(0.05, 0.05, 0.05, 0.5);
  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

Void _Sv_sdlQuit_Void_0() {
  SDL_DestroyWindow(_Sv_globalWindow);
  SDL_Quit();
  IMG_Quit();
  TTF_Quit();
}

String _Sv_toString_String_1SdlEvent(SdlEvent event) {
  return _makeString("{SdlEvent}");
}
SdlEvent _Sv_copy_SdlEvent_1VarSdlEvent(SdlEvent* event) {
  return *event;
}
_SV_DECL_LIST(SdlEvent)

ListSdlEvent _Sv_sdlPollEvents_ListSdlEvent_0() {
  ListSdlEvent ret = _makeListSdlEvent(4);
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    SdlEvent wrap = { event };
    _Sv_add_Void_2VarListSdlEventSdlEvent(&ret, wrap);
  }
  return ret;
}

#define _SV_SDL_EVENT_IS(Fn, Val) \
  Int _Sv_##Fn##_Int_1SdlEvent(SdlEvent event) { \
    if (event.event.type == Val) { \
      return 1; \
    } \
    return 0; \
  }
_SV_SDL_EVENT_IS(isQuit, SDL_QUIT)
_SV_SDL_EVENT_IS(isKeyDown, SDL_KEYDOWN)
_SV_SDL_EVENT_IS(isKeyUp, SDL_KEYUP)

Int _Sv_keycode_Int_1SdlEvent(SdlEvent event) {
  return event.event.key.keysym.sym;
}
Int _Sv_sdlKey_Int_1String(String name) {
  return SDL_GetKeyFromName(name.buffer);
}

typedef struct {
  SDL_Surface *surface;
} SdlSurface;

typedef struct {
  SDL_Rect rect;
} SdlRect;

SdlRect _Sv_sdlRect_SdlRect_4IntIntIntInt(Int x, Int y, Int w, Int h) {
  SdlRect result;
  result.rect.x = x;
  result.rect.y = y;
  result.rect.w = w;
  result.rect.h = h;
  return result;
}

SdlSurface _Sv_createRgbSurface_SdlSurface_2IntInt(Int width, Int height) {
  SdlSurface result;
  int flags = 0;
  int depth = 32;
  int rmask = 0xff000000;
  int gmask = 0x00ff0000;
  int bmask = 0x0000ff00;
  int amask = 0x000000ff;
  result.surface = SDL_CreateRGBSurface(
    flags, width, height, depth, rmask, gmask, bmask, amask);
  return result;
}
Void _Sv_freeSurface_Void_1SdlSurface(SdlSurface surface) {
  SDL_FreeSurface(surface.surface);
}

Void _Sv_blitSurface_Void_3SdlSurfaceSdlSurfaceSdlRect(
    SdlSurface src, SdlSurface dest, SdlRect clip) {
  SDL_BlitSurface(src.surface, NULL, dest.surface, &clip.rect);
}

Int _Sv_w_Int_1SdlSurface(SdlSurface surface) {
  return surface.surface->w;
}
Int _Sv_h_Int_1SdlSurface(SdlSurface surface) {
  return surface.surface->h;
}

typedef struct {
  TTF_Font *font;
} TtfFont;

typedef struct {
  SDL_Color color;
} SdlColor;

SdlColor _Sv_sdlColor_SdlColor_3IntIntInt(Int r, Int g, Int b) {
  SdlColor result;
  result.color.r = (unsigned char)r;
  result.color.g = (unsigned char)g;
  result.color.b = (unsigned char)b;
  result.color.a = 0xff;
  return result;
}

TtfFont _Sv_openFont_TtfFont_2StringInt(String filename, Int fontSize) {
  TtfFont result;
  result.font = TTF_OpenFont(filename.buffer, fontSize);
  if (!result.font) {
    printf("ERROR: Font %s could not be opened\n", filename.buffer);
    fflush(stdout);
    exit(1);
  }
  return result;
}
Void _Sv_closeFont_Void_1TtfFont(TtfFont font) {
  return;
}
SdlSurface _Sv_renderText_SdlSurface_3TtfFontStringSdlColor(
    TtfFont font, String text, SdlColor color) {
  SdlSurface result;
  result.surface = TTF_RenderText_Blended(font.font, text.buffer, color.color);
  return result;
}

Void _Sv_sdlDelay_Void_1Int(Int ms) {
  SDL_Delay(ms);
}

Void _Sv_sdlGlSwapWindow_Void_0() {
  SDL_GL_SwapWindow(_Sv_globalWindow);
}

Void _Sv_glClear_Void_0() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

Void _Sv_glBeginQuads_Void_0() {
  glBegin(GL_QUADS);
}
Void _Sv_glEnd_Void_0() {
  glEnd();
}

Void _Sv_glColor3f_Void_3FloatFloatFloat(Float r, Float g, Float b) {
  glColor3f(r, g, b);
}

Void _Sv_glVertex3f_Void_3FloatFloatFloat(Float x, Float y, Float z) {
  glVertex3f(x, y, z);
}

Void _Sv_glTexCoord2f_Void_2FloatFloat(Float u, Float v) {
  glTexCoord2f(u, v);
}

Void _Sv_glLoadIdentity_Void_0() {
  glLoadIdentity();
}

Void _Sv_glMatrixProjection_Void_0() {
  glMatrixMode(GL_PROJECTION);
}
Void _Sv_glMatrixModel_Void_0() {
  glMatrixMode(GL_MODELVIEW);
}

Void _Sv_glFrustum_Void_6FloatFloatFloatFloatFloatFloat(
    Float left, Float right, Float bottom, Float top, Float nearVal, Float farVal) {
  glFrustum(left, right, bottom, top, nearVal, farVal);
}

Void _Sv_glTranslate_Void_3FloatFloatFloat(Float x, Float y, Float z) {
  glTranslatef(x, y, z);
}
Void _Sv_glRotate_Void_4FloatFloatFloatFloat(Float deg, Float x, Float y, Float z) {
  glRotatef(deg, x, y, z);
}

Int _Sv_glCreateTexture_Int_0() {
  int texId;
  glGenTextures(1, &texId);
  return texId;
}

Void _Sv_glBindTexture_Void_1Int(Int texId) {
  glBindTexture(GL_TEXTURE_2D, texId);
}

Void _Sv_glTexImage_Void_2ListCharInt(ListChar pixels, Int width) {
  int height = pixels.len / width / 4;
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
    width, height, 0,
    GL_RGBA, GL_UNSIGNED_BYTE, pixels.buffer);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Void _Sv_glTexImage_Void_1SdlSurface(SdlSurface surface) {
  SDL_Surface *s = surface.surface;
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
    s->w, s->h, 0,
    GL_BGRA, GL_UNSIGNED_BYTE, s->pixels);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

#endif // _SHIV_USE_SDL
#endif // __SHIV_SDL2_H__
