# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sequtils
import strutils
import tables

import logger
import shiv_parser

let log = logger("COMPILE")

type
  Module* = object
    types*: seq[Type]
    funcs*: seq[Func]
    globals*: seq[VarDecl]
    # TODO: allow arbitrary global statements
    globalInits: seq[Stmt]

  Func* = object
    name*: string
    args*: seq[VarDecl]
    retType*: Type
    hasDefinition*: bool
    decls*: seq[VarDecl]
    body*: StmtList
    epilog*: StmtList

  TypeKind* = enum
    tyInfer
    tyVoid
    tyInt
    tyFloat
    tyChar
    tyString
    tyList
    tyVar
    tyStruct
    tyGeneric
  Type* = ref object
    case kind*: TypeKind
    of tyList:
      listTy*: Type
    of tyVar:
      varTy*: Type
    of tyStruct:
      stName*: string
      stFields*: seq[VarDecl]
      stExtern*: bool
      stIsDef*: bool
    of tyGeneric:
      genName*: string
    else:
      discard
  VarDecl* = object
    name*: string
    ty*: Type

  ExprKind* = enum
    exLiteral
    exVar
    exCall
  Expr* = object
    case kind*: ExprKind
    of exLiteral:
      litValue*: string
    of exVar:
      varName*: string
    of exCall:
      callFunc*: string
      callArgs*: ExprList
    ty*: Type
  ExprList* = seq[Expr]

  StmtKind* = enum
    stIf
    stReturn
    stVarDecl
    stLetDecl
    stAssign
    stLoop
    stBreak
    stExpr
  Stmt* = object
    case kind*: StmtKind
    of stIf:
      ifCond*: Expr
      ifTrue*: StmtList
    of stReturn:
      retExpr*: Expr
    of stVarDecl:
      varDecl*: VarDecl
    of stLetDecl:
      letDecl*: VarDecl
      letVal*: Expr
      letIntermediate*: bool
    of stAssign:
      asgLhs*: Expr
      asgVal*: Expr
    of stLoop:
      loopBody*: StmtList
    of stExpr:
      exprVal*: Expr
    of stBreak:
      discard
  StmtList* = seq[Stmt]

  # Internal helper type decls
  ExprCall = proc(ex: var Expr)
  StmtCall = proc(st: var Stmt): InsertStmts
  InsertStmts = object
    pre, post: seq[Stmt]
    removeSelf: bool

  Scope = Table[string, ptr Type]

# Shorter construction functions for types
func TInfer*(): Type = Type(kind: tyInfer)
func TVoid*(): Type = Type(kind: tyVoid)
func TInt*(): Type = Type(kind: tyInt)
func TFloat*(): Type = Type(kind: tyFloat)
func TChar*(): Type = Type(kind: tyChar)
func TString*(): Type = Type(kind: tyString)
func TList*(sub: Type): Type =
  Type(kind: tyList, listTy: sub)
func TVar*(sub: Type): Type =
  Type(kind: tyVar, varTy: sub)
func TStruct*(name: string, fields: seq[VarDecl] = @[]): Type =
  Type(kind: tyStruct, stName: name, stFields: fields)
func TGeneric*(name: string): Type =
  Type(kind: tyGeneric, genName: name)

func copy*(ty: Type): Type =
  result = Type(kind: ty.kind)
  case ty.kind
  of tyList:
    result.listTy = ty.listTy.copy()
  of tyVar:
    result.varTy = ty.varTy.copy()
  of tyStruct:
    result.stName = ty.stName
    result.stFields = ty.stFields
    result.stIsDef = false
  of tyGeneric:
    result.genName = ty.genName
  else:
    discard

func `==`(a, b: Type): bool =
  if a.kind != b.kind: return false
  case a.kind
  of tyList:
    a.listTy == b.listTy
  of tyVar:
    a.varTy == b.varTy
  of tyStruct:
    a.stName == b.stName
  else:
    true

const tabStr = "  "

func `$`*(ty: Type): string =
  case ty.kind
  of tyInfer: "Infer"
  of tyVoid: "Void"
  of tyInt: "Int"
  of tyFloat: "Float"
  of tyChar: "Char"
  of tyString: "String"
  of tyList:
    "(List " & $ty.listTy & ")"
  of tyVar:
    "(var " & $ty.varTy & ")"
  of tyStruct:
    ty.stName
  of tyGeneric:
    ty.genName

func `$`*(ex: Expr, indent = 0): string =
  case ex.kind:
  of exLiteral:
    result = tabStr.repeat(indent) & "Literal\n" &
      tabStr.repeat(indent + 1) & "Value: " & ex.litValue &
    "\n"
  of exVar:
    result = tabStr.repeat(indent) & "Var\n" &
      tabStr.repeat(indent + 1) & "Name: " & ex.varName &
    "\n"
  of exCall:
    result = tabStr.repeat(indent) & "Call\n" &
      tabStr.repeat(indent + 1) & "Func: " & ex.callFunc & "\n" &
      tabStr.repeat(indent + 1) & "Args:\n"
    for child in ex.callArgs:
      result &= `$`(child, indent + 2)
  result &= tabStr.repeat(indent + 1) & "Type: " & $ex.ty & "\n"

func `$`*(st: Stmt, indent = 0): string =
  case st.kind
  of stIf:
    result = tabStr.repeat(indent) & "If\n" &
      tabStr.repeat(indent + 1) & "Cond\n" & `$`(st.ifCond, indent + 2) &
      tabStr.repeat(indent + 1) & "IfTrue\n"
    for child in st.ifTrue:
      result &= `$`(child, indent + 2)
  of stReturn:
    result = tabStr.repeat(indent) & "Return:\n" & `$`(st.retExpr, indent + 1)
  of stVarDecl:
    result = tabStr.repeat(indent) & "Declaration\n" &
      tabStr.repeat(indent + 1) & "Var: " & $st.varDecl & "\n"
  of stLetDecl:
    result = tabStr.repeat(indent) & "Declaration\n" &
      tabStr.repeat(indent + 1) & "Let: " & $st.letDecl & "\n" &
      tabStr.repeat(indent + 1) & "Value:\n" & `$`(st.letVal, indent + 2)
  of stAssign:
    result = tabStr.repeat(indent) & "Assign\n" &
      tabStr.repeat(indent + 1) & "LHS:\n" & `$`(st.asgLhs, indent + 2) & "\n" &
      tabStr.repeat(indent + 1) & "Val:\n" & `$`(st.asgVal, indent + 2)
  of stLoop:
    result = tabStr.repeat(indent) & "Loop\n" &
      tabStr.repeat(indent + 1) & "Body\n"
    for child in st.loopBody:
      result &= `$`(child, indent + 2)
  of stBreak:
    result = tabStr.repeat(indent) & "Break\n"
  of stExpr:
    result = tabStr.repeat(indent) & "Expr:\n" &
      `$`(st.exprVal, indent + 1)

func `$`*(ast: Module): string =
  for fn in ast.funcs:
    result &= "Func\n" &
      tabStr & "Name: " & fn.name & "\n" &
      tabStr & "Args: " & $fn.args & "\n" &
      tabStr & "Returns: " & $fn.retType & "\n" &
      tabStr & "Body:\n"
    for st in fn.body:
      result &= `$`(st, indent=2)
  for i in 0..<ast.globals.len:
    result &= "Global: " & $ast.globals[i] & "\n" & 
      tabStr & "Init:\n" & `$`(ast.globalInits[i], indent=2)

func sigName*(fn: Func): string =
  fn.name & ": " & fn.args.mapIt($it.ty).join(" ") & " -> " & $fn.retType

func isVar(ty: Type): bool =
  ty.kind == tyVar
func isGeneric(ty: Type): bool =
  if ty.kind == tyInfer: return true
  if ty.kind == tyGeneric: return true
  if ty.kind == tyList and ty.listTy.isGeneric(): return true
  if ty.kind == tyVar and ty.varTy.isGeneric(): return true
  return false
func hasGenericArgs(fn: Func): bool =
  for arg in fn.args:
    if arg.ty.isGeneric():
      return true
  return false
func isGeneric(fn: Func): bool =
  if fn.retType.isGeneric(): return true
  return fn.hasGenericArgs()
func hasInfers(fn: Func): bool =
  if fn.retType.kind == tyInfer: return true
  for arg in fn.args:
    if arg.ty.kind == tyInfer:
      return true
  return false

func codegenFunc(sexpr: SExpr): Func

type StructFuncs* = object
  constructor*: Func
  copy*: Func
  toString*: Func
  accessors*: seq[Func]
  varAccessors*: seq[Func]
proc structFuncs*(name: string, fields: seq[VarDecl]): StructFuncs =
  let log = logger("strucFuncs: " & name)
  result.constructor = Func(
    name: name,
    args: fields,
    retType: TStruct(name),
  )
  result.toString = Func(
    name: "toString",
    args: @[VarDecl(name: "self", ty: TStruct(name))],
    retType: TString(),
  )
  for field in fields:
    result.accessors.add(Func(
      name: field.name,
      args: @[VarDecl(name: "self", ty: TStruct(name))],
      retType: field.ty.copy(),
    ))
    result.varAccessors.add(Func(
      name: field.name,
      args: @[VarDecl(name: "self", ty: TVar(TStruct(name)))],
      retType: TVar(field.ty.copy()),
    ))

  var copyText = "fn copy self:(var " & name & ") -> " & name &
    " {\n  return (" & name
  for field in fields:
    copyText &= " (" & field.name & " self)"
  copyText &= ")\n}"
  log "copy:\n", copyText
  let copyFunc = codegenFunc(parse(copyText).elems[0])
  result.copy = copyFunc

func all*(funcs: StructFuncs): seq[Func] =
  @[funcs.constructor, funcs.copy, funcs.toString] &
    funcs.accessors & funcs.varAccessors

func globalScope(module: var Module): Scope =
  for global in module.globals.mitems:
    result[global.name] = addr global.ty

func codegenType(sexpr: SExpr): Type =
  if sexpr.kind == sAtom:
    case sexpr.name
    of "Void": return TVoid()
    of "Int": return TInt()
    of "Float": return TFloat()
    of "Char": return TChar()
    of "String": return TString()
    if sexpr.name[0].isLowerAscii():
      return TGeneric(sexpr.name)
    if sexpr.name[0].isUpperAscii():
      return TStruct(sexpr.name)
  else:
    case sexpr.elems[0].name
    of "List":
      assert sexpr.elems.len == 2
      return TList(codegenType(sexpr.elems[1]))
    of "var":
      assert sexpr.elems.len == 2
      return TVar(codegenType(sexpr.elems[1]))
  assert false, "Unknown type: " & $sexpr

func codegenExpr(sexpr: SExpr): Expr =
  if sexpr.kind == sAtom:
    if sexpr.name[0] == '"':
      return Expr(kind: exLiteral, litValue: sexpr.name, ty: TString())
    elif sexpr.name[0].isDigit() or
        (sexpr.name.len > 1 and sexpr.name[0] == '-' and sexpr.name[1].isDigit()):
      let ty = if '.' in sexpr.name: TFloat() else: TInt()
      return Expr(kind: exLiteral, litValue: sexpr.name, ty: ty)
    else:
      return Expr(kind: exVar, varName: sexpr.name, ty: TInfer())
  else:
    let head = sexpr.elems[0]
    if head.kind == sAtom:
      result = Expr(kind: exCall, callFunc: head.name, ty: TInfer())
      for elem in sexpr.elems[1..<sexpr.elems.len]:
        result.callArgs.add(codegenExpr(elem))
      if head.name == "__list":
        let nargs = sa($result.callArgs.len)
        result.callArgs.insert(codegenExpr(nargs), 0)
      return
  assert false, "Unknown expr: " & $sexpr

func codegenStmtList(sexpr: SExpr): StmtList
func codegenStmt(sexpr: SExpr): Stmt =
  if sexpr.kind == sList:
    let head = sexpr.elems[0]
    if head.kind == sAtom:
      case head.name
      of "if":
        assert sexpr.elems.len >= 3
        result = Stmt(kind: stIf)
        result.ifCond = codegenExpr(sexpr.elems[1])
        result.ifTrue = codegenStmtList(sexpr.elems[2])
      of "return":
        assert sexpr.elems.len == 2
        result = Stmt(kind: stReturn)
        result.retExpr = codegenExpr(sexpr.elems[1])
      of "var":
        result = Stmt(kind: stVarDecl)
        if sexpr.elems.len == 4:
          assert sexpr.elems[2] == SExpr(kind: sAtom, name: ":")
          result.varDecl.ty = codegenType(sexpr.elems[3])
        else:
          assert sexpr.elems.len == 2
          result.varDecl.ty = TInfer()
        result.varDecl.name = sexpr.elems[1].name
      of "let":
        result = Stmt(kind: stLetDecl)
        if sexpr.elems.len == 6:
          assert sexpr.elems[2] == sa(":")
          result.letDecl.ty = codegenType(sexpr.elems[3])
          assert sexpr.elems[4] == sa("=")
          result.letVal = codegenExpr(sexpr.elems[5])
        else:
          assert sexpr.elems.len == 4
          assert sexpr.elems[2] == sa("=")
          result.letDecl.ty = TInfer()
          result.letVal = codegenExpr(sexpr.elems[3])
        result.letDecl.name = sexpr.elems[1].name
      of "<-":
        assert sexpr.elems.len == 3
        result = Stmt(kind: stAssign)
        result.asgLhs = codegenExpr(sexpr.elems[1])
        result.asgVal = codegenExpr(sexpr.elems[2])
      of "loop":
        assert sexpr.elems.len == 2
        result = Stmt(kind: stLoop)
        result.loopBody = codegenStmtList(sexpr.elems[1])
      of "break":
        assert sexpr.elems.len == 1
        result = Stmt(kind: stBreak)
      else:
        assert sexpr.elems.len > 0
        result = Stmt(kind: stExpr)
        result.exprVal = codegenExpr(sexpr)
      return
  assert false, "Unknown stmt: " & $sexpr

func codegenStmtList(sexpr: SExpr): StmtList =
  assert sexpr.kind == sList
  for elem in sexpr.elems:
    result.add(codegenStmt(elem))

func codegenFunc(sexpr: SExpr): Func =
  assert sexpr.kind == sList
  let elems = sexpr.elems
  result = Func(name: elems[1].name, retType: TInfer(), hasDefinition: true)
  var i = 2
  let lastIdx = elems.len - 1
  while i < lastIdx:
    assert elems[i].kind == sAtom
    if elems[i].name == "->":
      result.retType = codegenType(elems[i + 1]) 
      i += 2
      break
    else:
      let name = elems[i].name
      var ty: Type
      if elems[i + 1] != sa(":"):
        ty = TInfer()
        i += 1
      else:
        ty = codegenType(elems[i + 2])
        i += 3
      result.args.add(VarDecl(name: name, ty: ty))
  assert i == lastIdx
  result.body = codegenStmtList(elems[lastIdx])

proc codegenModule(sexpr: SExpr): Module =
  let log = logger("codegen")
  for topLevelStmt in sexpr.elems:
    if topLevelStmt.kind == sList:
      let elems = topLevelStmt.elems
      case elems[0].name
      of "fn":
        let fn = codegenFunc(topLevelStmt)
        result.funcs.add(fn)
      of "declare":
        var fn = Func(name: elems[1].name, hasDefinition: false)
        assert elems[elems.len - 2] == sa("->")
        for arg in elems[2..<elems.len-2]:
          let ty = codegenType(arg)
          fn.args.add(VarDecl(name: "_", ty: ty))
        fn.retType = codegenType(elems[elems.len - 1])
        assert not fn.hasInfers(), "Need fully declared type for " & $fn
        log "Function declaration: ", fn
        result.funcs.add(fn)
      of "type":
        assert elems.len >= 4
        var ty = TStruct(elems[1].name)
        assert elems[2] == sa("=")
        if elems[3] == sa("extern"):
          assert elems.len == 4
          ty.stExtern = true
          log "extern struct: ", ty
          result.types.add(ty)
        elif elems[3] == sa("struct"):
          assert elems.len == 5
          assert elems[4].kind == sList
          # var ty = ty
          for field in elems[4].elems:
            assert field.kind == sList
            assert field.elems.len == 3
            assert field.elems[0].kind == sAtom
            assert field.elems[1] == sa(":")
            let name = field.elems[0].name
            let fieldTy = codegenType(field.elems[2])
            assert not fieldTy.isGeneric()
            ty.stFields.add(VarDecl(name: name, ty: fieldTy))
          ty.stIsDef = true
          result.types.add(ty)
          log "struct type: ", ty
          let funcs = structFuncs(ty.stName, ty.stFields)
          for fn in all(funcs):
            log "  ", fn
            result.funcs.add(fn)
      of "var":
        # TODO: support global let-values
        let st = codegenStmt(topLevelStmt)
        assert st.kind == stVarDecl
        result.globals.add(st.varDecl)
      of "<-":
        let st = codegenStmt(topLevelStmt)
        result.globalInits.add(st)
  assert result.globals.len == result.globalInits.len
  for fn in result.funcs.mitems:
    if fn.name == "main":
      fn.body = result.globalInits & fn.body

proc applyToExpr(f: ExprCall, x: var Expr) =
  if f != nil:
    if x.kind == exCall:
      for ex in x.callArgs.mitems:
        applyToExpr(f, ex)
    f(x)
proc forAll(list: var StmtList,
    exprs: ExprCall = nil,
    stmts: StmtCall = nil) =
  var i = 0
  while i < list.len:
    template st: Stmt = list[i]
    case st.kind
    of stIf:
      applyToExpr(exprs, st.ifCond)
      forAll(st.ifTrue, exprs, stmts)
    of stReturn:
      applyToExpr(exprs, st.retExpr)
    of stAssign:
      applyToExpr(exprs, st.asgLhs)
      applyToExpr(exprs, st.asgVal)
    of stLoop:
      forAll(st.loopBody, exprs, stmts)
    of stExpr:
      applyToExpr(exprs, st.exprVal)
    of stLetDecl:
      applyToExpr(exprs, st.letVal)
    of stVarDecl, stBreak:
      discard
    if stmts != nil:
      let toAdd = stmts(st)
      if toAdd.pre.len + toAdd.post.len > 0 or toAdd.removeSelf:
        let self = if not toAdd.removeSelf: @[st] else: @[]
        list = list[0..<i] & toAdd.pre & self & toAdd.post & list[i+1..^1]
        i += toAdd.pre.len + toAdd.post.len
        if toAdd.removeSelf:
          i -= 1
    i += 1
proc forAll(st: var Stmt,
    exprs: ExprCall = nil) =
  var list = @[st]
  forAll(list, exprs, nil)
  assert list.len == 1
  st = list[0]

func isCompatible(fnArg, arg: Type): bool =
  fnArg == arg or
    (fnArg.isGeneric() and not (not arg.isVar and fnArg.isVar)) or
    (arg.isVar and fnArg.isCompatible(arg.varTy))
func isCompatible(fn: Func, args: seq[Type]): bool =
  if fn.args.len != args.len: return false
  for i in 0..<fn.args.len:
    if not fn.args[i].ty.isCompatible(args[i]):
      return false
  return true

# Creates a call to "copy" for an expression, to turn a var type into a nonvar
func makeCopy(ex: Expr): Expr =
  assert ex.ty.isVar
  Expr(
    kind: exCall,
    callFunc: "copy",
    callArgs: @[ex],
    ty: ex.ty.varTy,
  )
func makeDeref(ex: Expr): Expr =
  assert ex.ty.isVar
  Expr(
    kind: exCall,
    callFunc: "_deref",
    callArgs: @[ex],
    ty: ex.ty.varTy,
  )
func makeDealloc(ex: Expr): Expr =
  Expr(
    kind: exCall,
    callFunc: "dealloc",
    ty: TVoid(),
    callArgs: @[ex]
  )

proc typeCheckFunc(module: ptr Module, fnIdx: int)
proc findFunc(module: ptr Module, name: string, args: seq[Type]): Func =
  let log = logger("findFunc(" & name & ")")
  log "Finding func w/ args: ", args
  var possible: seq[Func]
  func matchScore(fn: Func): int =
    # Finds concrete functions first. If this generic already generated, prefer
    # it over all others.
    if args != fn.args.mapIt(it.ty):
      result += args.len
    for i in 0..<args.len:
      # penalize inserted copies
      if args[i].isVar and fn.args[i].ty.kind != tyVar:
        result += 1
  proc findBestMatch(): Func =
    if possible.len == 1: return possible[0]
    result = possible[0]
    var best = matchScore(result)
    log "Finding best match"
    log "  " & $best & ": " & result.sigName()
    for fn in possible[1..^1]:
      let score = matchScore(fn)
      log "  " & $score & ": " & fn.sigName()
      if score < best:
        result = fn
        best = score
    log "Best match: ", result

  for i in 0..<module[].funcs.len:
    var fn = module[].funcs[i]
    if fn.name == name and fn.isCompatible(args):
      if not fn.hasGenericArgs() and fn.retType.isGeneric():
        typeCheckFunc(module, i)
        fn = module[].funcs[i]
      log "Found possible func: ", fn.sigName
      possible.add(fn)
  if possible.len > 0:
    let fn = findBestMatch()
    if not fn.hasGenericArgs():
      return fn
    var ret = Func(name: fn.name, hasDefinition: fn.hasDefinition, retType: fn.retType.copy())
    var bound: Table[string, Type]
    proc unify(decl, arg: Type): Type =
      if not decl.isGeneric(): return decl.copy()
      log "unifying types: ", decl, " <- ", arg
      if decl.kind == tyList:
        if arg.isVar:
          let sub = arg.varTy
          assert sub.kind == tyList
          return TList(unify(decl.listTy, sub.listTy))
        else:
          assert arg.kind == tyList
          return TList(unify(decl.listTy, arg.listTy))
      elif decl.isVar:
        if arg.isVar:
          return TVar(unify(decl.varTy, arg.varTy))
        else:
          return TVar(unify(decl.varTy, arg))
      elif decl.kind == tyInfer:
        return arg.copy()
      elif decl.genName in bound:
        assert bound[decl.genName].isCompatible(arg)
      else:
        bound[decl.genName] = arg.copy()
      return bound[decl.genName].copy()
    proc bindRet(ret: var Type) =
      if not ret.isGeneric(): return
      if ret.kind == tyInfer: return
      log "binding return type: ", ret
      if ret.kind == tyList:
        bindRet(ret.listTy)
      elif ret.kind == tyVar:
        bindRet(ret.varTy)
      else:
        assert ret.genName in bound, "Unbound generic: " & ret.genName
        ret = bound[ret.genName].copy()

    for i in 0..<args.len:
      var decl = fn.args[i]
      decl.ty = unify(decl.ty, args[i])
      ret.args.add(decl)
    ret.retType.bindRet()
    log "Generated generic function: ", ret
    ret.body = fn.body
    let idx = module[].funcs.len
    module[].funcs.add(ret)
    if ret.hasDefinition:
      typeCheckFunc(module, idx)
    ret = module[].funcs[idx]
    assert not ret.isGeneric()
    return ret

  assert false, "Can't find function: " & name & " with args: " & $args

proc typeCheckFunc(module: ptr Module, fnIdx: int) =
  template fn: Func = module[].funcs[fnIdx]
  if not fn.hasDefinition or fn.hasGenericArgs(): return

  var scope = module[].globalScope()
  for decl in fn.args.mitems:
    scope[decl.name] = addr decl.ty

  let log = logger("typeCheckFunc : " & fn.name)
  log "Typechecking ", fn.sigName
  forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
    case st.kind
    of stVarDecl:
      if st.varDecl.ty.kind notin [tyInfer, tyVar]:
        st.varDecl.ty = TVar(st.varDecl.ty.copy())
      scope[st.varDecl.name] = addr st.varDecl.ty
      log "var decl: ", st.varDecl.name, ": ", st.varDecl.ty
    of stLetDecl:
      scope[st.letDecl.name] = addr st.letDecl.ty
      if st.letDecl.ty.kind != tyInfer:
        assert st.letDecl.ty == st.letVal.ty
      else:
        assert st.letVal.ty.kind != tyInfer
        if st.letVal.ty.isVar:
          st.letVal = makeCopy(st.letVal)
        st.letDecl.ty = st.letVal.ty.copy()
      log "let decl: ", st.letDecl.name, ": ", st.letDecl.ty
    of stAssign:
      if st.asgVal.ty.isVar:
        st.asgVal = makeCopy(st.asgVal)
      if st.asgLhs.ty.kind == tyInfer:
        st.asgLhs.ty = TVar(st.asgVal.ty.copy())
      if st.asgLhs.kind == exVar:
        let asgName = st.asgLhs.varName
        assert asgName in scope, "Assigning to unknown var: " & asgName
        if scope[asgName][].kind == tyInfer:
          scope[asgName][] = TVar(st.asgVal.ty.copy())
          log "assigned decl ", asgName, " inferred as ", st.asgVal.ty
    of stReturn:
      if st.retExpr.ty.isVar:
        if fn.retType.kind == tyInfer or st.retExpr.ty.varTy == fn.retType:
          st.retExpr = makeCopy(st.retExpr)
      if fn.retType.kind == tyInfer:
        fn.retType = st.retExpr.ty.copy()
        log "inferred return type as ", fn.retType
      else:
        assert fn.retType.isCompatible(st.retExpr.ty)
    of stIf:
      if st.ifCond.ty.isVar:
        st.ifCond = makeDeref(st.ifCond)
    else:
      discard
  , exprs=proc(ex: var Expr) =
    case ex.kind
    of exVar:
      if ex.ty.kind != tyInfer: return
      assert ex.varName in scope, "Unknown var: " & $ex
      ex.ty = scope[ex.varName][].copy()
      log "var ", ex.varName, " inferred as ", ex.ty
    of exCall:
      if ex.ty.kind != tyInfer: return
      log "inferring call ", ex.callFunc, ", initial type: ", ex.ty
      let fn = module.findFunc(ex.callFunc, ex.callArgs.mapIt(it.ty))
      if fn.retType.kind != tyGeneric:
        ex.ty = fn.retType.copy()
      else:
        for arg in fn.args:
          if arg.ty.kind == tyGeneric and arg.ty.genName == fn.retType.genName:
            ex.ty = arg.ty.copy()
            break
      for i in 0..<fn.args.len:
        if not fn.args[i].ty.isVar and ex.callArgs[i].ty.isVar:
          # FIXME: this isn't 100% safe
          # Can only pass by reference if no other argument references this one
          ex.callArgs[i] = makeDeref(ex.callArgs[i])
          log "arg ", i, " is var: ", ex.callArgs[i]
      log "  inferred as ", ex.ty
    else:
      discard
  )

  if fn.retType.kind == tyInfer:
    # If retType is still Infer, there are no return statements
    fn.retType = TVoid()
    log "inferred return type as ", fn.retType

proc typeInference(module: ptr Module) =
  # Variable expr inference. Assume one scope, essentially hoisting, for now
  var i = 0
  while i < module[].funcs.len:
    typeCheckFunc(module, i)
    i += 1

  # Hoist type declarations for used types
  proc addType(ty: Type) =
    if ty notin module[].types:
      if ty.kind == tyList:
        addType(ty.listTy)
      log "Adding type decl: ", ty
      module[].types.add(ty)
  for fn in module[].funcs.mitems:
    forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
      if st.kind == stVarDecl:
        addType(st.varDecl.ty)
    , exprs=proc(ex: var Expr) =
      addType(ex.ty)
    )

# Generic templates are already instantiated (or not), and are no longer needed
proc removeGenericFunctions(module: var Module) =
  var i = 0
  while i < module.funcs.len:
    let fn = module.funcs[i]
    if fn.isGeneric():
      module.funcs.delete(i)
    else:
      i += 1

# Turn AST to SSA for purposes of having access to temporary values
proc unfoldExpressions(module: var Module) =
  for fn in module.funcs.mitems:
    var n = 0
    forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
      # Statements with nested statements need to not be double-counted,
      # the outer forAll will handle them.
      if st.kind == stLoop: return
      var decls: seq[Stmt]
      proc unfoldExpr(ex: var Expr) =
        if ex.kind == exVar: return
        if ex.ty.kind == tyVoid: return
        assert ex.ty.kind != tyInfer, "Invalid infer type: " & $ex
        let tempName = "_t" & $n
        decls.add(Stmt(
          kind: stLetDecl,
          letDecl: VarDecl(name: tempName, ty: ex.ty.copy()),
          letVal: ex,
          letIntermediate: true,
        ))
        ex = Expr(
          kind: exVar,
          varName: tempName,
          ty: ex.ty.copy(),
        )
        n += 1
      if st.kind == stIf:
        # Ifs have nested statements, but also a condition expression
        applyToExpr(unfoldExpr, st.ifCond)
      else:
        forAll(st, exprs=unfoldExpr)
      result.pre = decls
    )

# Insert calls to initList after list declarations
proc initializeListVars(module: var Module) =
  let log = logger("initializeListVars")
  for fn in module.funcs.mitems:
    forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
      if st.kind != stVarDecl: return
      let ty = st.varDecl.ty
      if ty.isVar and ty.varTy.kind == tyList:
        log "Inserting initList: ", st
        result.post.add(Stmt(
          kind: stExpr,
          exprVal: Expr(
            kind: exCall,
            callFunc: "initList",
            ty: TVoid(),
            callArgs: @[Expr(
              kind: exVar,
              varName: st.varDecl.name,
              ty: ty.copy(),
            )],
          ),
        ))
    )

# Remove toString(stringVal) calls, e.g.:
#   (toString "foo") ==> "foo"
proc simplifyStringToString(module: var Module) =
  for fn in module.funcs.mitems:
    forAll(fn.body, exprs=proc(ex: var Expr) =
      if ex.kind != exCall: return
      if ex.callFunc == "toString" and
         ex.callArgs[0].ty.kind == tyString:
        assert ex.callArgs.len == 1
        ex = ex.callArgs[0]
    )

func hasHeapData(ty: Type): bool =
  # TODO: will need module for struct definitions
  ty.kind in [tyString, tyList]

proc hoistVarDecls(module: var Module) =
  for fn in module.funcs.mitems:
    let f = addr fn
    var owned: seq[VarDecl]
    forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
      if st.kind == stLetDecl:
        f[].decls.add(st.letDecl)
        if not st.letIntermediate:
          owned.add(st.letDecl)
      elif st.kind == stVarDecl:
        f[].decls.add(st.varDecl)
        owned.add(st.varDecl)
        result.removeSelf = true
    )
    for decl in owned:
      let isVar = decl.ty.isVar
      let ty = if isVar: decl.ty.varTy else: decl.ty
      if hasHeapData(ty):
        var ex = Expr(kind: exVar, varName: decl.name, ty: decl.ty.copy())
        if isVar:
          ex = makeDeref(ex)
        fn.epilog.add(Stmt(
          kind: stExpr,
          exprVal: makeDealloc(ex),
        ))

proc deallocBeforeAssign(module: var Module) =
  for fn in module.funcs.mitems:
    forAll(fn.body, stmts=proc(st: var Stmt): InsertStmts =
      if st.kind != stAssign: return
      if hasHeapData(st.asgLhs.ty):
        result.pre.add(Stmt(
          kind: stExpr,
          exprVal: makeDealloc(st.asgLhs)
        ))
    )

proc compileModule*(sexpr: SExpr): Module =
  result = codegenModule(sexpr)
  typeInference(addr result)
  removeGenericFunctions(result)
  simplifyStringToString(result)
  unfoldExpressions(result)
  initializeListVars(result)
  hoistVarDecls(result)
  deallocBeforeAssign(result)
