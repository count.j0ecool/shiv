# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sequtils
import strutils

import logger
import shiv_compiler
import shiv_parser

# cMangle(string) legalizes any invalid characters in a C name
func cMangle(c: char): string =
  if c.isAlphaNumeric() or c == '_':
    $c
  else:
    "A" & toHex(int(c), 2)

func cMangle(name: string): string =
  result = ""
  for c in name:
    result &= cMangle(c)

func cMangle(ty: Type): string =
  case ty.kind
  of tyVoid:   return "Void"
  of tyInt:    return "Int"
  of tyFloat:  return "Float"
  of tyChar:   return "Char"
  of tyString: return "String"
  of tyList:   return "List" & cMangle(ty.listTy)
  of tyVar:    return "Var" & cMangle(ty.varTy)
  of tyStruct: return cMangle(ty.stName)
  else:
    assert false, "Unknown c type: " & $ty

func cMangle(name: string, ret: Type, args: seq[Type]): string =
  if name == "main": return name
  "_Sv_" & cMangle(name) & "_" & cMangle(ret) & "_" &
    $args.len & args.map(cMangle).join("")

func cType(ty: Type): string =
  case ty.kind
  of tyVoid:   return "Void"
  of tyInt:    return "Int"
  of tyFloat:  return "Float"
  of tyChar:   return "Char"
  of tyString: return "String"
  of tyList:   return "List" & cType(ty.listTy)
  of tyVar:    return cType(ty.varTy) & "*"
  of tyStruct: return cMangle(ty.stName)
  else:
    assert false, "Unknown c type: " & $ty

func cDecl(decl: VarDecl): string =
  cType(decl.ty) & " " & cMangle(decl.name)
func cVarDecl(decl: VarDecl): string =
  let valDecl = VarDecl(name: decl.name & "_val__", ty: decl.ty.varTy)
  cDecl(valDecl) & "; " & cDecl(decl) & " = &" & valDecl.name

func isCMain(fn: Func): bool =
  fn.name == "main"

func cPrototype(fn: Func, forceNames = false): string =
  # forceNames is for body-less functions that have named args,
  # e.g. generated struct functions
  if fn.isCMain():
    return "int main(int argc, char** argv)"
  let args =
    if fn.hasDefinition or forceNames:
      fn.args.map(cDecl)
    else:
      fn.args.mapIt(cType(it.ty))
  let argStr = args.join(", ")
  let mangled = cMangle(fn.name, fn.retType, fn.args.mapIt(it.ty))
  cType(fn.retType) & " " & mangled & "(" & argStr & ")"

func cExprList(exprList: ExprList): string
func cExpr(ex: Expr): string =
  case ex.kind
  of exLiteral:
    if ex.litValue[0] == '"':
      "_makeString(" & ex.litValue & ")"
    else: 
      ex.litValue
  of exVar:
    ex.varName
  of exCall:
    let f =
      case ex.callFunc
      of "_deref":
        "*"
      else:
        cMangle(ex.callFunc, ex.ty, ex.callArgs.mapIt(it.ty))
    f & "(" & cExprList(ex.callArgs) & ")"

func cExprList(exprList: ExprList): string =
  exprList.map(cExpr).join(", ")

const tabStr = "  "
func cStmtList(stmtList: StmtList, indent: int): string
func cStmt(st: Stmt, indent: int): string =
  let tab = tabStr.repeat(indent)
  case st.kind
  of stIf:
    return tab & "if (" & cExpr(st.ifCond) & ") {\n" &
      cStmtList(st.ifTrue, indent + 1) &
    tab & "}\n"
  of stReturn:
    return tab & "_retVal = " & cExpr(st.retExpr) & "; goto epilog;\n"
  of stLetDecl:
    return tab & cMangle(st.letDecl.name) & " = " & cExpr(st.letVal) & ";\n"
  of stAssign:
    return tab & "*" & cExpr(st.asgLhs) & " = " & cExpr(st.asgVal) & ";\n"
  of stLoop:
    return tab & "while (1) {\n" &
      cStmtList(st.loopBody, indent + 1) &
    tab & "}\n"
  of stBreak:
    return tab & "break;\n"
  of stExpr:
    return tab & cExpr(st.exprVal) & ";\n"
  else:
    assert false, "Unsupported c stmt:\n" & $st

func cStmtList(stmtList: StmtList, indent: int): string =
  for st in stmtList:
    result &= cStmt(st, indent)

proc cStructDecl(name: string, fields: seq[VarDecl]): string =
  let log = logger("cStructDecl(" & name & ")")
  log "fields: ", fields
  let mangled = cMangle(name)
  result = "typedef struct {\n"
  for field in fields:
    result &= "  " & cType(field.ty) & " " & cMangle(field.name) & ";\n"
  result &= "} " & mangled & ";\n"

  let funcs = structFuncs(name, fields)

  let con = funcs.constructor
  result &= cPrototype(con, forceNames = true) & " {\n"
  result &= "  " & mangled & " result;\n"
  for field in fields:
    result &= "  result." & field.name & " = " & field.name & ";\n"
  result &= "  return result;\n"
  result &= "}\n"

  let toStr = funcs.toString
  result &= cPrototype(toStr, forceNames = true) & " {\n"
  result &= "  return _makeString(\"{" & name & "}\");\n"
  result &= "}\n"

  for acc in funcs.accessors:
    result &= cPrototype(acc, forceNames = true) & " {\n"
    result &= "  return " & acc.args[0].name & "." & acc.name & ";\n"
    result &= "}\n"
  for acc in funcs.varAccessors:
    result &= cPrototype(acc, forceNames = true) & " {\n"
    result &= "  return &" & acc.args[0].name & "->" & acc.name & ";\n"
    result &= "}\n"
  result &= "\n"

proc cFunc(fn: Func): string =
  if not fn.hasDefinition: return ""
  let hasRet = fn.retType.kind != tyVoid
  result = "// " & fn.sigName() & "\n" & cPrototype(fn) & " {\n"
  if hasRet:
    result &= tabStr & cDecl(VarDecl(name: "_retVal", ty: fn.retType)) & ";\n"
  for decl in fn.decls:
    if decl.ty.kind != tyVar:
      result &= tabStr & cDecl(decl) & ";\n"
    else:
      result &= tabStr & cVarDecl(decl) & ";\n"
  result &= cStmtList(fn.body, 1)
  result &= "epilog:\n"
  result &= cStmtList(fn.epilog, 1)
  if hasRet:
    result &= tabStr & "return _retVal;\n"
  else:
    result &= tabStr & "return;\n"
  result &= "}\n"

proc cTypeDecl(ty: Type, incTypes: var seq[Type]): string =
  if ty in incTypes: return ""
  case ty.kind
  of tyVar:
    result = cTypeDecl(ty.varTy, incTypes)
    # Bail after checking varTy subtype; if varTy is not in incTypes after
    # recursing, it hasn't been added yet, and this should not add it
    if ty.varTy notin incTypes: return ""
  of tyList:
    if ty.listTy notin incTypes: return ""
    # Only declare List types not defined in stdlib
    result = "_SV_DECL_LIST(" & cMangle(ty.listTy) & ")\n"
  of tyStruct:
    if not ty.stIsDef: return ""
    if ty.stExtern:
      result = ""
    else:
      for field in ty.stFields:
        if field.ty notin incTypes: return ""
      result = cStructDecl(ty.stName, ty.stFields)
  else:
    discard
  incTypes.add(ty)

proc cLayoutTypes(module: Module): string =
  var incTypes = @[
    TInt(), TFloat(), TChar(), TString(),
    TList(TInt()), TList(TChar()),
    TList(TStruct("SdlEvent")),
  ]
  var typeSize = 0
  while incTypes.len != typeSize:
    typeSize = incTypes.len
    for ty in module.types:
      result &= cTypeDecl(ty, incTypes)

proc cBackend*(module: Module): string =
  result &= "#include \"shiv_stdlib.h\"\n"
  result &= "#include \"shiv_sdl2.h\"\n"
  result &= "\n"

  result &= cLayoutTypes(module)
  result &= "\n"

  for fn in module.funcs:
    # Forward declarations
    result &= cPrototype(fn) & "; // " & fn.sigName() & "\n"
  result &= "\n"

  for global in module.globals:
    result &= cVarDecl(global) & ";\n"
  result &= "\n"

  for fn in module.funcs:
    result &= cFunc(fn)
