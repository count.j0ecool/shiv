# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sequtils
import strutils

type
  Location* = tuple[row, col: int]
  TokenKind* = enum
    tSpace
    tLine
    tNumber
    tIdentifier
    tString
    tParenOpen
    tParenClose
    tCurlyOpen
    tCurlyClose
    tSquareOpen
    tSquareClose
    tEof
  Token* = object
    case kind*: TokenKind
    of tNumber, tIdentifier, tString:
      value*: string
    else:
      discard
    location*: Location

  SExprKind* = enum
    sAtom
    sList
  SExpr* = object
    case kind*: SExprKind
    of sAtom:
      name*: string
    of sList:
      elems*: seq[SExpr]

func sa*(name: string): SExpr =
  SExpr(kind: sAtom, name: name)
func sa*(x: SExpr): SExpr =
  x
func ssx*(elems: seq[SExpr]): SExpr =
  SExpr(kind: sList, elems: elems)
func ss*(elems: varargs[SExpr, sa]): SExpr =
  ssx(@elems)

func isIdentifierChar(c: char): bool =
  c == '_' or c.isAlphaNumeric()

iterator lex*(input: string): Token =
  var i = 0
  var row = 0
  var col = 0

  while i < input.len:
    let start = i
    let loc = (row, col)
    let c = input[i]
    var next: Token

    i += 1
    case c
    of '\n': next = Token(kind: tLine)
    of ' ', '\r', '\t':
      while i < input.len and input[i] in " \r\t":
        i += 1
      next = Token(kind: tSpace)
    of '(': next = Token(kind: tParenOpen)
    of ')': next = Token(kind: tParenClose)
    of '{': next = Token(kind: tCurlyOpen)
    of '}': next = Token(kind: tCurlyClose)
    of '[': next = Token(kind: tSquareOpen)
    of ']': next = Token(kind: tSquareClose)
    of '"':
      var str = ""
      while i < input.len and input[i] != '"':
        str &= input[i]
        i += 1
      i += 1
      next = Token(kind: tString, value: str)
    of ';':
      while i < input.len and input[i] != '\n':
        i += 1
    else:
      var ident = $c
      let breakSet = "(){}[] \n\r\t"
      if c.isDigit():
        var sawDot = false
        while i < input.len:
          if input[i] in breakSet: break
          if input[i] == '.':
            if sawDot:
              break
            sawDot = true
            ident &= input[i]
          elif not input[i].isDigit():
            break
          else:
            # is digit
            ident &= input[i]
          i += 1
        if ident[^1] == '.':
          # Ignore trailing decimal, e.g. "1." -> ["1", "."]
          ident = ident[0..^2]
          i -= 1
        next = Token(kind: tNumber, value: ident)
      else:
        let isAlpha = c.isIdentifierChar()
        while i < input.len:
          if isAlpha != input[i].isIdentifierChar(): break
          if input[i] in breakSet: break
          ident &= input[i]
          i += 1
        next = Token(kind: tIdentifier, value: ident)
    next.location = loc
    yield next

    col += i - start
    if c == '\n':
      row += 1
      col = 0

  yield Token(kind: tEof)

type Stack[T] = distinct seq[T]
func newStack*[T](): Stack[T] =
  Stack[T](@[])
proc top*[T](stack: var Stack[T]): var T =
  seq[T](stack)[seq[T](stack).len - 1]
proc push*[T](stack: var Stack[T], item: T) =
  seq[T](stack).add(item)
proc pop*[T](stack: var Stack[T]): T =
  result = stack.top()
  seq[T](stack).del(stack.len - 1)
func len*[T](stack: Stack[T]): int =
  seq[T](stack).len

proc parse*(contents: string): SExpr =
  var stack = newStack[seq[SExpr]]()
  var parenStack = newStack[TokenKind]()
  var prevToken = Token(kind: tSpace)
  var nextIsNegative = false
  stack.push(@[])
  stack.push(@[])
  parenStack.push(tCurlyOpen)
  parenStack.push(tCurlyOpen)

  for token in lex(contents):
    if nextIsNegative and token.kind != tNumber:
      nextIsNegative = false
      stack.top().add(SExpr(kind: sAtom, name: "-"))
    case token.kind
    of tIdentifier:
      if token.value == "-" and prevToken.kind in [
         tSpace, tParenOpen, tCurlyOpen, tSquareOpen, tLine]:
        nextIsNegative = true
      else:
        stack.top().add(SExpr(kind: sAtom, name: token.value))
    of tNumber:
      var name = token.value
      if nextIsNegative:
        name = "-" & name
        nextIsNegative = false
      stack.top().add(SExpr(kind: sAtom, name: name))
    of tString:
      stack.top().add(SExpr(kind: sAtom, name: '"' & token.value & '"'))
    of tLine, tEof:
      if parenStack.top() == tCurlyOpen:
        let curList = stack.pop()
        if curList.len == 1 and curList[0].kind == sList:
          stack.top().add(curList[0])
        elif curList.len > 0:
          stack.top().add(SExpr(kind: sList, elems: curList))
        stack.push(@[])
    of tParenOpen:
      stack.push(@[])
      parenStack.push(tParenOpen)
    of tParenClose:
      let last = stack.pop()
      assert parenStack.pop() == tParenOpen
      stack.top().add(SExpr(kind: sList, elems: last))
    of tCurlyOpen:
      stack.push(@[])
      stack.push(@[])
      parenStack.push(tCurlyOpen)
      parenStack.push(tCurlyOpen)
    of tCurlyClose:
      let last = stack.pop()
      assert parenStack.pop() == tCurlyOpen
      if last.len > 0:
        stack.top().add(SExpr(kind: sList, elems: last))
      let l2 = stack.pop()
      assert parenStack.pop() == tCurlyOpen
      stack.top().add(SExpr(kind: sList, elems: l2))
    of tSquareOpen:
      stack.push(@[])
      if prevToken.kind in [tSpace, tLine, tParenOpen, tCurlyOpen, tSquareOpen]:
        parenStack.push(tSquareOpen)
      else:
        parenStack.push(tSquareClose) # index
    of tSquareClose:
      let last = stack.pop()
      let paren = parenStack.pop()
      if paren == tSquareOpen:
        stack.top().add(ssx(@[sa("__list")] & last))
      else:
        assert paren == tSquareClose
        let prev = stack.top().pop()
        let lastSexpr = if last.len == 1: sa(last[0]) else: ssx(last)
        stack.top().add(ss("__index", prev, lastSexpr))
    else:
      discard
    prevToken = token
  assert parenStack.top() == tCurlyOpen
  discard stack.pop()
  let curList = stack.pop()
  assert stack.len == 0
  SExpr(kind: sList, elems: curList)

func `==`*(a, b: SExpr): bool =
  if a.kind != b.kind:
    return false
  case a.kind
  of sAtom:
    a.name == b.name
  of sList:
    a.elems == b.elems

func `$`*(sexpr: SExpr): string =
  case sexpr.kind
  of sAtom:
    sexpr.name
  of sList:
    "(" & sexpr.elems.join(" ") & ")"

func isComplex(sexpr: SExpr): bool =
  if sexpr.kind == sAtom:
    false
  else:
    sexpr.elems.anyIt(it.kind == sList)

const tabStr = "  "
proc pretty*(sexpr: SExpr, indent = 0): string =
  case sexpr.kind
  of sAtom:
    result = sexpr.name
  of sList:
    if not isComplex(sexpr):
      return $sexpr
    result = "("
    var didSeeList = false
    for i in 0..<sexpr.elems.len:
      let child = sexpr.elems[i]
      if not didSeeList:
        if child.kind == sList:
          didSeeList = true
        elif i != 0:
          result &= " "
      if didSeeList:
        result &= "\n" & tabStr.repeat(indent + 1)
      result &= pretty(child, indent + 1)
    result &= "\n" & tabStr.repeat(indent) & ")"
