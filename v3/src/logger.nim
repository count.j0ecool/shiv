# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

var logFile = stdout

proc closeLog*() =
  if logFile == nil: return
  logFile.close()
  logFile = nil

proc initLog*(tempDir: string) =
  if logFile != nil and logFile != stdout:
    closeLog()
  logFile = open(tempDir / "debug_log.txt", fmWrite)

proc silenceLog*() =
  logFile = nil

proc logKind*(kind: string, msgs: varargs[string, `$`]) =
  if logFile == nil: return
  logFile.write("[")
  logFile.write(kind)
  logFile.write("] ")
  for msg in msgs:
    logFile.write(msg)
  logFile.writeLine("")

proc logger*(kind: string): proc(msgs: varargs[string, `$`]) =
  return proc(msgs: varargs[string, `$`]) =
    logKind(kind, msgs)
