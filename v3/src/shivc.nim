# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import osproc
import strutils

import c_backend
import logger
import shiv_compiler
import shiv_parser
import shiv_preprocessor

type
  BuildKind = enum
    buildCompile
    buildRun
  Options = object
    input: string
    output: string
    build: BuildKind
    debug: bool
    tempDir: string
    useSdl: bool

func parseOptions(args: seq[string]): Options =
  result.build = buildRun
  result.debug = true
  result.tempDir = ".shiv_temp"

  var i = 0
  proc pop(): string =
    result = args[i]
    i += 1
  while i < args.len:
    let arg = pop()
    case arg
    of "--output", "-o": result.output = pop()
    of "--compile": result.build = buildCompile
    of "--run": result.build = buildRun
    of "--sdl": result.useSdl = true
    else: result.input = arg

proc doCompile(opt: Options): int =
  let log = logger("shivc main")
  log "Parsed options: ", opt
  let filename = opt.input
  assert filename != "", "Must specify an input filename"

  let tempDir = opt.tempDir
  createDir(tempDir)
  let includeDirs = @[
    filename.parentDir(),
    getAppFilename().parentDir().parentDir() / "include",
  ]
  log "Include dirs: ", $includeDirs
  let contents = open(filename).readAll()

  if opt.debug:
    let file = open(tempDir / "debug_lex.txt", fmWrite)
    for token in lex(contents):
      file.writeLine($token)
    file.close()

  log "Parsing..."
  var sexpr = parse(contents)
  if opt.debug:
    let file = open(tempDir / "debug_parse.shv", fmWrite)
    for elem in sexpr.elems:
      file.writeLine(pretty(elem))
    file.close()

  log "Preprocessing..."
  preprocess(sexpr, includeDirs)
  if opt.debug:
    let file = open(tempDir / "debug_preprocess.shv", fmWrite)
    for elem in sexpr.elems:
      file.writeLine(pretty(elem))
    file.close()

  log "Building AST..."
  var ast = compileModule(sexpr)
  if opt.debug:
    let file = open(tempDir / "debug_compile.txt", fmWrite)
    file.write(ast)
    file.close()

  log "Running C Backend..."
  let cSource = cBackend(ast)
  if opt.debug:
    let file = open(tempDir / "debug_backend.c", fmWrite)
    file.write(cSource)
    file.close()
  if opt.build == buildCompile:
    echo cSource
    return 0

  log "Running C compiler..."
  assert opt.build == buildRun
  let baseName = filename.lastPathPart()
  let cFilename = tempDir / baseName.changeFileExt("c")
  let exeName = tempDir / baseName.changeFileExt("")
  removeFile(cFilename)
  let cFile = open(cFilename, fmWrite)
  cFile.write(cSource)
  cFile.close()
  removeFile(exeName)
  let procOpts = {poUsePath, poStdErrToStdOut}
  var sdlArgs: seq[string]
  if opt.useSdl:
    sdlArgs = @[
      "-lmingw32", "-lopengl32", # Windows only
      # "-lGL", # Linux only
      "-lSDL2main", "-lSDL2", "-lSDL2_image", "-lSDL2_ttf",
      "-D_SHIV_USE_SDL",
    ]
  let gccOut = execProcess("gcc", options = procOpts,
    args = @[cFilename, "-o", exeName,
      "-std=c99",
      "-Ic_include",
      # "-lm", # Linux only
    ] & sdlArgs)
  if gccOut != "":
    echo gccOut
    return 1
  log "Running program..."
  let output = execProcess(exeName, args = [], options = procOpts)
  echo output
  return 0

proc main(args: seq[string]): int =
  let options = parseOptions(args)
  if options.debug:
    initLog(options.tempDir)
  else:
    silenceLog()
  defer: closeLog()
  return doCompile(options)

when isMainModule:
  quit(main(commandLineParams()))
