# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

import logger
import shiv_parser

type SExprFunc* = proc(elem: var SExpr)
proc dfs*(sexpr: var SExpr, f: SExprFunc) =
  if sexpr.kind == sAtom:
    f(sexpr)
  else:
    for elem in sexpr.elems.mitems:
      elem.dfs(f)
    f(sexpr)

func unwrapSingle(sexprs: seq[SExpr]): SExpr =
  if sexprs.len == 1:
    sexprs[0]
  else:
    ssx(sexprs)

type DefineDef* = tuple[before, after: string]
proc collectDefines*(defines: ptr seq[DefineDef]): SExprFunc =
  return proc(sexpr: var SExpr) =
    if sexpr.kind != sList: return
    var i = 0
    while i < sexpr.elems.len:
      let child = sexpr.elems[i]
      if child.kind == sList and child.elems[0] == sa("define"):
        assert child.elems.len == 3
        defines[].add((child.elems[1].name, child.elems[2].name))
        sexpr.elems.delete(i)
      else:
        i += 1

proc processDefines(defines: seq[DefineDef]): SExprFunc =
  return proc(sexpr: var SExpr) =
    if sexpr.kind != sList: return
    var didMod = true
    while didMod:
      didMod = false
      for def in defines:
        let idx = sexpr.elems.find(sa(def.before))
        if idx >= 0:
          sexpr.elems[idx] = sa(def.after)
          didMod = true

# Auto-parenthesizes return/if/while/for
proc parenlessKeywords*(sexpr: var SExpr) =
  if sexpr.kind != sList: return
  let elems = sexpr.elems
  if elems[0].kind != sAtom: return
  let us = unwrapSingle
  case elems[0].name
  of "return":
    assert elems.len >= 2
    sexpr.elems = @[elems[0], us(elems[1..^1])]
  of "while":
    assert elems.len >= 3
    sexpr.elems = @[elems[0], us(elems[1..^2]), elems[^1]]
  of "for":
    assert elems.len >= 5
    assert elems[2] == sa("in")
    sexpr.elems = elems[0..2] & @[us(elems[3..^2]), elems[^1]]
  of "if":
    assert elems.len >= 3
    # TODO: else
    sexpr.elems = @[elems[0], us(elems[1..^2]), elems[^1]]

type InfixData = seq[seq[string]]

proc findInfixData(module: var SExpr): InfixData =
  let defaultPrecedence = "(default)"
  result = @[
    @["<-"],
    @["and", "or"],
    @["&", "|"],
    @["<<", ">>"],
    @["<", ">", "==", "<=", ">="],
    @[defaultPrecedence, "+", "-"],
    @["*", "/"],
  ]
  var i = 0
  while i < module.elems.len:
    let sexpr = module.elems[i]
    if sexpr.kind != sList or sexpr.elems[0] != sa("infix"):
      i += 1
    else:
      module.elems.delete(i)
      assert sexpr.elems.len in [2, 3]
      assert sexpr.elems[1].kind == sAtom
      let operatorName = sexpr.elems[1].name
      var precedenceName = defaultPrecedence
      if sexpr.elems.len == 3:
        assert sexpr.elems[2].kind == sList
        let prec = sexpr.elems[2].elems
        assert prec[0] == sa("precedence")
        assert prec[1].kind == sAtom and prec[2].kind == sAtom
        assert prec[1].name == "=", "TODO: > and < precedence"
        precedenceName = prec[2].name
      var addToIdx = -1
      for i in 0..<result.len:
        if precedenceName in result[i]:
          addToIdx = i
          break
      result[addToIdx].add(operatorName)

proc parseInfix(data: InfixData): SExprFunc =
  proc recur(sexpr: var SExpr) =
    if sexpr.kind != sList: return
    let elems = sexpr.elems
    if elems[0] in [sa("fn"), sa("declare")]: return
    for precedenceLevel in data:
      for j in 1..<elems.len:
        let i = elems.len - j
        if elems[i].kind == sAtom and elems[i].name in precedenceLevel:
          var lhs = unwrapSingle(elems[0..<i])
          var rhs = unwrapSingle(elems[i+1..<elems.len])
          recur(lhs)
          recur(rhs)
          sexpr = ss(elems[i], lhs, rhs)
          return
  return recur

# Allows var declarations with initializations
proc initializedVars*(sexpr: var SExpr) =
  if sexpr.kind != sList: return
  var i = 0
  while i < sexpr.elems.len:
    let child = sexpr.elems[i]
    if child.kind == sList and
       child.elems.len >= 4 and
       child.elems[0] in [sa("var"), sa("let")]:
      for j in 2..<child.elems.len-1:
        if child.elems[j] == sa("="):
          let lhs = ssx(child.elems[0..<j])
          let rhs = unwrapSingle(child.elems[j+1..<child.elems.len])
          if child.elems[0] == sa("var"):
            let assign = ss("<-", child.elems[1], rhs)
            sexpr.elems[i] = lhs
            sexpr.elems.insert(assign, i + 1)
          else:
            assert child.elems[0] == sa("let")
            sexpr.elems[i] = ssx(lhs.elems & @[sa("="), rhs])
          break
    i += 1

proc whileLoopSupport(sexpr: var SExpr) =
  if sexpr.kind != sList: return
  var i = 0
  while i < sexpr.elems.len:
    let child = sexpr.elems[i]
    if child.kind == sList and
       child.elems.len > 0 and
       child.elems[0] == sa("while"):
      assert child.elems.len == 3
      let cond = child.elems[1]
      var body = child.elems[2]
      let pre = sexpr.elems[0..<i]
      let post = sexpr.elems[i+1..<sexpr.elems.len]
      body.elems.insert(ss("if", ss("not", cond), ss(ss("break"))), 0)
      sexpr = ssx(pre & @[ss("loop", body)] & post)
    i += 1

var numFors = 0
proc forLoopSupport(sexpr: var SExpr) =
  let log = logger "forLoopSupport"
  if sexpr.kind != sList: return
  var i = 0
  while i < sexpr.elems.len:
    let child = sexpr.elems[i]
    if child.kind == sList and
       child.elems.len > 0 and
       child.elems[0] == sa("for"):
      log "Found a for: ", child
      assert child.elems.len == 5
      let name = child.elems[1].name
      assert child.elems[2] == sa("in")
      let listElem = child.elems[3]
      var body = child.elems[4]
      var pre = sexpr.elems[0..<i]
      let post = sexpr.elems[i+1..<sexpr.elems.len]
      let index = "_idx" & $numFors
      let list =
        if listElem.kind == sAtom:
          listElem.name
        else:
          "_list" & $numFors
      numFors += 1
      pre.add(ss("var", index, ":", "Int"))
      pre.add(ss("<-", index, "0"))
      if listElem.kind != sAtom:
        pre.add(ss("let", list, "=", listElem))
      body.elems.insert(ss("if",
        ss(">=", index, ss("len", list)),
        ss(ss("break")),
      ), 0)
      body.elems.insert(ss("var", name), 1)
      body.elems.insert(ss("<-", name, ss("__index", list, index)), 2)
      body.elems.add(ss("<-", index, ss("+", index, "1")))
      sexpr = ssx(pre & @[ss("loop", body)] & post)
    i += 1

proc splitExcluding[T](list: seq[T], idx: int): tuple[left, right: seq[T]] =
  (list[0..<idx], list[idx+1..<list.len])

# Handle dotted sexprs, e.g. `(a . b)`
proc dotFunctions*(sexpr: var SExpr) =
  let log = logger("dotFunctions")
  if sexpr.kind != sList: return
  let idx = sexpr.elems.find(sa("."))
  if idx == -1: return
  log sexpr, ": ", idx
  assert idx != 0
  assert idx != sexpr.elems.len - 1
  var pre = ssx(sexpr.elems[0..<idx - 1])
  let post = sexpr.elems[idx + 2..<sexpr.elems.len]
  let head = sexpr.elems[idx - 1]
  let tail = sexpr.elems[idx + 1]
  sexpr = unwrapSingle(pre.elems & @[ss(tail, head)] & post)
  sexpr.dfs(dotFunctions)

# Implements print stmt as calls to toString, putString, and putLine
proc printStmts*(sexpr: var SExpr) =
  if sexpr.kind != sList: return
  var i = 0
  while i < sexpr.elems.len:
    let child = sexpr.elems[i]
    if child.kind == sList and
       child.elems.len > 0 and
       child.elems[0] == sa("print"):
      let (pre, post) = sexpr.elems.splitExcluding(i)
      var prints: seq[SExpr]
      for s in child.elems[1..<child.elems.len]:
        prints.add(ss("putString", ss("toString", s)))
      prints.add(ss("putLine"))
      sexpr = ssx(pre & prints & post)
    i += 1

proc handleIncludes(module: var SExpr, includeDirs: seq[string]) =
  module.elems.insert(ss("include", "builtins"), 0)
  module.elems.insert(ss("include", "stdlib"), 1)
  var i = 0
  while i < module.elems.len:
    let sexp = module.elems[i]
    if sexp.kind != sList or sexp.elems[0] != sa("include"):
      i += 1
    else:
      assert sexp.elems.len == 2 and sexp.elems[1].kind == sAtom
      let filename = sexp.elems[1].name & ".shv"
      var path = ""
      for dir in includeDirs:
        if existsFile(dir / filename):
          path = dir / filename
          break
      assert path != "", "File " & filename & " not found, paths: " & $includeDirs
      let toInclude = open(path).readAll().parse()
      let (pre, post) = module.elems.splitExcluding(i)
      module = ssx(pre & toInclude.elems & post)

proc preprocess*(module: var SExpr, includeDirs: seq[string]) =
  assert module.kind == sList
  handleIncludes(module, includeDirs)
  var defines: seq[DefineDef]
  module.dfs(collectDefines(addr defines))
  module.dfs(processDefines(defines))
  module.dfs(parenlessKeywords)
  module.dfs(initializedVars)
  let infixData = findInfixData(module)
  module.dfs(parseInfix(infixData))
  module.dfs(whileLoopSupport)
  module.dfs(forLoopSupport)
  module.dfs(dotFunctions)
  module.dfs(printStmts)
