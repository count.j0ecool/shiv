# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest

import logger
import shiv_parser
import shiv_preprocessor

silenceLog()

proc runPass(pass: SExprFunc): proc(input: string): SExpr =
  return proc(input: string): SExpr =
    result = parse(input)
    result.dfs(pass)

suite "Preprocessor":
  test "collectDefines":
    proc test(input: string): tuple[module: SExpr, defs: seq[DefineDef]] =
      var module = parse(input)
      var defs: seq[DefineDef]
      module.dfs(collectDefines(addr defs))
      (module, defs)
    check:
      test("define foo bar") ==
        (ss(), @[("foo", "bar")])

      test("""
        define foo bar
        define one two
      """) ==
        (ss(), @[("foo", "bar"), ("one", "two")])

      test("""
        define one two
        infix foo
        define three four
      """) ==
        (parse("infix foo"), @[("one", "two"), ("three", "four")])

  test "parenlessKeywords":
    let test = runPass(parenlessKeywords)
    check:
      test("return foo") == parse("return foo")
      test("return f x y") == parse("return (f x y)")

      test("while x { print x }") == parse("while x { print x }")
      test("while (f) { print x }") == parse("while (f) { print x }")
      test("while x > 5 { foo x }") == parse("while (x > 5) { foo x }")

      test("if x { print x }") == parse("if x { print x }")
      test("if (f) { print x }") == parse("if (f) { print x }")
      test("if (f x) { print x }") == parse("if (f x) { print x }")
      test("if x > 5 { foo x }") == parse("if (x > 5) { foo x }")

      test("for item in list { f item }") ==
        parse("for item in list { f item }")
      test("for i in 0 -> 10 { f i }") ==
        parse("for i in (0 -> 10) { f i }")

  test "initializedVars":
    let test = runPass(initializedVars)
    check:
      test("var a = 0") == parse("(var a)\n(<- a 0)")
      test("var b = f x") == parse("(var b)\n(<- b (f x))")
      test("let a = 0") == parse("(let a = 0)")
      test("let b = f x") == parse("(let b = (f x))")

  test "dotFunctions":
    let test = runPass(dotFunctions)
    check:
      test("x.y") == parse("(y x)")
      test("x.y.z") == parse("(z (y x))")
      test("f x.y") == parse("(f (y x))")
      test("a.b c") == parse("(b a) c")
      test("(f a).b") == parse("(b (f a))")
      test("a.(g b)") == parse("((g b) a)")
      test("(f a).(g b)") == parse("((g b) (f a))")
      test("f a.(b c).d x") == parse("f (d ((b c) a)) x")
      test("1.foo") == parse("(foo 1)")
      test("1.2.foo") == parse("(foo 1.2)")
      test("x.f y.g") == parse("((f x) (g y))")

  test "printStmts":
    let test = runPass(printStmts)
    check:
      test("print \"hello\"") == parse("""
        (putString (toString "hello"))
        (putLine)
      """)
      test("print x \":\" y") == parse("""
        (putString (toString x))
        (putString (toString ":"))
        (putString (toString y))
        (putLine)
      """)
