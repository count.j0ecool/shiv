#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <math.h>

// gcc test.c -lmingw32 -lopengl32 -lSDL2main -lSDL2

void drawRect(float x, float y, float w, float h) {
  glBegin(GL_QUADS);
  glVertex3f(x    , y    , 0);
  glVertex3f(x + w, y    , 0);
  glVertex3f(x + w, y + h, 0);
  glVertex3f(x    , y + h, 0);
  glEnd();
}

int main(int argc, char** argv) {
  SDL_Init(SDL_INIT_EVERYTHING);

  SDL_Window* window = SDL_CreateWindow("Hellu",
    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
    1200, 900,
    SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

  SDL_GL_CreateContext(window);

  // SDL_LoadExtensions();
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glShadeModel(GL_SMOOTH);
  glClearColor(0.1, 0.2, 0.2, 0.5);
  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  int quit = 0;
  float t = 0;
  while (!quit) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        quit = 1;
      }
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor4f(1, 0, 0, 1);
    drawRect(0.5 * cosf(t), 0.5 * sinf(t), 0.3, 0.3);
    glColor3f(0, 1, 0);
    drawRect(0.4 * sinf(t), -0.4 * cos(t + 1), 0.2, 0.2);

    SDL_GL_SwapWindow(window);

    SDL_Delay(10);
    t += 0.01 * 2.5;
  }

  SDL_DestroyWindow(window);

  SDL_Quit();
  return 0;
}
