#/bin/sh
# Script to build hello_wasm project

DIR=projects/hello_wasm
SHIVC=../../shivc.js

pushd $DIR
node $SHIVC hello.shv # builds hello.wasm

if [[ "$1" == "--run" ]]; then
  node script.js
fi
