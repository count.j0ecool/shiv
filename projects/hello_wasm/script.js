// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KtestSexprParserIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var node_fs = require('fs');

function loadWasm(filename, imports) {
  var bytes = node_fs.readFileSync(filename);
  var wasm = new WebAssembly.Module(bytes);
  var instance = new WebAssembly.Instance(wasm, imports);
  return instance;
}

var memory = new WebAssembly.Memory({
  initial: 256,
  maximum: 256,
});
var u8 = new Uint8Array(memory.buffer);
function readStr(ptr) {
  var str = "";
  var i = 0
  while (u8[ptr+i] != 0) {
    str += String.fromCharCode(u8[ptr+i]);
    i++;
  }
  return str;
}

var imports = {
  env: {
    memory: memory,
    console_log: function (ptr) {
      var str = readStr(ptr);
      console.log(str);
    },
  }
}

var wasm = loadWasm("hello.wasm", imports);
wasm.exports.sayHi();
