#/bin/sh
# Script to build hello_html project
# Currently needs to be ran from the top-level shiv directory because the
# include directory is not relative to the shivc binary

DIR=projects/hello_html
OUT=$DIR/out

rm -rf $OUT
mkdir -p $OUT

node shivc.js $DIR/index.shv > $OUT/index.html
node shivc.js $DIR/script.shv > $OUT/script.js
