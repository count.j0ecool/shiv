#/bin/sh
# Script to build js_pong project
# Currently needs to be ran from the top-level shiv directory because the
# include directory is not relative to the shivc binary

DIR=projects/js_pong
OUT=$DIR/out

if [ "$1" != "--skip-html" ]; then
  rm -rf $OUT
  mkdir -p $OUT

  node shivc.js $DIR/index.shv > $OUT/index.html
fi

node shivc.js $DIR/script.shv > $OUT/script.js
