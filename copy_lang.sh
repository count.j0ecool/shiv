#!/bin/bash

SHIV_DIR=~/.config/sublime-text-3/Packages/User/Shiv
if [[ $1 == "--mac" ]]; then
  SHIV_DIR=~/"Library/Application Support/Sublime Text 3/Packages/User/Shiv"
fi

echo "Copying to: $SHIV_DIR"

rm -rf "$SHIV_DIR"
mkdir -p "$SHIV_DIR"

cp Shiv.sublime-syntax "$SHIV_DIR/Shiv.sublime-syntax"
cp Comments.tmPreferences "$SHIV_DIR/Comments.tmPreferences"
