; Copyright 2019 Google LLC
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     https://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

include rewrite_infix

; fs methods
fn readFile(filename) {
  js_asm "return _allocString(
    require('fs').readFileSync(_loadString(filename), 'utf8'));"
}
fn fileExists(filename) {
  js_asm "return _allocBool(require('fs').existsSync(_loadString(filename)));"
}
fn writeByteFile(filename bytes) {
  ; Writes a list of Int bytes to a given file
  js_asm "
  var jsBytes = [];
  var len = _loadInt(_len(bytes));
  var dataOff = _i32M[(bytes >> 2) + 3] >> 2;
  for (var i = 0; i < len; ++i) {
    jsBytes.push(_loadInt(_i32M[dataOff + i]));
  }
  require('fs').writeFileSync(_loadString(filename), Buffer(jsBytes));"
}

; path methods
fn parentDir(filename) {
  js_asm "return _allocString(require('path').dirname(_loadString(filename)));"
}
fn joinPaths(a b) {
  js_asm "return _allocString(require('path').join(_loadString(a), _loadString(b)));"
}

; builtins
fn currentFile() {
  js_asm "return _allocString(__filename);"
}
