// Shiv JS Preamble
  // 512MB should be enough for anyone...
  var _memoryLimit = 1024 * 1024 * 1024;
  var _memory = new ArrayBuffer(_memoryLimit);
  var _u8M = new Uint8Array(_memory);
  var _i32M = new Int32Array(_memory);

  var _types = {
    int: 0,
    list: 1,
    string: 2,
    dict: 3,
    char: 4,
    bool: 5,
  };
  var _typeNames = {
    0: 'int',
    1: 'list',
    2: 'string',
    3: 'dict',
    4: 'char',
    5: 'bool',
  };
  var _freeListPtr = 0;
  var _allocPtr = 1024;
  // header format: { nextPtr, size, refcount }
  var _allocHeaderSize = 12;
  // End Shiv JS Preamble
function assert(cond, msg) {
  var _reg2 = cond;
  var _reg1 = not(_reg2);
  var _reg3 = _loadBool(_reg1);
  free(_reg1);
  if (_reg3) {
    var _reg5 = msg;
    var _reg4 = toStr(_reg5);
    console.log(_loadString(_reg4));
    free(_reg4);
    var _reg7 = _allocInt(1);
    var _reg6 = exit(_reg7);
  }
};
function getOrDefault(dct, key, defaultVal) {
  var _reg10 = dct;
  var _reg11 = key;
  var _reg9 = hasKey(_reg10, _reg11);
  var _reg12 = _loadBool(_reg9);
  free(_reg9);
  if (_reg12) {
    var _reg14 = dct;
    var _reg15 = key;
    var _reg13 = _index(_reg14, _reg15);
    return _reg13;
  } else {
    var _reg16 = defaultVal;
    return _reg16;
  }
};
function $plus(lhs, rhs) {
  var _reg19 = lhs;
  var _reg18 = isList(_reg19);
  var _reg20 = _loadBool(_reg18);
  free(_reg18);
  if (_reg20) {
    var _reg22 = lhs;
    var _reg23 = rhs;
    var _reg21 = $at$plus(_reg22, _reg23);
    return _reg21;
  } else {
    var _reg26 = lhs;
    var _reg25 = isString(_reg26);
    var _reg28 = rhs;
    var _reg27 = isString(_reg28);
    var _reg24 = or(_reg25, _reg27);
    var _reg29 = _loadBool(_reg24);
    free(_reg24);
    if (_reg29) {
      var _reg31 = lhs;
      var _reg32 = rhs;
      var _reg30 = $dollar$plus(_reg31, _reg32);
      return _reg30;
    } else {
      var _reg34 = lhs;
      var _reg35 = rhs;
      var _reg33 = $num$plus(_reg34, _reg35);
      return _reg33;
    }
  }
};
function map(_f, lst) {
  var _reg37 = _allocList();
  var result = _reg37;
  var _reg38 = _allocInt(0);
  var i = _reg38;
  while(true) {
    var _reg40 = i;
    var _reg42 = lst;
    var _reg41 = len(_reg42);
    var _reg39 = $lt(_reg40, _reg41);
    var _reg52 = _reg39;
    var _reg51 = not(_reg52);
    var _reg53 = _loadBool(_reg51);
    free(_reg51);
    if (_reg53) {
      break;
    }
    var _reg43 = result;
    var _reg46 = lst;
    var _reg47 = i;
    var _reg45 = _index(_reg46, _reg47);
    var _reg44 = _f(_reg45);
    add(_reg43, _reg44);
    var _reg49 = i;
    var _reg50 = _allocInt(1);
    var _reg48 = $plus(_reg49, _reg50);
    decref(i);
    incref(_reg48);
    i = _reg48;
  }
  var _reg54 = result;
  return _reg54;
};
function concat(lst) {
  var _reg56 = _allocList();
  var result = _reg56;
  var _reg57 = _allocInt(0);
  var i = _reg57;
  while(true) {
    var _reg59 = i;
    var _reg61 = lst;
    var _reg60 = len(_reg61);
    var _reg58 = $lt(_reg59, _reg60);
    var _reg71 = _reg58;
    var _reg70 = not(_reg71);
    var _reg72 = _loadBool(_reg70);
    free(_reg70);
    if (_reg72) {
      break;
    }
    var _reg63 = result;
    var _reg65 = lst;
    var _reg66 = i;
    var _reg64 = _index(_reg65, _reg66);
    var _reg62 = $plus(_reg63, _reg64);
    decref(result);
    incref(_reg62);
    result = _reg62;
    var _reg68 = i;
    var _reg69 = _allocInt(1);
    var _reg67 = $plus(_reg68, _reg69);
    decref(i);
    incref(_reg67);
    i = _reg67;
  }
  var _reg73 = result;
  return _reg73;
};
function concatMap(_f, lst) {
  var _reg77 = _f;
  var _reg78 = lst;
  var _reg76 = map(_reg77, _reg78);
  var _reg75 = concat(_reg76);
  return _reg75;
};
function range(num) {
  var _reg80 = _allocInt(0);
  var i = _reg80;
  var _reg81 = _allocList();
  var result = _reg81;
  while(true) {
    var _reg83 = i;
    var _reg84 = num;
    var _reg82 = $lt(_reg83, _reg84);
    var _reg91 = _reg82;
    var _reg90 = not(_reg91);
    var _reg92 = _loadBool(_reg90);
    free(_reg90);
    if (_reg92) {
      break;
    }
    var _reg85 = result;
    var _reg86 = i;
    add(_reg85, _reg86);
    var _reg88 = i;
    var _reg89 = _allocInt(1);
    var _reg87 = $plus(_reg88, _reg89);
    decref(i);
    incref(_reg87);
    i = _reg87;
  }
  var _reg93 = result;
  return _reg93;
};
function slice(lst, lo, hi) {
  var _reg96 = lst;
  var _reg95 = isList(_reg96);
  var _reg97 = _loadBool(_reg95);
  free(_reg95);
  if (_reg97) {
    var _reg99 = lst;
    var _reg100 = lo;
    var _reg101 = hi;
    var _reg98 = sliceList(_reg99, _reg100, _reg101);
    return _reg98;
  } else {
    var _reg103 = lst;
    var _reg104 = lo;
    var _reg105 = hi;
    var _reg102 = sliceString(_reg103, _reg104, _reg105);
    return _reg102;
  }
  function sliceList(lst, lo, hi) {
    var _reg107 = _allocList();
    var result = _reg107;
    var _reg108 = _allocInt(0);
    var i = _reg108;
    while(true) {
      var _reg110 = i;
      var _reg112 = hi;
      var _reg113 = lo;
      var _reg111 = $minus(_reg112, _reg113);
      var _reg109 = $lt(_reg110, _reg111);
      var _reg124 = _reg109;
      var _reg123 = not(_reg124);
      var _reg125 = _loadBool(_reg123);
      free(_reg123);
      if (_reg125) {
        break;
      }
      var _reg114 = result;
      var _reg116 = lst;
      var _reg118 = i;
      var _reg119 = lo;
      var _reg117 = $plus(_reg118, _reg119);
      var _reg115 = _index(_reg116, _reg117);
      add(_reg114, _reg115);
      var _reg121 = i;
      var _reg122 = _allocInt(1);
      var _reg120 = $plus(_reg121, _reg122);
      decref(i);
      incref(_reg120);
      i = _reg120;
    }
    var _reg126 = result;
    return _reg126;
  };
  function sliceString(str, lo, hi) {
    var _reg128 = _allocString("");
    var result = _reg128;
    var _reg129 = _allocInt(0);
    var i = _reg129;
    while(true) {
      var _reg131 = i;
      var _reg133 = hi;
      var _reg134 = lo;
      var _reg132 = $minus(_reg133, _reg134);
      var _reg130 = $lt(_reg131, _reg132);
      var _reg146 = _reg130;
      var _reg145 = not(_reg146);
      var _reg147 = _loadBool(_reg145);
      free(_reg145);
      if (_reg147) {
        break;
      }
      var _reg136 = result;
      var _reg138 = str;
      var _reg140 = i;
      var _reg141 = lo;
      var _reg139 = $plus(_reg140, _reg141);
      var _reg137 = _index(_reg138, _reg139);
      var _reg135 = $plus(_reg136, _reg137);
      decref(result);
      incref(_reg135);
      result = _reg135;
      var _reg143 = i;
      var _reg144 = _allocInt(1);
      var _reg142 = $plus(_reg143, _reg144);
      decref(i);
      incref(_reg142);
      i = _reg142;
    }
    var _reg148 = result;
    return _reg148;
  };
};
function sliceTo(lst, hi) {
  var _reg151 = lst;
  var _reg152 = _allocInt(0);
  var _reg153 = hi;
  var _reg150 = slice(_reg151, _reg152, _reg153);
  return _reg150;
};
function sliceFrom(lst, lo) {
  var _reg156 = lst;
  var _reg157 = lo;
  var _reg159 = lst;
  var _reg158 = len(_reg159);
  var _reg155 = slice(_reg156, _reg157, _reg158);
  return _reg155;
};
function find(lst, val) {
  var _reg161 = _allocInt(0);
  var i = _reg161;
  while(true) {
    var _reg163 = i;
    var _reg165 = lst;
    var _reg164 = len(_reg165);
    var _reg162 = $lt(_reg163, _reg164);
    var _reg177 = _reg162;
    var _reg176 = not(_reg177);
    var _reg178 = _loadBool(_reg176);
    free(_reg176);
    if (_reg178) {
      break;
    }
    var _reg168 = lst;
    var _reg169 = i;
    var _reg167 = _index(_reg168, _reg169);
    var _reg170 = val;
    var _reg166 = $eq$eq(_reg167, _reg170);
    var _reg171 = _loadBool(_reg166);
    free(_reg166);
    if (_reg171) {
      var _reg172 = i;
      return _reg172;
    }
    var _reg174 = i;
    var _reg175 = _allocInt(1);
    var _reg173 = $plus(_reg174, _reg175);
    decref(i);
    incref(_reg173);
    i = _reg173;
  }
  var _reg179 = _allocInt(-1);
  return _reg179;
};
function reverseFind(lst, val) {
  var _reg183 = lst;
  var _reg182 = len(_reg183);
  var _reg184 = _allocInt(1);
  var _reg181 = $minus(_reg182, _reg184);
  var i = _reg181;
  while(true) {
    var _reg186 = i;
    var _reg187 = _allocInt(-1);
    var _reg185 = $gt(_reg186, _reg187);
    var _reg199 = _reg185;
    var _reg198 = not(_reg199);
    var _reg200 = _loadBool(_reg198);
    free(_reg198);
    if (_reg200) {
      break;
    }
    var _reg190 = lst;
    var _reg191 = i;
    var _reg189 = _index(_reg190, _reg191);
    var _reg192 = val;
    var _reg188 = $eq$eq(_reg189, _reg192);
    var _reg193 = _loadBool(_reg188);
    free(_reg188);
    if (_reg193) {
      var _reg194 = i;
      return _reg194;
    }
    var _reg196 = i;
    var _reg197 = _allocInt(1);
    var _reg195 = $minus(_reg196, _reg197);
    decref(i);
    incref(_reg195);
    i = _reg195;
  }
  var _reg201 = _allocInt(-1);
  return _reg201;
};
function splitAt(lst, idx) {
  var _reg204 = lst;
  var _reg205 = _allocInt(0);
  var _reg206 = idx;
  var _reg203 = slice(_reg204, _reg205, _reg206);
  var left = _reg203;
  var _reg208 = lst;
  var _reg210 = idx;
  var _reg211 = _allocInt(1);
  var _reg209 = $plus(_reg210, _reg211);
  var _reg213 = lst;
  var _reg212 = len(_reg213);
  var _reg207 = slice(_reg208, _reg209, _reg212);
  var right = _reg207;
  var _reg216 = left;
  var _reg215 = len(_reg216);
  var _reg217 = _allocInt(2);
  var _reg214 = $lt(_reg215, _reg217);
  var _reg218 = _loadBool(_reg214);
  free(_reg214);
  if (_reg218) {
    var _reg220 = left;
    var _reg221 = _allocInt(0);
    var _reg219 = _index(_reg220, _reg221);
    decref(left);
    incref(_reg219);
    left = _reg219;
  }
  var _reg224 = right;
  var _reg223 = len(_reg224);
  var _reg225 = _allocInt(2);
  var _reg222 = $lt(_reg223, _reg225);
  var _reg226 = _loadBool(_reg222);
  free(_reg222);
  if (_reg226) {
    var _reg228 = right;
    var _reg229 = _allocInt(0);
    var _reg227 = _index(_reg228, _reg229);
    decref(right);
    incref(_reg227);
    right = _reg227;
  }
  var _reg230 = _allocList();
  var _reg231 = left;
  add(_reg230, _reg231);
  var _reg232 = right;
  add(_reg230, _reg232);
  return _reg230;
};
function mapDfs(f, xs) {
  function recurDfs(x) {
    var _reg236 = f;
    var _reg237 = x;
    var _reg235 = mapDfs(_reg236, _reg237);
    return _reg235;
  };
  var _reg239 = xs;
  var _reg238 = isList(_reg239);
  var _reg240 = _loadBool(_reg238);
  free(_reg238);
  if (_reg240) {
    var _reg243 = recurDfs;
    var _reg244 = xs;
    var _reg242 = map(_reg243, _reg244);
    var _reg241 = f(_reg242);
    return _reg241;
  } else {
    var _reg246 = xs;
    var _reg245 = f(_reg246);
    return _reg245;
  }
};
function mapBfs(f, xs) {
  function recurBfs(x) {
    var _reg250 = f;
    var _reg251 = x;
    var _reg249 = mapBfs(_reg250, _reg251);
    return _reg249;
  };
  var _reg253 = xs;
  var _reg252 = isList(_reg253);
  var _reg254 = _loadBool(_reg252);
  free(_reg252);
  if (_reg254) {
    var _reg256 = recurBfs;
    var _reg258 = xs;
    var _reg257 = f(_reg258);
    var _reg255 = map(_reg256, _reg257);
    return _reg255;
  } else {
    var _reg260 = xs;
    var _reg259 = f(_reg260);
    return _reg259;
  }
};
function contains(lst, elem) {
  var _reg262 = _allocInt(0);
  var i = _reg262;
  while(true) {
    var _reg264 = i;
    var _reg266 = lst;
    var _reg265 = len(_reg266);
    var _reg263 = $lt(_reg264, _reg265);
    var _reg279 = _reg263;
    var _reg278 = not(_reg279);
    var _reg280 = _loadBool(_reg278);
    free(_reg278);
    if (_reg280) {
      break;
    }
    var _reg268 = lst;
    var _reg269 = i;
    var _reg267 = _index(_reg268, _reg269);
    var x = _reg267;
    var _reg271 = x;
    var _reg272 = elem;
    var _reg270 = $eq$eq(_reg271, _reg272);
    var _reg273 = _loadBool(_reg270);
    free(_reg270);
    if (_reg273) {
      var _reg274 = _allocBool(true);
      return _reg274;
    }
    var _reg276 = i;
    var _reg277 = _allocInt(1);
    var _reg275 = $plus(_reg276, _reg277);
    decref(i);
    incref(_reg275);
    i = _reg275;
  }
  var _reg281 = _allocBool(false);
  return _reg281;
};
function isListStartingWith(list, head) {
  var _reg285 = list;
  var _reg284 = isList(_reg285);
  var _reg283 = not(_reg284);
  var _reg286 = _loadBool(_reg283);
  free(_reg283);
  if (_reg286) {
    var _reg287 = _allocBool(false);
    return _reg287;
  }
  var _reg290 = list;
  var _reg289 = len(_reg290);
  var _reg291 = _allocInt(0);
  var _reg288 = $eq$eq(_reg289, _reg291);
  var _reg292 = _loadBool(_reg288);
  free(_reg288);
  if (_reg292) {
    var _reg293 = _allocBool(false);
    return _reg293;
  }
  var _reg296 = list;
  var _reg297 = _allocInt(0);
  var _reg295 = _index(_reg296, _reg297);
  var _reg298 = head;
  var _reg294 = $eq$eq(_reg295, _reg298);
  return _reg294;
};
function isListStartingWithAny(list, options) {
  var _reg303 = list;
  var _reg302 = isList(_reg303);
  var _reg301 = not(_reg302);
  var _reg306 = list;
  var _reg305 = len(_reg306);
  var _reg307 = _allocInt(0);
  var _reg304 = $eq$eq(_reg305, _reg307);
  var _reg300 = or(_reg301, _reg304);
  var _reg308 = _loadBool(_reg300);
  free(_reg300);
  if (_reg308) {
    var _reg309 = _allocBool(false);
    return _reg309;
  }
  var _reg310 = _allocInt(0);
  var i = _reg310;
  while(true) {
    var _reg312 = i;
    var _reg314 = options;
    var _reg313 = len(_reg314);
    var _reg311 = $lt(_reg312, _reg313);
    var _reg329 = _reg311;
    var _reg328 = not(_reg329);
    var _reg330 = _loadBool(_reg328);
    free(_reg328);
    if (_reg330) {
      break;
    }
    var _reg316 = options;
    var _reg317 = i;
    var _reg315 = _index(_reg316, _reg317);
    var item = _reg315;
    var _reg320 = list;
    var _reg321 = _allocInt(0);
    var _reg319 = _index(_reg320, _reg321);
    var _reg322 = item;
    var _reg318 = $eq$eq(_reg319, _reg322);
    var _reg323 = _loadBool(_reg318);
    free(_reg318);
    if (_reg323) {
      var _reg324 = _allocBool(true);
      return _reg324;
    }
    var _reg326 = i;
    var _reg327 = _allocInt(1);
    var _reg325 = $plus(_reg326, _reg327);
    decref(i);
    incref(_reg325);
    i = _reg325;
  }
  var _reg331 = _allocBool(false);
  return _reg331;
};
function sexprHasChildStartingWith(sexpr, item) {
  var _reg334 = sexpr;
  var _reg333 = isList(_reg334);
  var _reg335 = _loadBool(_reg333);
  free(_reg333);
  if (_reg335) {
    var _reg336 = _allocInt(0);
    var i = _reg336;
    while(true) {
      var _reg338 = i;
      var _reg340 = sexpr;
      var _reg339 = len(_reg340);
      var _reg337 = $lt(_reg338, _reg339);
      var _reg352 = _reg337;
      var _reg351 = not(_reg352);
      var _reg353 = _loadBool(_reg351);
      free(_reg351);
      if (_reg353) {
        break;
      }
      var _reg343 = sexpr;
      var _reg344 = i;
      var _reg342 = _index(_reg343, _reg344);
      var _reg345 = item;
      var _reg341 = isListStartingWith(_reg342, _reg345);
      var _reg346 = _loadBool(_reg341);
      free(_reg341);
      if (_reg346) {
        var _reg347 = _allocBool(true);
        return _reg347;
      }
      var _reg349 = i;
      var _reg350 = _allocInt(1);
      var _reg348 = $plus(_reg349, _reg350);
      decref(i);
      incref(_reg348);
      i = _reg348;
    }
  }
  var _reg354 = _allocBool(false);
  return _reg354;
};
function flatten(lst) {
  var _reg356 = _allocString("");
  var result = _reg356;
  var _reg357 = _allocInt(0);
  var i6 = _reg357;
  while(true) {
    var _reg359 = i6;
    var _reg361 = lst;
    var _reg360 = len(_reg361);
    var _reg358 = $lt(_reg359, _reg360);
    var _reg372 = _reg358;
    var _reg371 = not(_reg372);
    var _reg373 = _loadBool(_reg371);
    free(_reg371);
    if (_reg373) {
      break;
    }
    var _reg363 = lst;
    var _reg364 = i6;
    var _reg362 = _index(_reg363, _reg364);
    var x = _reg362;
    var _reg366 = result;
    var _reg367 = x;
    var _reg365 = $plus(_reg366, _reg367);
    decref(result);
    incref(_reg365);
    result = _reg365;
    var _reg369 = i6;
    var _reg370 = _allocInt(1);
    var _reg368 = $plus(_reg369, _reg370);
    decref(i6);
    incref(_reg368);
    i6 = _reg368;
  }
  var _reg374 = result;
  return _reg374;
};
function join(lst, sep) {
  var _reg376 = _allocString("");
  var result = _reg376;
  var _reg377 = _allocInt(0);
  var i = _reg377;
  while(true) {
    var _reg379 = i;
    var _reg381 = lst;
    var _reg380 = len(_reg381);
    var _reg378 = $lt(_reg379, _reg380);
    var _reg398 = _reg378;
    var _reg397 = not(_reg398);
    var _reg399 = _loadBool(_reg397);
    free(_reg397);
    if (_reg399) {
      break;
    }
    var _reg383 = _allocInt(0);
    var _reg384 = i;
    var _reg382 = $lt(_reg383, _reg384);
    var _reg385 = _loadBool(_reg382);
    free(_reg382);
    if (_reg385) {
      var _reg387 = result;
      var _reg388 = sep;
      var _reg386 = $plus(_reg387, _reg388);
      decref(result);
      incref(_reg386);
      result = _reg386;
    }
    var _reg390 = result;
    var _reg392 = lst;
    var _reg393 = i;
    var _reg391 = _index(_reg392, _reg393);
    var _reg389 = $plus(_reg390, _reg391);
    decref(result);
    incref(_reg389);
    result = _reg389;
    var _reg395 = i;
    var _reg396 = _allocInt(1);
    var _reg394 = $plus(_reg395, _reg396);
    decref(i);
    incref(_reg394);
    i = _reg394;
  }
  var _reg400 = result;
  return _reg400;
};
function repeat(str, num) {
  var _reg402 = _allocString("");
  var result = _reg402;
  var _reg403 = _allocInt(0);
  var _ = _reg403;
  while(true) {
    var _reg405 = _;
    var _reg406 = num;
    var _reg404 = $lt(_reg405, _reg406);
    var _reg414 = _reg404;
    var _reg413 = not(_reg414);
    var _reg415 = _loadBool(_reg413);
    free(_reg413);
    if (_reg415) {
      break;
    }
    var _reg408 = result;
    var _reg409 = str;
    var _reg407 = $plus(_reg408, _reg409);
    decref(result);
    incref(_reg407);
    result = _reg407;
    var _reg411 = _;
    var _reg412 = _allocInt(1);
    var _reg410 = $plus(_reg411, _reg412);
    decref(_);
    incref(_reg410);
    _ = _reg410;
  }
  var _reg416 = result;
  return _reg416;
};
function isDigit(ch) {
  var _reg420 = ch;
  var _reg421 = _allocChar("0");
  var _reg419 = $gt$eq(_reg420, _reg421);
  var _reg423 = ch;
  var _reg424 = _allocChar("9");
  var _reg422 = $lt$eq(_reg423, _reg424);
  var _reg418 = and(_reg419, _reg422);
  return _reg418;
};
function dequote(str) {
  var _reg427 = str;
  var _reg428 = _allocInt(1);
  var _reg431 = str;
  var _reg430 = len(_reg431);
  var _reg432 = _allocInt(1);
  var _reg429 = $minus(_reg430, _reg432);
  var _reg426 = slice(_reg427, _reg428, _reg429);
  return _reg426;
};
function prettySexpr(sexpr) {
  var _reg435 = sexpr;
  var _reg436 = _allocInt(1);
  var _reg434 = inner(_reg435, _reg436);
  return _reg434;
  function inner(sexpr, indent) {
    var _reg440 = sexpr;
    var _reg439 = isList(_reg440);
    var _reg438 = not(_reg439);
    var _reg441 = _loadBool(_reg438);
    free(_reg438);
    if (_reg441) {
      var _reg442 = sexpr;
      return _reg442;
    }
    var _reg445 = sexpr;
    var _reg444 = len(_reg445);
    var _reg446 = _allocInt(0);
    var _reg443 = $eq$eq(_reg444, _reg446);
    var _reg447 = _loadBool(_reg443);
    free(_reg443);
    if (_reg447) {
      var _reg448 = _allocString("()");
      return _reg448;
    }
    var _reg450 = _allocString("  ");
    var _reg451 = indent;
    var _reg449 = repeat(_reg450, _reg451);
    var indentStr = _reg449;
    var _reg452 = _allocString("(");
    var result = _reg452;
    var _reg453 = _allocInt(0);
    var i = _reg453;
    while(true) {
      var _reg455 = i;
      var _reg457 = sexpr;
      var _reg456 = len(_reg457);
      var _reg454 = $lt(_reg455, _reg456);
      var _reg490 = _reg454;
      var _reg489 = not(_reg490);
      var _reg491 = _loadBool(_reg489);
      free(_reg489);
      if (_reg491) {
        break;
      }
      var _reg459 = i;
      var _reg460 = _allocInt(0);
      var _reg458 = $gt(_reg459, _reg460);
      var _reg461 = _loadBool(_reg458);
      free(_reg458);
      if (_reg461) {
        var _reg464 = sexpr;
        var _reg466 = i;
        var _reg467 = _allocInt(1);
        var _reg465 = $minus(_reg466, _reg467);
        var _reg463 = _index(_reg464, _reg465);
        var _reg462 = isList(_reg463);
        var _reg468 = _loadBool(_reg462);
        free(_reg462);
        if (_reg468) {
          var _reg471 = result;
          var _reg472 = _allocString("\n");
          var _reg470 = $plus(_reg471, _reg472);
          var _reg473 = indentStr;
          var _reg469 = $plus(_reg470, _reg473);
          decref(result);
          incref(_reg469);
          result = _reg469;
        } else {
          var _reg475 = result;
          var _reg476 = _allocString(" ");
          var _reg474 = $plus(_reg475, _reg476);
          decref(result);
          incref(_reg474);
          result = _reg474;
        }
      }
      var _reg478 = result;
      var _reg481 = sexpr;
      var _reg482 = i;
      var _reg480 = _index(_reg481, _reg482);
      var _reg484 = indent;
      var _reg485 = _allocInt(1);
      var _reg483 = $plus(_reg484, _reg485);
      var _reg479 = inner(_reg480, _reg483);
      var _reg477 = $plus(_reg478, _reg479);
      decref(result);
      incref(_reg477);
      result = _reg477;
      var _reg487 = i;
      var _reg488 = _allocInt(1);
      var _reg486 = $plus(_reg487, _reg488);
      decref(i);
      incref(_reg486);
      i = _reg486;
    }
    var _reg493 = result;
    var _reg494 = _allocString(")");
    var _reg492 = $plus(_reg493, _reg494);
    decref(result);
    incref(_reg492);
    result = _reg492;
    var _reg495 = result;
    return _reg495;
  };
};
function readFile(filename) {
return _allocString(
    require('fs').readFileSync(_loadString(filename), 'utf8'));
};
function fileExists(filename) {
return _allocBool(require('fs').existsSync(_loadString(filename)));
};
function writeByteFile(filename, bytes) {
  var jsBytes = [];
  var len = _loadInt(_len(bytes));
  var dataOff = _i32M[(bytes >> 2) + 3] >> 2;
  for (var i = 0; i < len; ++i) {
    jsBytes.push(_loadInt(_i32M[dataOff + i]));
  }
  require('fs').writeFileSync(_loadString(filename), Buffer(jsBytes));
};
function parentDir(filename) {
return _allocString(require('path').dirname(_loadString(filename)));
};
function joinPaths(a, b) {
return _allocString(require('path').join(_loadString(a), _loadString(b)));
};
function currentFile() {
return _allocString(__filename);
};
function parseSexprs(input) {
  var _reg503 = _allocList();
  var _reg504 = _allocList();
  add(_reg503, _reg504);
  var _reg505 = _allocList();
  add(_reg503, _reg505);
  var stack = _reg503;
  var _reg506 = _allocList();
  var parenStack = _reg506;
  var _reg507 = _allocBool(false);
  var isStrLit = _reg507;
  var _reg508 = _allocBool(true);
  var wasSep = _reg508;
  var _reg509 = _allocBool(false);
  var wasSymbol = _reg509;
  var _reg510 = _allocChar("0");
  var commentKind = _reg510;
  var _reg511 = _allocString("");
  var str = _reg511;
  var _reg512 = _allocInt(0);
  var i = _reg512;
  while(true) {
    var _reg514 = i;
    var _reg516 = input;
    var _reg515 = len(_reg516);
    var _reg513 = $lt(_reg514, _reg515);
    var _reg533 = _reg513;
    var _reg532 = not(_reg533);
    var _reg534 = _loadBool(_reg532);
    free(_reg532);
    if (_reg534) {
      break;
    }
    var _reg518 = input;
    var _reg519 = i;
    var _reg517 = _index(_reg518, _reg519);
    var c = _reg517;
    var _reg521 = commentKind;
    var _reg522 = _allocChar("0");
    var _reg520 = $not$eq(_reg521, _reg522);
    var _reg523 = _loadBool(_reg520);
    free(_reg520);
    if (_reg523) {
      var _reg524 = commentState();
    } else {
      var _reg525 = isStrLit;
      var _reg526 = _loadBool(_reg525);
      if (_reg526) {
        var _reg527 = stringState();
      } else {
        var _reg528 = normalState();
      }
    }
    var _reg530 = i;
    var _reg531 = _allocInt(1);
    var _reg529 = $num$plus(_reg530, _reg531);
    decref(i);
    incref(_reg529);
    i = _reg529;
  }
  var _reg535 = handleNewline();
  var _reg539 = stack;
  var _reg538 = len(_reg539);
  var _reg540 = _allocInt(1);
  var _reg537 = $eq$eq(_reg538, _reg540);
  var _reg541 = _allocString("Assertion failure: [len, stack, ==, 1]");
  var _reg536 = assert(_reg537, _reg541);
  var _reg543 = stack;
  var _reg542 = top(_reg543);
  return _reg542;
  function normalState() {
    var _reg546 = c;
    var _reg547 = _allocChar(" ");
    var _reg545 = $eq$eq(_reg546, _reg547);
    var _reg548 = _loadBool(_reg545);
    free(_reg545);
    if (_reg548) {
      var _reg549 = sep();
      var _reg550 = _allocBool(true);
      decref(wasSep);
      incref(_reg550);
      wasSep = _reg550;
    } else {
      var _reg552 = c;
      var _reg553 = _allocChar("\r");
      var _reg551 = $eq$eq(_reg552, _reg553);
      var _reg554 = _loadBool(_reg551);
      free(_reg551);
      if (_reg554) {
      } else {
        var _reg556 = c;
        var _reg557 = _allocChar("\n");
        var _reg555 = $eq$eq(_reg556, _reg557);
        var _reg558 = _loadBool(_reg555);
        free(_reg555);
        if (_reg558) {
          var _reg559 = sep();
          var _reg560 = _allocBool(true);
          decref(wasSep);
          incref(_reg560);
          wasSep = _reg560;
          var _reg561 = flatTop();
          var _reg562 = _loadBool(_reg561);
          if (_reg562) {
            var _reg563 = handleNewline();
            var _reg565 = stack;
            var _reg566 = _allocList();
            var _reg564 = push(_reg565, _reg566);
          }
        } else {
          var _reg568 = c;
          var _reg569 = _allocChar(";");
          var _reg567 = $eq$eq(_reg568, _reg569);
          var _reg570 = _loadBool(_reg567);
          free(_reg567);
          if (_reg570) {
            var _reg571 = sep();
            var _reg573 = _allocChar("(");
            var _reg572 = nextCharIsCommented(_reg573);
            var _reg574 = _loadBool(_reg572);
            free(_reg572);
            if (_reg574) {
              var _reg575 = _allocChar("(");
              decref(commentKind);
              incref(_reg575);
              commentKind = _reg575;
              var _reg577 = i;
              var _reg578 = _allocInt(1);
              var _reg576 = $num$plus(_reg577, _reg578);
              decref(i);
              incref(_reg576);
              i = _reg576;
            } else {
              var _reg580 = _allocChar("{");
              var _reg579 = nextCharIsCommented(_reg580);
              var _reg581 = _loadBool(_reg579);
              free(_reg579);
              if (_reg581) {
                var _reg582 = _allocChar("{");
                decref(commentKind);
                incref(_reg582);
                commentKind = _reg582;
                var _reg584 = i;
                var _reg585 = _allocInt(1);
                var _reg583 = $num$plus(_reg584, _reg585);
                decref(i);
                incref(_reg583);
                i = _reg583;
              } else {
                var _reg586 = _allocChar(";");
                decref(commentKind);
                incref(_reg586);
                commentKind = _reg586;
              }
            }
          } else {
            var _reg588 = c;
            var _reg589 = _allocChar("\"");
            var _reg587 = $eq$eq(_reg588, _reg589);
            var _reg590 = _loadBool(_reg587);
            free(_reg587);
            if (_reg590) {
              var _reg591 = sep();
              var _reg592 = _allocBool(true);
              decref(isStrLit);
              incref(_reg592);
              isStrLit = _reg592;
              var _reg593 = _allocBool(false);
              decref(wasSep);
              incref(_reg593);
              wasSep = _reg593;
            } else {
              var _reg595 = c;
              var _reg596 = _allocChar("'");
              var _reg594 = $eq$eq(_reg595, _reg596);
              var _reg597 = _loadBool(_reg594);
              free(_reg594);
              if (_reg597) {
                var _reg598 = sep();
                var _reg600 = i;
                var _reg601 = _allocInt(1);
                var _reg599 = $num$plus(_reg600, _reg601);
                decref(i);
                incref(_reg599);
                i = _reg599;
                var _reg603 = stack;
                var _reg602 = top(_reg603);
                var _reg605 = _allocString("'");
                var _reg608 = input;
                var _reg609 = i;
                var _reg607 = _index(_reg608, _reg609);
                var _reg606 = readChar(_reg607);
                var _reg604 = $dollar$plus(_reg605, _reg606);
                add(_reg602, _reg604);
              } else {
                var _reg611 = c;
                var _reg612 = _allocChar("(");
                var _reg610 = $eq$eq(_reg611, _reg612);
                var _reg613 = _loadBool(_reg610);
                free(_reg610);
                if (_reg613) {
                  var _reg614 = sep();
                  var _reg615 = _allocBool(true);
                  decref(wasSep);
                  incref(_reg615);
                  wasSep = _reg615;
                  var _reg617 = stack;
                  var _reg618 = _allocList();
                  var _reg616 = push(_reg617, _reg618);
                  var _reg620 = parenStack;
                  var _reg621 = _allocChar("(");
                  var _reg619 = push(_reg620, _reg621);
                } else {
                  var _reg623 = c;
                  var _reg624 = _allocChar(")");
                  var _reg622 = $eq$eq(_reg623, _reg624);
                  var _reg625 = _loadBool(_reg622);
                  free(_reg622);
                  if (_reg625) {
                    var _reg630 = parenStack;
                    var _reg629 = len(_reg630);
                    var _reg631 = _allocInt(0);
                    var _reg628 = $gt(_reg629, _reg631);
                    var _reg634 = parenStack;
                    var _reg633 = top(_reg634);
                    var _reg635 = _allocChar("(");
                    var _reg632 = $eq$eq(_reg633, _reg635);
                    var _reg627 = and(_reg628, _reg632);
                    var _reg636 = _allocString("Mismatched parens");
                    var _reg626 = assert(_reg627, _reg636);
                    var _reg638 = parenStack;
                    var _reg637 = pop(_reg638);
                    var _reg639 = endSexp();
                    var t = _reg639;
                    var _reg641 = stack;
                    var _reg640 = top(_reg641);
                    var _reg642 = t;
                    add(_reg640, _reg642);
                    var _reg643 = _allocBool(false);
                    decref(wasSep);
                    incref(_reg643);
                    wasSep = _reg643;
                  } else {
                    var _reg645 = c;
                    var _reg646 = _allocChar("{");
                    var _reg644 = $eq$eq(_reg645, _reg646);
                    var _reg647 = _loadBool(_reg644);
                    free(_reg644);
                    if (_reg647) {
                      var _reg648 = sep();
                      var _reg649 = _allocBool(true);
                      decref(wasSep);
                      incref(_reg649);
                      wasSep = _reg649;
                      var _reg651 = stack;
                      var _reg652 = _allocList();
                      var _reg650 = push(_reg651, _reg652);
                      var _reg654 = stack;
                      var _reg655 = _allocList();
                      var _reg653 = push(_reg654, _reg655);
                      var _reg657 = parenStack;
                      var _reg658 = _allocChar("{");
                      var _reg656 = push(_reg657, _reg658);
                    } else {
                      var _reg660 = c;
                      var _reg661 = _allocChar("}");
                      var _reg659 = $eq$eq(_reg660, _reg661);
                      var _reg662 = _loadBool(_reg659);
                      free(_reg659);
                      if (_reg662) {
                        var _reg667 = parenStack;
                        var _reg666 = len(_reg667);
                        var _reg668 = _allocInt(0);
                        var _reg665 = $gt(_reg666, _reg668);
                        var _reg671 = parenStack;
                        var _reg670 = top(_reg671);
                        var _reg672 = _allocChar("{");
                        var _reg669 = $eq$eq(_reg670, _reg672);
                        var _reg664 = and(_reg665, _reg669);
                        var _reg673 = _allocString("Mismatched parens");
                        var _reg663 = assert(_reg664, _reg673);
                        var _reg675 = parenStack;
                        var _reg674 = pop(_reg675);
                        var _reg676 = endSexp();
                        var t = _reg676;
                        var _reg679 = t;
                        var _reg678 = len(_reg679);
                        var _reg680 = _allocInt(0);
                        var _reg677 = $gt(_reg678, _reg680);
                        var _reg681 = _loadBool(_reg677);
                        free(_reg677);
                        if (_reg681) {
                          var _reg683 = stack;
                          var _reg682 = top(_reg683);
                          var _reg684 = t;
                          add(_reg682, _reg684);
                        }
                        var _reg685 = endSexp();
                        var v = _reg685;
                        var _reg687 = stack;
                        var _reg686 = top(_reg687);
                        var _reg688 = v;
                        add(_reg686, _reg688);
                        var _reg689 = _allocBool(false);
                        decref(wasSep);
                        incref(_reg689);
                        wasSep = _reg689;
                      } else {
                        var _reg691 = c;
                        var _reg692 = _allocChar("[");
                        var _reg690 = $eq$eq(_reg691, _reg692);
                        var _reg693 = _loadBool(_reg690);
                        free(_reg690);
                        if (_reg693) {
                          var _reg694 = wasSep;
                          var _reg695 = _loadBool(_reg694);
                          if (_reg695) {
                            var _reg697 = parenStack;
                            var _reg698 = _allocChar("[");
                            var _reg696 = push(_reg697, _reg698);
                          } else {
                            var _reg699 = sep();
                            var _reg701 = parenStack;
                            var _reg702 = _allocChar("i");
                            var _reg700 = push(_reg701, _reg702);
                          }
                          var _reg704 = stack;
                          var _reg705 = _allocList();
                          var _reg703 = push(_reg704, _reg705);
                          var _reg706 = _allocBool(true);
                          decref(wasSep);
                          incref(_reg706);
                          wasSep = _reg706;
                        } else {
                          var _reg708 = c;
                          var _reg709 = _allocChar("]");
                          var _reg707 = $eq$eq(_reg708, _reg709);
                          var _reg710 = _loadBool(_reg707);
                          free(_reg707);
                          if (_reg710) {
                            var _reg715 = parenStack;
                            var _reg714 = len(_reg715);
                            var _reg716 = _allocInt(0);
                            var _reg713 = $gt(_reg714, _reg716);
                            var _reg720 = parenStack;
                            var _reg719 = top(_reg720);
                            var _reg721 = _allocChar("i");
                            var _reg718 = $eq$eq(_reg719, _reg721);
                            var _reg724 = parenStack;
                            var _reg723 = top(_reg724);
                            var _reg725 = _allocChar("[");
                            var _reg722 = $eq$eq(_reg723, _reg725);
                            var _reg717 = or(_reg718, _reg722);
                            var _reg712 = and(_reg713, _reg717);
                            var _reg726 = _allocString("Mismatched square brackets");
                            var _reg711 = assert(_reg712, _reg726);
                            var _reg729 = parenStack;
                            var _reg728 = pop(_reg729);
                            var _reg730 = _allocChar("i");
                            var _reg727 = $eq$eq(_reg728, _reg730);
                            var isIndex = _reg727;
                            var _reg731 = endSexp();
                            var t = _reg731;
                            var _reg733 = isIndex;
                            var _reg732 = not(_reg733);
                            var _reg734 = _loadBool(_reg732);
                            free(_reg732);
                            if (_reg734) {
                              var _reg736 = stack;
                              var _reg735 = top(_reg736);
                              var _reg738 = _allocList();
                              var _reg739 = _allocString("__list");
                              add(_reg738, _reg739);
                              var _reg740 = t;
                              var _reg737 = $at$plus(_reg738, _reg740);
                              add(_reg735, _reg737);
                            } else {
                              var _reg741 = t;
                              var idx = _reg741;
                              var _reg744 = idx;
                              var _reg743 = len(_reg744);
                              var _reg745 = _allocInt(1);
                              var _reg742 = $eq$eq(_reg743, _reg745);
                              var _reg746 = _loadBool(_reg742);
                              free(_reg742);
                              if (_reg746) {
                                var _reg748 = idx;
                                var _reg749 = _allocInt(0);
                                var _reg747 = _index(_reg748, _reg749);
                                decref(idx);
                                incref(_reg747);
                                idx = _reg747;
                              }
                              var _reg752 = stack;
                              var _reg751 = top(_reg752);
                              var _reg750 = top(_reg751);
                              var prev = _reg750;
                              var _reg755 = stack;
                              var _reg754 = top(_reg755);
                              var _reg753 = pop(_reg754);
                              var _reg758 = stack;
                              var _reg757 = top(_reg758);
                              var _reg759 = _allocList();
                              var _reg760 = _allocString("__index");
                              add(_reg759, _reg760);
                              var _reg761 = prev;
                              add(_reg759, _reg761);
                              var _reg762 = idx;
                              add(_reg759, _reg762);
                              var _reg756 = push(_reg757, _reg759);
                            }
                          } else {
                            var _reg766 = str;
                            var _reg767 = _allocString("");
                            var _reg765 = $eq$eq(_reg766, _reg767);
                            var _reg769 = c;
                            var _reg770 = _allocChar("-");
                            var _reg768 = $eq$eq(_reg769, _reg770);
                            var _reg764 = and(_reg765, _reg768);
                            var _reg773 = input;
                            var _reg775 = i;
                            var _reg776 = _allocInt(1);
                            var _reg774 = $num$plus(_reg775, _reg776);
                            var _reg772 = _index(_reg773, _reg774);
                            var _reg771 = isDigit(_reg772);
                            var _reg763 = and(_reg764, _reg771);
                            var _reg777 = _loadBool(_reg763);
                            free(_reg763);
                            if (_reg777) {
                              var _reg778 = _allocBool(false);
                              decref(wasSep);
                              incref(_reg778);
                              wasSep = _reg778;
                              var _reg779 = _allocString("-");
                              decref(str);
                              incref(_reg779);
                              str = _reg779;
                              var _reg780 = _allocBool(false);
                              decref(wasSymbol);
                              incref(_reg780);
                              wasSymbol = _reg780;
                            } else {
                              var _reg781 = _allocBool(false);
                              decref(wasSep);
                              incref(_reg781);
                              wasSep = _reg781;
                              var _reg784 = _allocString("!@#$%^&*~`+-*/?<>=|.,:");
                              var _reg785 = c;
                              var _reg783 = find(_reg784, _reg785);
                              var _reg786 = _allocInt(-1);
                              var _reg782 = $not$eq(_reg783, _reg786);
                              var isSymbol = _reg782;
                              var _reg788 = isSymbol;
                              var _reg789 = wasSymbol;
                              var _reg787 = $not$eq(_reg788, _reg789);
                              var _reg790 = _loadBool(_reg787);
                              free(_reg787);
                              if (_reg790) {
                                var _reg791 = sep();
                              }
                              var _reg792 = isSymbol;
                              decref(wasSymbol);
                              incref(_reg792);
                              wasSymbol = _reg792;
                              var _reg794 = str;
                              var _reg795 = c;
                              var _reg793 = $dollar$plus(_reg794, _reg795);
                              decref(str);
                              incref(_reg793);
                              str = _reg793;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  function commentState() {
    var _reg798 = commentKind;
    var _reg799 = _allocChar(";");
    var _reg797 = $eq$eq(_reg798, _reg799);
    var _reg800 = _loadBool(_reg797);
    free(_reg797);
    if (_reg800) {
      var _reg802 = c;
      var _reg803 = _allocChar("\n");
      var _reg801 = $eq$eq(_reg802, _reg803);
      var _reg804 = _loadBool(_reg801);
      free(_reg801);
      if (_reg804) {
        var _reg805 = _allocChar("0");
        decref(commentKind);
        incref(_reg805);
        commentKind = _reg805;
        var _reg806 = normalState();
      }
    } else {
      var _reg808 = commentKind;
      var _reg809 = _allocChar("(");
      var _reg807 = $eq$eq(_reg808, _reg809);
      var _reg810 = _loadBool(_reg807);
      free(_reg807);
      if (_reg810) {
        var _reg812 = _allocChar(")");
        var _reg811 = nextCharIsCommented(_reg812);
        var _reg813 = _loadBool(_reg811);
        free(_reg811);
        if (_reg813) {
          var _reg814 = _allocChar("0");
          decref(commentKind);
          incref(_reg814);
          commentKind = _reg814;
          var _reg816 = i;
          var _reg817 = _allocInt(1);
          var _reg815 = $num$plus(_reg816, _reg817);
          decref(i);
          incref(_reg815);
          i = _reg815;
        }
      } else {
        var _reg819 = commentKind;
        var _reg820 = _allocChar("{");
        var _reg818 = $eq$eq(_reg819, _reg820);
        var _reg821 = _loadBool(_reg818);
        free(_reg818);
        if (_reg821) {
          var _reg823 = _allocChar("}");
          var _reg822 = nextCharIsCommented(_reg823);
          var _reg824 = _loadBool(_reg822);
          free(_reg822);
          if (_reg824) {
            var _reg825 = _allocChar("0");
            decref(commentKind);
            incref(_reg825);
            commentKind = _reg825;
            var _reg827 = i;
            var _reg828 = _allocInt(1);
            var _reg826 = $num$plus(_reg827, _reg828);
            decref(i);
            incref(_reg826);
            i = _reg826;
          }
        } else {
          var _reg830 = _allocBool(false);
          var _reg832 = _allocString("Unexpected comment kind: ");
          var _reg833 = commentKind;
          var _reg831 = $dollar$plus(_reg832, _reg833);
          var _reg829 = assert(_reg830, _reg831);
        }
      }
    }
  };
  function stringState() {
    var _reg836 = c;
    var _reg837 = _allocChar("\"");
    var _reg835 = $not$eq(_reg836, _reg837);
    var _reg838 = _loadBool(_reg835);
    free(_reg835);
    if (_reg838) {
      var _reg840 = str;
      var _reg842 = c;
      var _reg841 = readChar(_reg842);
      var _reg839 = $dollar$plus(_reg840, _reg841);
      decref(str);
      incref(_reg839);
      str = _reg839;
    } else {
      var _reg843 = _allocBool(false);
      decref(isStrLit);
      incref(_reg843);
      isStrLit = _reg843;
      var _reg846 = _allocString("\"");
      var _reg847 = str;
      var _reg845 = $dollar$plus(_reg846, _reg847);
      var _reg848 = _allocString("\"");
      var _reg844 = $dollar$plus(_reg845, _reg848);
      decref(str);
      incref(_reg844);
      str = _reg844;
      var _reg849 = sep();
    }
  };
  function flatTop() {
    var _reg854 = parenStack;
    var _reg853 = len(_reg854);
    var _reg855 = _allocInt(0);
    var _reg852 = $eq$eq(_reg853, _reg855);
    var _reg858 = parenStack;
    var _reg857 = top(_reg858);
    var _reg859 = _allocChar("{");
    var _reg856 = $eq$eq(_reg857, _reg859);
    var _reg851 = or(_reg852, _reg856);
    return _reg851;
  };
  function sep() {
    var _reg863 = str;
    var _reg862 = len(_reg863);
    var _reg864 = _allocInt(0);
    var _reg861 = $gt(_reg862, _reg864);
    var _reg865 = _loadBool(_reg861);
    free(_reg861);
    if (_reg865) {
      var _reg867 = stack;
      var _reg866 = top(_reg867);
      var _reg868 = str;
      add(_reg866, _reg868);
      var _reg869 = _allocString("");
      decref(str);
      incref(_reg869);
      str = _reg869;
    }
  };
  function endSexp() {
    var _reg871 = sep();
    var _reg873 = stack;
    var _reg872 = pop(_reg873);
    return _reg872;
  };
  function handleNewline() {
    var _reg875 = endSexp();
    var t = _reg875;
    var _reg879 = t;
    var _reg878 = len(_reg879);
    var _reg880 = _allocInt(1);
    var _reg877 = $eq$eq(_reg878, _reg880);
    var _reg883 = t;
    var _reg884 = _allocInt(0);
    var _reg882 = _index(_reg883, _reg884);
    var _reg881 = isList(_reg882);
    var _reg876 = and(_reg877, _reg881);
    var _reg885 = _loadBool(_reg876);
    free(_reg876);
    if (_reg885) {
      var _reg887 = stack;
      var _reg886 = top(_reg887);
      var _reg889 = t;
      var _reg890 = _allocInt(0);
      var _reg888 = _index(_reg889, _reg890);
      add(_reg886, _reg888);
    } else {
      var _reg893 = t;
      var _reg892 = len(_reg893);
      var _reg894 = _allocInt(0);
      var _reg891 = $gt(_reg892, _reg894);
      var _reg895 = _loadBool(_reg891);
      free(_reg891);
      if (_reg895) {
        var _reg897 = stack;
        var _reg896 = top(_reg897);
        var _reg898 = t;
        add(_reg896, _reg898);
      }
    }
  };
  function readChar(c) {
    var _reg901 = c;
    var _reg902 = _allocChar("\\");
    var _reg900 = $not$eq(_reg901, _reg902);
    var _reg903 = _loadBool(_reg900);
    free(_reg900);
    if (_reg903) {
      var _reg904 = c;
      return _reg904;
    }
    var _reg906 = i;
    var _reg907 = _allocInt(1);
    var _reg905 = $num$plus(_reg906, _reg907);
    decref(i);
    incref(_reg905);
    i = _reg905;
    var _reg909 = input;
    var _reg910 = i;
    var _reg908 = _index(_reg909, _reg910);
    var next = _reg908;
    var _reg912 = next;
    var _reg913 = _allocChar("\"");
    var _reg911 = $eq$eq(_reg912, _reg913);
    var _reg914 = _loadBool(_reg911);
    free(_reg911);
    if (_reg914) {
      var _reg915 = _allocChar("\"");
      return _reg915;
    } else {
      var _reg917 = next;
      var _reg918 = _allocChar("\\");
      var _reg916 = $eq$eq(_reg917, _reg918);
      var _reg919 = _loadBool(_reg916);
      free(_reg916);
      if (_reg919) {
        var _reg920 = _allocChar("\\");
        return _reg920;
      } else {
        var _reg922 = next;
        var _reg923 = _allocChar("n");
        var _reg921 = $eq$eq(_reg922, _reg923);
        var _reg924 = _loadBool(_reg921);
        free(_reg921);
        if (_reg924) {
          var _reg925 = _allocChar("\n");
          return _reg925;
        } else {
          var _reg927 = next;
          var _reg928 = _allocChar("r");
          var _reg926 = $eq$eq(_reg927, _reg928);
          var _reg929 = _loadBool(_reg926);
          free(_reg926);
          if (_reg929) {
            var _reg930 = _allocChar("\r");
            return _reg930;
          } else {
            var _reg932 = next;
            var _reg933 = _allocChar("0");
            var _reg931 = $eq$eq(_reg932, _reg933);
            var _reg934 = _loadBool(_reg931);
            free(_reg931);
            if (_reg934) {
              var _reg935 = _allocChar(" ");
              return _reg935;
            } else {
              var _reg937 = _allocBool(false);
              var _reg939 = _allocString("Unknown escape character: \\");
              var _reg940 = next;
              var _reg938 = $dollar$plus(_reg939, _reg940);
              var _reg936 = assert(_reg937, _reg938);
            }
          }
        }
      }
    }
  };
  function nextCharIsCommented(end) {
    var _reg946 = i;
    var _reg947 = _allocInt(1);
    var _reg945 = $num$plus(_reg946, _reg947);
    var _reg949 = input;
    var _reg948 = len(_reg949);
    var _reg944 = $lt(_reg945, _reg948);
    var _reg952 = input;
    var _reg953 = i;
    var _reg951 = _index(_reg952, _reg953);
    var _reg954 = _allocChar(";");
    var _reg950 = $eq$eq(_reg951, _reg954);
    var _reg943 = and(_reg944, _reg950);
    var _reg957 = input;
    var _reg959 = i;
    var _reg960 = _allocInt(1);
    var _reg958 = $num$plus(_reg959, _reg960);
    var _reg956 = _index(_reg957, _reg958);
    var _reg961 = end;
    var _reg955 = $eq$eq(_reg956, _reg961);
    var _reg942 = and(_reg943, _reg955);
    return _reg942;
  };
  function top(stack) {
    var _reg964 = stack;
    var _reg967 = stack;
    var _reg966 = len(_reg967);
    var _reg968 = _allocInt(1);
    var _reg965 = $minus(_reg966, _reg968);
    var _reg963 = _index(_reg964, _reg965);
    return _reg963;
  };
  function push(stack, item) {
    var _reg970 = stack;
    var _reg971 = item;
    add(_reg970, _reg971);
  };
  function pop(stack) {
    var _reg974 = stack;
    var _reg973 = top(_reg974);
    var ret = _reg973;
    var _reg975 = stack;
    var _reg978 = stack;
    var _reg977 = len(_reg978);
    var _reg979 = _allocInt(1);
    var _reg976 = $minus(_reg977, _reg979);
    del(_reg975, _reg976);
    var _reg980 = ret;
    return _reg980;
  };
};
function ssaFormConversion(module) {
  var _reg982 = _allocList();
  var result = _reg982;
  var _reg983 = _allocInt(0);
  var nextReg = _reg983;
  var _reg985 = ssaStmt;
  var _reg986 = module;
  var _reg984 = concatMap(_reg985, _reg986);
  return _reg984;
  function ssaStmt(stmt) {
    var _reg990 = stmt;
    var _reg989 = isList(_reg990);
    var _reg991 = _allocString("Assertion failure: [isList, stmt]");
    var _reg988 = assert(_reg989, _reg991);
    var _reg994 = stmt;
    var _reg993 = len(_reg994);
    var _reg995 = _allocInt(0);
    var _reg992 = $eq$eq(_reg993, _reg995);
    var _reg996 = _loadBool(_reg992);
    free(_reg992);
    if (_reg996) {
      var _reg997 = _allocList();
      return _reg997;
    }
    var _reg999 = stmt;
    var _reg1000 = _allocInt(0);
    var _reg998 = _index(_reg999, _reg1000);
    var head = _reg998;
    var _reg1002 = head;
    var _reg1003 = _allocString("var");
    var _reg1001 = $eq$eq(_reg1002, _reg1003);
    var _reg1004 = _loadBool(_reg1001);
    free(_reg1001);
    if (_reg1004) {
      var _reg1007 = stmt;
      var _reg1008 = _allocInt(3);
      var _reg1006 = _index(_reg1007, _reg1008);
      var _reg1005 = ssaExpr(_reg1006);
      var expr = _reg1005;
      var _reg1012 = expr;
      var _reg1013 = _allocString("instrs");
      var _reg1011 = _index(_reg1012, _reg1013);
      var _reg1014 = _allocList();
      var _reg1015 = _allocList();
      var _reg1016 = _allocString("var");
      add(_reg1015, _reg1016);
      var _reg1018 = stmt;
      var _reg1019 = _allocInt(1);
      var _reg1017 = _index(_reg1018, _reg1019);
      add(_reg1015, _reg1017);
      var _reg1020 = _allocString("=");
      add(_reg1015, _reg1020);
      var _reg1023 = expr;
      var _reg1024 = _allocString("reg");
      var _reg1022 = _index(_reg1023, _reg1024);
      add(_reg1015, _reg1022);
      add(_reg1014, _reg1015);
      var _reg1009 = $plus(_reg1011, _reg1014);
      return _reg1009;
    } else {
      var _reg1026 = head;
      var _reg1027 = _allocString("if");
      var _reg1025 = $eq$eq(_reg1026, _reg1027);
      var _reg1028 = _loadBool(_reg1025);
      free(_reg1025);
      if (_reg1028) {
        var _reg1032 = stmt;
        var _reg1031 = len(_reg1032);
        var _reg1033 = _allocInt(3);
        var _reg1030 = $gt$eq(_reg1031, _reg1033);
        var _reg1034 = _allocString("Assertion failure: [len, stmt, >=, 3]");
        var _reg1029 = assert(_reg1030, _reg1034);
        var _reg1035 = _allocList();
        var clauses = _reg1035;
        var _reg1036 = _allocInt(0);
        var idx = _reg1036;
        while(true) {
          var _reg1038 = idx;
          var _reg1040 = stmt;
          var _reg1039 = len(_reg1040);
          var _reg1037 = $lt(_reg1038, _reg1039);
          var _reg1131 = _reg1037;
          var _reg1130 = not(_reg1131);
          var _reg1132 = _loadBool(_reg1130);
          free(_reg1130);
          if (_reg1132) {
            break;
          }
          var _reg1044 = idx;
          var _reg1045 = _allocInt(0);
          var _reg1043 = $eq$eq(_reg1044, _reg1045);
          var _reg1048 = stmt;
          var _reg1049 = idx;
          var _reg1047 = _index(_reg1048, _reg1049);
          var _reg1050 = _allocString("else");
          var _reg1046 = $eq$eq(_reg1047, _reg1050);
          var _reg1042 = or(_reg1043, _reg1046);
          var _reg1051 = _allocString("Assertion failure: [idx, ==, 0, or, [__index, stmt, idx], ==, \"else\"]");
          var _reg1041 = assert(_reg1042, _reg1051);
          var _reg1052 = _allocList();
          var clause = _reg1052;
          var _reg1055 = idx;
          var _reg1056 = _allocInt(2);
          var _reg1054 = $plus(_reg1055, _reg1056);
          var _reg1058 = stmt;
          var _reg1057 = len(_reg1058);
          var _reg1053 = $lt(_reg1054, _reg1057);
          var _reg1059 = _loadBool(_reg1053);
          free(_reg1053);
          if (_reg1059) {
            var _reg1062 = stmt;
            var _reg1064 = idx;
            var _reg1065 = _allocInt(1);
            var _reg1063 = $plus(_reg1064, _reg1065);
            var _reg1061 = _index(_reg1062, _reg1063);
            var _reg1060 = ssaExpr(_reg1061);
            var cond = _reg1060;
            var _reg1066 = allocReg();
            var condReg = _reg1066;
            var _reg1070 = cond;
            var _reg1071 = _allocString("instrs");
            var _reg1069 = _index(_reg1070, _reg1071);
            var _reg1072 = _allocList();
            var _reg1073 = _allocList();
            var _reg1074 = _allocString("register");
            add(_reg1073, _reg1074);
            var _reg1075 = condReg;
            add(_reg1073, _reg1075);
            var _reg1076 = _allocString("=");
            add(_reg1073, _reg1076);
            var _reg1077 = _allocList();
            var _reg1078 = _allocString("_loadBool");
            add(_reg1077, _reg1078);
            var _reg1081 = cond;
            var _reg1082 = _allocString("reg");
            var _reg1080 = _index(_reg1081, _reg1082);
            add(_reg1077, _reg1080);
            add(_reg1073, _reg1077);
            add(_reg1072, _reg1073);
            var _reg1067 = $plus(_reg1069, _reg1072);
            var instrs = _reg1067;
            var _reg1087 = cond;
            var _reg1088 = _allocString("instrs");
            var _reg1086 = _index(_reg1087, _reg1088);
            var _reg1084 = len(_reg1086);
            var _reg1089 = _allocInt(1);
            var _reg1083 = $gt(_reg1084, _reg1089);
            var _reg1090 = _loadBool(_reg1083);
            free(_reg1083);
            if (_reg1090) {
              var _reg1092 = instrs;
              var _reg1093 = _allocList();
              var _reg1094 = _allocList();
              var _reg1095 = _allocString("free");
              add(_reg1094, _reg1095);
              var _reg1098 = cond;
              var _reg1099 = _allocString("reg");
              var _reg1097 = _index(_reg1098, _reg1099);
              add(_reg1094, _reg1097);
              add(_reg1093, _reg1094);
              var _reg1091 = $plus(_reg1092, _reg1093);
              decref(instrs);
              incref(_reg1091);
              instrs = _reg1091;
            }
            var _reg1101 = ssaStmt;
            var _reg1103 = stmt;
            var _reg1105 = idx;
            var _reg1106 = _allocInt(2);
            var _reg1104 = $plus(_reg1105, _reg1106);
            var _reg1102 = _index(_reg1103, _reg1104);
            var _reg1100 = concatMap(_reg1101, _reg1102);
            var body = _reg1100;
            var _reg1108 = instrs;
            var _reg1109 = _allocList();
            var _reg1110 = _allocList();
            var _reg1111 = _allocString("if");
            add(_reg1110, _reg1111);
            var _reg1112 = condReg;
            add(_reg1110, _reg1112);
            var _reg1113 = body;
            add(_reg1110, _reg1113);
            add(_reg1109, _reg1110);
            var _reg1107 = $plus(_reg1108, _reg1109);
            decref(clause);
            incref(_reg1107);
            clause = _reg1107;
            var _reg1115 = idx;
            var _reg1116 = _allocInt(3);
            var _reg1114 = $plus(_reg1115, _reg1116);
            decref(idx);
            incref(_reg1114);
            idx = _reg1114;
          } else {
            var _reg1118 = ssaStmt;
            var _reg1120 = stmt;
            var _reg1122 = idx;
            var _reg1123 = _allocInt(1);
            var _reg1121 = $plus(_reg1122, _reg1123);
            var _reg1119 = _index(_reg1120, _reg1121);
            var _reg1117 = concatMap(_reg1118, _reg1119);
            var body = _reg1117;
            var _reg1124 = body;
            decref(clause);
            incref(_reg1124);
            clause = _reg1124;
            var _reg1126 = idx;
            var _reg1127 = _allocInt(2);
            var _reg1125 = $plus(_reg1126, _reg1127);
            decref(idx);
            incref(_reg1125);
            idx = _reg1125;
          }
          var _reg1128 = clauses;
          var _reg1129 = clause;
          add(_reg1128, _reg1129);
        }
        var _reg1134 = clauses;
        var _reg1137 = clauses;
        var _reg1136 = len(_reg1137);
        var _reg1138 = _allocInt(1);
        var _reg1135 = $minus(_reg1136, _reg1138);
        var _reg1133 = _index(_reg1134, _reg1135);
        var result = _reg1133;
        var _reg1139 = _allocInt(1);
        var i = _reg1139;
        while(true) {
          var _reg1141 = i;
          var _reg1143 = clauses;
          var _reg1142 = len(_reg1143);
          var _reg1140 = $lt(_reg1141, _reg1142);
          var _reg1171 = _reg1140;
          var _reg1170 = not(_reg1171);
          var _reg1172 = _loadBool(_reg1170);
          free(_reg1170);
          if (_reg1172) {
            break;
          }
          var _reg1144 = result;
          var last = _reg1144;
          var _reg1146 = clauses;
          var _reg1150 = clauses;
          var _reg1149 = len(_reg1150);
          var _reg1151 = i;
          var _reg1148 = $minus(_reg1149, _reg1151);
          var _reg1152 = _allocInt(1);
          var _reg1147 = $minus(_reg1148, _reg1152);
          var _reg1145 = _index(_reg1146, _reg1147);
          decref(result);
          incref(_reg1145);
          result = _reg1145;
          var _reg1154 = result;
          var _reg1157 = result;
          var _reg1156 = len(_reg1157);
          var _reg1158 = _allocInt(1);
          var _reg1155 = $minus(_reg1156, _reg1158);
          var _reg1153 = _index(_reg1154, _reg1155);
          var _reg1159 = _allocString("else");
          add(_reg1153, _reg1159);
          var _reg1161 = result;
          var _reg1164 = result;
          var _reg1163 = len(_reg1164);
          var _reg1165 = _allocInt(1);
          var _reg1162 = $minus(_reg1163, _reg1165);
          var _reg1160 = _index(_reg1161, _reg1162);
          var _reg1166 = last;
          add(_reg1160, _reg1166);
          var _reg1168 = i;
          var _reg1169 = _allocInt(1);
          var _reg1167 = $plus(_reg1168, _reg1169);
          decref(i);
          incref(_reg1167);
          i = _reg1167;
        }
        var _reg1173 = result;
        return _reg1173;
      } else {
        var _reg1175 = head;
        var _reg1176 = _allocString("while");
        var _reg1174 = $eq$eq(_reg1175, _reg1176);
        var _reg1177 = _loadBool(_reg1174);
        free(_reg1174);
        if (_reg1177) {
          var _reg1180 = stmt;
          var _reg1181 = _allocInt(1);
          var _reg1179 = _index(_reg1180, _reg1181);
          var _reg1178 = ssaExpr(_reg1179);
          var cond = _reg1178;
          var _reg1183 = ssaStmt;
          var _reg1185 = stmt;
          var _reg1186 = _allocInt(2);
          var _reg1184 = _index(_reg1185, _reg1186);
          var _reg1182 = concatMap(_reg1183, _reg1184);
          var body = _reg1182;
          var _reg1188 = _allocList();
          var _reg1189 = _allocString("if");
          add(_reg1188, _reg1189);
          var _reg1190 = _allocList();
          var _reg1191 = _allocString("not");
          add(_reg1190, _reg1191);
          var _reg1194 = cond;
          var _reg1195 = _allocString("reg");
          var _reg1193 = _index(_reg1194, _reg1195);
          add(_reg1190, _reg1193);
          add(_reg1188, _reg1190);
          var _reg1196 = _allocList();
          var _reg1197 = _allocList();
          var _reg1198 = _allocString("break");
          add(_reg1197, _reg1198);
          add(_reg1196, _reg1197);
          add(_reg1188, _reg1196);
          var _reg1187 = ssaStmt(_reg1188);
          var ifStmt = _reg1187;
          var _reg1200 = _allocList();
          var _reg1201 = _allocString("loop");
          add(_reg1200, _reg1201);
          var _reg1202 = _allocList();
          var _reg1207 = cond;
          var _reg1208 = _allocString("instrs");
          var _reg1206 = _index(_reg1207, _reg1208);
          var _reg1209 = ifStmt;
          var _reg1204 = $plus(_reg1206, _reg1209);
          var _reg1210 = body;
          var _reg1203 = $plus(_reg1204, _reg1210);
          add(_reg1202, _reg1203);
          var _reg1199 = $plus(_reg1200, _reg1202);
          var loop = _reg1199;
          var _reg1211 = _allocList();
          var _reg1212 = loop;
          add(_reg1211, _reg1212);
          return _reg1211;
        } else {
          var _reg1214 = head;
          var _reg1215 = _allocString("return");
          var _reg1213 = $eq$eq(_reg1214, _reg1215);
          var _reg1216 = _loadBool(_reg1213);
          free(_reg1213);
          if (_reg1216) {
            var _reg1219 = stmt;
            var _reg1218 = len(_reg1219);
            var _reg1220 = _allocInt(1);
            var _reg1217 = $eq$eq(_reg1218, _reg1220);
            var _reg1221 = _loadBool(_reg1217);
            free(_reg1217);
            if (_reg1221) {
              var _reg1222 = _allocList();
              var _reg1223 = _allocList();
              var _reg1224 = _allocString("return");
              add(_reg1223, _reg1224);
              add(_reg1222, _reg1223);
              return _reg1222;
            } else {
              var _reg1227 = stmt;
              var _reg1228 = _allocInt(1);
              var _reg1226 = _index(_reg1227, _reg1228);
              var _reg1225 = ssaExpr(_reg1226);
              var expr = _reg1225;
              var _reg1232 = expr;
              var _reg1233 = _allocString("instrs");
              var _reg1231 = _index(_reg1232, _reg1233);
              var _reg1234 = _allocList();
              var _reg1235 = _allocList();
              var _reg1236 = _allocString("return");
              add(_reg1235, _reg1236);
              var _reg1239 = expr;
              var _reg1240 = _allocString("reg");
              var _reg1238 = _index(_reg1239, _reg1240);
              add(_reg1235, _reg1238);
              add(_reg1234, _reg1235);
              var _reg1229 = $plus(_reg1231, _reg1234);
              return _reg1229;
            }
          } else {
            var _reg1242 = head;
            var _reg1243 = _allocString("<-");
            var _reg1241 = $eq$eq(_reg1242, _reg1243);
            var _reg1244 = _loadBool(_reg1241);
            free(_reg1241);
            if (_reg1244) {
              var _reg1246 = stmt;
              var _reg1247 = _allocInt(1);
              var _reg1245 = _index(_reg1246, _reg1247);
              var lhs = _reg1245;
              var _reg1250 = stmt;
              var _reg1251 = _allocInt(2);
              var _reg1249 = _index(_reg1250, _reg1251);
              var _reg1248 = ssaExpr(_reg1249);
              var rhs = _reg1248;
              var _reg1255 = rhs;
              var _reg1256 = _allocString("instrs");
              var _reg1254 = _index(_reg1255, _reg1256);
              var _reg1257 = _allocList();
              var _reg1258 = _allocList();
              var _reg1259 = _allocString("decref");
              add(_reg1258, _reg1259);
              var _reg1260 = lhs;
              add(_reg1258, _reg1260);
              add(_reg1257, _reg1258);
              var _reg1261 = _allocList();
              var _reg1262 = _allocString("incref");
              add(_reg1261, _reg1262);
              var _reg1265 = rhs;
              var _reg1266 = _allocString("reg");
              var _reg1264 = _index(_reg1265, _reg1266);
              add(_reg1261, _reg1264);
              add(_reg1257, _reg1261);
              var _reg1267 = _allocList();
              var _reg1268 = _allocString("<-");
              add(_reg1267, _reg1268);
              var _reg1269 = lhs;
              add(_reg1267, _reg1269);
              var _reg1272 = rhs;
              var _reg1273 = _allocString("reg");
              var _reg1271 = _index(_reg1272, _reg1273);
              add(_reg1267, _reg1271);
              add(_reg1257, _reg1267);
              var _reg1252 = $plus(_reg1254, _reg1257);
              return _reg1252;
            } else {
              var _reg1275 = head;
              var _reg1276 = _allocString("print");
              var _reg1274 = $eq$eq(_reg1275, _reg1276);
              var _reg1277 = _loadBool(_reg1274);
              free(_reg1274);
              if (_reg1277) {
                var _reg1279 = _allocList();
                var _reg1280 = _allocString("toStr");
                add(_reg1279, _reg1280);
                var _reg1282 = stmt;
                var _reg1283 = _allocInt(1);
                var _reg1281 = _index(_reg1282, _reg1283);
                add(_reg1279, _reg1281);
                var _reg1278 = ssaExpr(_reg1279);
                var first = _reg1278;
                var _reg1286 = first;
                var _reg1287 = _allocString("reg");
                var _reg1285 = _index(_reg1286, _reg1287);
                var reg = _reg1285;
                var _reg1290 = first;
                var _reg1291 = _allocString("instrs");
                var _reg1289 = _index(_reg1290, _reg1291);
                var instrs = _reg1289;
                var _reg1292 = _allocInt(2);
                var i = _reg1292;
                while(true) {
                  var _reg1294 = i;
                  var _reg1296 = stmt;
                  var _reg1295 = len(_reg1296);
                  var _reg1293 = $lt(_reg1294, _reg1295);
                  var _reg1341 = _reg1293;
                  var _reg1340 = not(_reg1341);
                  var _reg1342 = _loadBool(_reg1340);
                  free(_reg1340);
                  if (_reg1342) {
                    break;
                  }
                  var _reg1298 = _allocList();
                  var _reg1299 = _allocString("toStr");
                  add(_reg1298, _reg1299);
                  var _reg1301 = stmt;
                  var _reg1302 = i;
                  var _reg1300 = _index(_reg1301, _reg1302);
                  add(_reg1298, _reg1300);
                  var _reg1297 = ssaExpr(_reg1298);
                  var cur = _reg1297;
                  var _reg1304 = _allocList();
                  var _reg1305 = _allocString("$+");
                  add(_reg1304, _reg1305);
                  var _reg1306 = reg;
                  add(_reg1304, _reg1306);
                  var _reg1309 = cur;
                  var _reg1310 = _allocString("reg");
                  var _reg1308 = _index(_reg1309, _reg1310);
                  add(_reg1304, _reg1308);
                  var _reg1303 = ssaExpr(_reg1304);
                  var addExpr = _reg1303;
                  var _reg1314 = instrs;
                  var _reg1317 = cur;
                  var _reg1318 = _allocString("instrs");
                  var _reg1316 = _index(_reg1317, _reg1318);
                  var _reg1313 = $plus(_reg1314, _reg1316);
                  var _reg1321 = addExpr;
                  var _reg1322 = _allocString("instrs");
                  var _reg1320 = _index(_reg1321, _reg1322);
                  var _reg1312 = $plus(_reg1313, _reg1320);
                  var _reg1323 = _allocList();
                  var _reg1324 = _allocList();
                  var _reg1325 = _allocString("free");
                  add(_reg1324, _reg1325);
                  var _reg1328 = cur;
                  var _reg1329 = _allocString("reg");
                  var _reg1327 = _index(_reg1328, _reg1329);
                  add(_reg1324, _reg1327);
                  add(_reg1323, _reg1324);
                  var _reg1330 = _allocList();
                  var _reg1331 = _allocString("free");
                  add(_reg1330, _reg1331);
                  var _reg1332 = reg;
                  add(_reg1330, _reg1332);
                  add(_reg1323, _reg1330);
                  var _reg1311 = $plus(_reg1312, _reg1323);
                  decref(instrs);
                  incref(_reg1311);
                  instrs = _reg1311;
                  var _reg1335 = addExpr;
                  var _reg1336 = _allocString("reg");
                  var _reg1334 = _index(_reg1335, _reg1336);
                  decref(reg);
                  incref(_reg1334);
                  reg = _reg1334;
                  var _reg1338 = i;
                  var _reg1339 = _allocInt(1);
                  var _reg1337 = $plus(_reg1338, _reg1339);
                  decref(i);
                  incref(_reg1337);
                  i = _reg1337;
                }
                var _reg1344 = instrs;
                var _reg1345 = _allocList();
                var _reg1346 = _allocList();
                var _reg1347 = _allocString("print");
                add(_reg1346, _reg1347);
                var _reg1348 = reg;
                add(_reg1346, _reg1348);
                add(_reg1345, _reg1346);
                var _reg1349 = _allocList();
                var _reg1350 = _allocString("free");
                add(_reg1349, _reg1350);
                var _reg1351 = reg;
                add(_reg1349, _reg1351);
                add(_reg1345, _reg1349);
                var _reg1343 = $plus(_reg1344, _reg1345);
                return _reg1343;
              } else {
                var _reg1354 = _allocList();
                var _reg1355 = _allocString("add");
                add(_reg1354, _reg1355);
                var _reg1356 = _allocString("del");
                add(_reg1354, _reg1356);
                var _reg1357 = head;
                var _reg1353 = find(_reg1354, _reg1357);
                var _reg1358 = _allocInt(-1);
                var _reg1352 = $not$eq(_reg1353, _reg1358);
                var _reg1359 = _loadBool(_reg1352);
                free(_reg1352);
                if (_reg1359) {
                  var _reg1362 = stmt;
                  var _reg1363 = _allocInt(1);
                  var _reg1361 = _index(_reg1362, _reg1363);
                  var _reg1360 = ssaExpr(_reg1361);
                  var obj = _reg1360;
                  var _reg1366 = stmt;
                  var _reg1367 = _allocInt(2);
                  var _reg1365 = _index(_reg1366, _reg1367);
                  var _reg1364 = ssaExpr(_reg1365);
                  var val = _reg1364;
                  var _reg1372 = obj;
                  var _reg1373 = _allocString("instrs");
                  var _reg1371 = _index(_reg1372, _reg1373);
                  var _reg1376 = val;
                  var _reg1377 = _allocString("instrs");
                  var _reg1375 = _index(_reg1376, _reg1377);
                  var _reg1369 = $plus(_reg1371, _reg1375);
                  var _reg1378 = _allocList();
                  var _reg1379 = _allocList();
                  var _reg1380 = head;
                  add(_reg1379, _reg1380);
                  var _reg1383 = obj;
                  var _reg1384 = _allocString("reg");
                  var _reg1382 = _index(_reg1383, _reg1384);
                  add(_reg1379, _reg1382);
                  var _reg1387 = val;
                  var _reg1388 = _allocString("reg");
                  var _reg1386 = _index(_reg1387, _reg1388);
                  add(_reg1379, _reg1386);
                  add(_reg1378, _reg1379);
                  var _reg1368 = $plus(_reg1369, _reg1378);
                  return _reg1368;
                } else {
                  var _reg1390 = head;
                  var _reg1391 = _allocString("break");
                  var _reg1389 = $eq$eq(_reg1390, _reg1391);
                  var _reg1392 = _loadBool(_reg1389);
                  free(_reg1389);
                  if (_reg1392) {
                    var _reg1393 = _allocList();
                    var _reg1394 = stmt;
                    add(_reg1393, _reg1394);
                    return _reg1393;
                  } else {
                    var _reg1396 = head;
                    var _reg1397 = _allocString("continue");
                    var _reg1395 = $eq$eq(_reg1396, _reg1397);
                    var _reg1398 = _loadBool(_reg1395);
                    free(_reg1395);
                    if (_reg1398) {
                      var _reg1399 = _allocList();
                      var _reg1400 = stmt;
                      add(_reg1399, _reg1400);
                      return _reg1399;
                    } else {
                      var _reg1402 = head;
                      var _reg1403 = _allocString("pass");
                      var _reg1401 = $eq$eq(_reg1402, _reg1403);
                      var _reg1404 = _loadBool(_reg1401);
                      free(_reg1401);
                      if (_reg1404) {
                        var _reg1405 = _allocList();
                        var _reg1406 = stmt;
                        add(_reg1405, _reg1406);
                        return _reg1405;
                      } else {
                        var _reg1408 = head;
                        var _reg1409 = _allocString("js_asm");
                        var _reg1407 = $eq$eq(_reg1408, _reg1409);
                        var _reg1410 = _loadBool(_reg1407);
                        free(_reg1407);
                        if (_reg1410) {
                          var _reg1411 = _allocList();
                          var _reg1412 = stmt;
                          add(_reg1411, _reg1412);
                          return _reg1411;
                        } else {
                          var _reg1414 = stmt;
                          var _reg1413 = ssaExpr(_reg1414);
                          var expr = _reg1413;
                          var _reg1417 = expr;
                          var _reg1418 = _allocString("instrs");
                          var _reg1416 = _index(_reg1417, _reg1418);
                          return _reg1416;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  function ssaExpr(expr) {
    var _reg1420 = allocReg();
    var r = _reg1420;
    function exprWith(instrs) {
      var _reg1423 = _allocDict();
      var _reg1424 = _allocString("instrs");
      var _reg1425 = instrs;
      set(_reg1423, _reg1424, _reg1425);
      var _reg1426 = _allocString("reg");
      var _reg1427 = r;
      set(_reg1423, _reg1426, _reg1427);
      return _reg1423;
    };
    function assign(val) {
      var _reg1429 = _allocList();
      var _reg1430 = _allocList();
      var _reg1431 = _allocString("register");
      add(_reg1430, _reg1431);
      var _reg1432 = r;
      add(_reg1430, _reg1432);
      var _reg1433 = _allocString("=");
      add(_reg1430, _reg1433);
      var _reg1434 = val;
      add(_reg1430, _reg1434);
      add(_reg1429, _reg1430);
      return _reg1429;
    };
    var _reg1437 = expr;
    var _reg1436 = isList(_reg1437);
    var _reg1435 = not(_reg1436);
    var _reg1438 = _loadBool(_reg1435);
    free(_reg1435);
    if (_reg1438) {
      var _reg1441 = expr;
      var _reg1442 = _allocInt(0);
      var _reg1440 = _index(_reg1441, _reg1442);
      var _reg1443 = _allocChar("\"");
      var _reg1439 = $eq$eq(_reg1440, _reg1443);
      var _reg1444 = _loadBool(_reg1439);
      free(_reg1439);
      if (_reg1444) {
        var _reg1447 = _allocList();
        var _reg1448 = _allocString("string");
        add(_reg1447, _reg1448);
        var _reg1450 = expr;
        var _reg1451 = _allocInt(1);
        var _reg1454 = expr;
        var _reg1453 = len(_reg1454);
        var _reg1455 = _allocInt(1);
        var _reg1452 = $minus(_reg1453, _reg1455);
        var _reg1449 = slice(_reg1450, _reg1451, _reg1452);
        add(_reg1447, _reg1449);
        var _reg1446 = assign(_reg1447);
        var _reg1445 = exprWith(_reg1446);
        return _reg1445;
      } else {
        var _reg1458 = expr;
        var _reg1459 = _allocInt(0);
        var _reg1457 = _index(_reg1458, _reg1459);
        var _reg1460 = _allocChar("'");
        var _reg1456 = $eq$eq(_reg1457, _reg1460);
        var _reg1461 = _loadBool(_reg1456);
        free(_reg1456);
        if (_reg1461) {
          var _reg1464 = _allocList();
          var _reg1465 = _allocString("char");
          add(_reg1464, _reg1465);
          var _reg1467 = expr;
          var _reg1468 = _allocInt(1);
          var _reg1466 = sliceFrom(_reg1467, _reg1468);
          add(_reg1464, _reg1466);
          var _reg1463 = assign(_reg1464);
          var _reg1462 = exprWith(_reg1463);
          return _reg1462;
        } else {
          var _reg1471 = expr;
          var _reg1470 = assign(_reg1471);
          var _reg1469 = exprWith(_reg1470);
          return _reg1469;
        }
      }
    }
    var _reg1475 = expr;
    var _reg1474 = len(_reg1475);
    var _reg1476 = _allocInt(0);
    var _reg1473 = $gt(_reg1474, _reg1476);
    var _reg1477 = _allocString("Assertion failure: [len, expr, >, 0]");
    var _reg1472 = assert(_reg1473, _reg1477);
    var _reg1479 = expr;
    var _reg1480 = _allocInt(0);
    var _reg1478 = _index(_reg1479, _reg1480);
    var head = _reg1478;
    var _reg1482 = head;
    var _reg1483 = _allocString("fn");
    var _reg1481 = $eq$eq(_reg1482, _reg1483);
    var _reg1484 = _loadBool(_reg1481);
    free(_reg1481);
    if (_reg1484) {
      var _reg1486 = expr;
      var _reg1489 = expr;
      var _reg1488 = len(_reg1489);
      var _reg1490 = _allocInt(1);
      var _reg1487 = $minus(_reg1488, _reg1490);
      var _reg1485 = sliceTo(_reg1486, _reg1487);
      var decl = _reg1485;
      var _reg1491 = decl;
      var _reg1493 = ssaStmt;
      var _reg1495 = expr;
      var _reg1498 = expr;
      var _reg1497 = len(_reg1498);
      var _reg1499 = _allocInt(1);
      var _reg1496 = $minus(_reg1497, _reg1499);
      var _reg1494 = _index(_reg1495, _reg1496);
      var _reg1492 = concatMap(_reg1493, _reg1494);
      add(_reg1491, _reg1492);
      var _reg1502 = expr;
      var _reg1503 = _allocInt(1);
      var _reg1501 = _index(_reg1502, _reg1503);
      var _reg1500 = isList(_reg1501);
      var isAnonymous = _reg1500;
      var _reg1504 = isAnonymous;
      var _reg1505 = _loadBool(_reg1504);
      if (_reg1505) {
        var _reg1508 = decl;
        var _reg1507 = assign(_reg1508);
        var _reg1506 = exprWith(_reg1507);
        return _reg1506;
      } else {
        var _reg1510 = _allocList();
        var _reg1511 = decl;
        add(_reg1510, _reg1511);
        var _reg1509 = exprWith(_reg1510);
        return _reg1509;
      }
    } else {
      var _reg1513 = head;
      var _reg1514 = _allocString("__list");
      var _reg1512 = $eq$eq(_reg1513, _reg1514);
      var _reg1515 = _loadBool(_reg1512);
      free(_reg1512);
      if (_reg1515) {
        var _reg1517 = _allocList();
        var _reg1518 = _allocString("_allocList");
        add(_reg1517, _reg1518);
        var _reg1516 = assign(_reg1517);
        var instrs = _reg1516;
        var _reg1519 = _allocInt(0);
        var i8 = _reg1519;
        while(true) {
          var _reg1521 = i8;
          var _reg1524 = expr;
          var _reg1525 = _allocInt(1);
          var _reg1523 = sliceFrom(_reg1524, _reg1525);
          var _reg1522 = len(_reg1523);
          var _reg1520 = $lt(_reg1521, _reg1522);
          var _reg1552 = _reg1520;
          var _reg1551 = not(_reg1552);
          var _reg1553 = _loadBool(_reg1551);
          free(_reg1551);
          if (_reg1553) {
            break;
          }
          var _reg1528 = expr;
          var _reg1529 = _allocInt(1);
          var _reg1527 = sliceFrom(_reg1528, _reg1529);
          var _reg1530 = i8;
          var _reg1526 = _index(_reg1527, _reg1530);
          var item = _reg1526;
          var _reg1532 = item;
          var _reg1531 = ssaExpr(_reg1532);
          var sub = _reg1531;
          var _reg1535 = instrs;
          var _reg1538 = sub;
          var _reg1539 = _allocString("instrs");
          var _reg1537 = _index(_reg1538, _reg1539);
          var _reg1534 = $plus(_reg1535, _reg1537);
          var _reg1540 = _allocList();
          var _reg1541 = _allocList();
          var _reg1542 = _allocString("add");
          add(_reg1541, _reg1542);
          var _reg1543 = r;
          add(_reg1541, _reg1543);
          var _reg1546 = sub;
          var _reg1547 = _allocString("reg");
          var _reg1545 = _index(_reg1546, _reg1547);
          add(_reg1541, _reg1545);
          add(_reg1540, _reg1541);
          var _reg1533 = $plus(_reg1534, _reg1540);
          decref(instrs);
          incref(_reg1533);
          instrs = _reg1533;
          var _reg1549 = i8;
          var _reg1550 = _allocInt(1);
          var _reg1548 = $plus(_reg1549, _reg1550);
          decref(i8);
          incref(_reg1548);
          i8 = _reg1548;
        }
        var _reg1555 = instrs;
        var _reg1554 = exprWith(_reg1555);
        return _reg1554;
      } else {
        var _reg1557 = head;
        var _reg1558 = _allocString("dict");
        var _reg1556 = $eq$eq(_reg1557, _reg1558);
        var _reg1559 = _loadBool(_reg1556);
        free(_reg1556);
        if (_reg1559) {
          var _reg1561 = _allocList();
          var _reg1562 = _allocString("_allocDict");
          add(_reg1561, _reg1562);
          var _reg1560 = assign(_reg1561);
          var instrs = _reg1560;
          var _reg1563 = _allocInt(0);
          var i9 = _reg1563;
          while(true) {
            var _reg1565 = i9;
            var _reg1568 = expr;
            var _reg1569 = _allocInt(1);
            var _reg1567 = _index(_reg1568, _reg1569);
            var _reg1566 = len(_reg1567);
            var _reg1564 = $lt(_reg1565, _reg1566);
            var _reg1611 = _reg1564;
            var _reg1610 = not(_reg1611);
            var _reg1612 = _loadBool(_reg1610);
            free(_reg1610);
            if (_reg1612) {
              break;
            }
            var _reg1572 = expr;
            var _reg1573 = _allocInt(1);
            var _reg1571 = _index(_reg1572, _reg1573);
            var _reg1574 = i9;
            var _reg1570 = _index(_reg1571, _reg1574);
            var pair = _reg1570;
            var _reg1577 = pair;
            var _reg1578 = _allocInt(0);
            var _reg1576 = _index(_reg1577, _reg1578);
            var _reg1575 = ssaExpr(_reg1576);
            var lhs = _reg1575;
            var _reg1581 = pair;
            var _reg1582 = _allocInt(1);
            var _reg1580 = _index(_reg1581, _reg1582);
            var _reg1579 = ssaExpr(_reg1580);
            var rhs = _reg1579;
            var _reg1586 = instrs;
            var _reg1589 = lhs;
            var _reg1590 = _allocString("instrs");
            var _reg1588 = _index(_reg1589, _reg1590);
            var _reg1585 = $plus(_reg1586, _reg1588);
            var _reg1593 = rhs;
            var _reg1594 = _allocString("instrs");
            var _reg1592 = _index(_reg1593, _reg1594);
            var _reg1584 = $plus(_reg1585, _reg1592);
            var _reg1595 = _allocList();
            var _reg1596 = _allocList();
            var _reg1597 = _allocString("set");
            add(_reg1596, _reg1597);
            var _reg1598 = r;
            add(_reg1596, _reg1598);
            var _reg1601 = lhs;
            var _reg1602 = _allocString("reg");
            var _reg1600 = _index(_reg1601, _reg1602);
            add(_reg1596, _reg1600);
            var _reg1605 = rhs;
            var _reg1606 = _allocString("reg");
            var _reg1604 = _index(_reg1605, _reg1606);
            add(_reg1596, _reg1604);
            add(_reg1595, _reg1596);
            var _reg1583 = $plus(_reg1584, _reg1595);
            decref(instrs);
            incref(_reg1583);
            instrs = _reg1583;
            var _reg1608 = i9;
            var _reg1609 = _allocInt(1);
            var _reg1607 = $plus(_reg1608, _reg1609);
            decref(i9);
            incref(_reg1607);
            i9 = _reg1607;
          }
          var _reg1614 = instrs;
          var _reg1613 = exprWith(_reg1614);
          return _reg1613;
        } else {
          var _reg1616 = head;
          var _reg1617 = _allocString("object");
          var _reg1615 = $eq$eq(_reg1616, _reg1617);
          var _reg1618 = _loadBool(_reg1615);
          free(_reg1615);
          if (_reg1618) {
            var _reg1619 = _allocList();
            var toDict = _reg1619;
            var _reg1620 = _allocInt(0);
            var i10 = _reg1620;
            while(true) {
              var _reg1622 = i10;
              var _reg1625 = expr;
              var _reg1626 = _allocInt(1);
              var _reg1624 = _index(_reg1625, _reg1626);
              var _reg1623 = len(_reg1624);
              var _reg1621 = $lt(_reg1622, _reg1623);
              var _reg1648 = _reg1621;
              var _reg1647 = not(_reg1648);
              var _reg1649 = _loadBool(_reg1647);
              free(_reg1647);
              if (_reg1649) {
                break;
              }
              var _reg1629 = expr;
              var _reg1630 = _allocInt(1);
              var _reg1628 = _index(_reg1629, _reg1630);
              var _reg1631 = i10;
              var _reg1627 = _index(_reg1628, _reg1631);
              var pair = _reg1627;
              var _reg1632 = toDict;
              var _reg1633 = _allocList();
              var _reg1636 = _allocString("\"");
              var _reg1638 = pair;
              var _reg1639 = _allocInt(0);
              var _reg1637 = _index(_reg1638, _reg1639);
              var _reg1635 = $plus(_reg1636, _reg1637);
              var _reg1640 = _allocString("\"");
              var _reg1634 = $plus(_reg1635, _reg1640);
              add(_reg1633, _reg1634);
              var _reg1642 = pair;
              var _reg1643 = _allocInt(1);
              var _reg1641 = _index(_reg1642, _reg1643);
              add(_reg1633, _reg1641);
              add(_reg1632, _reg1633);
              var _reg1645 = i10;
              var _reg1646 = _allocInt(1);
              var _reg1644 = $plus(_reg1645, _reg1646);
              decref(i10);
              incref(_reg1644);
              i10 = _reg1644;
            }
            var _reg1651 = _allocList();
            var _reg1652 = _allocString("dict");
            add(_reg1651, _reg1652);
            var _reg1653 = toDict;
            add(_reg1651, _reg1653);
            var _reg1650 = ssaExpr(_reg1651);
            return _reg1650;
          } else {
            var _reg1655 = head;
            var _reg1656 = _allocString(".");
            var _reg1654 = $eq$eq(_reg1655, _reg1656);
            var _reg1657 = _loadBool(_reg1654);
            free(_reg1654);
            if (_reg1657) {
              var _reg1659 = _allocList();
              var _reg1660 = _allocString("__index");
              add(_reg1659, _reg1660);
              var _reg1662 = expr;
              var _reg1663 = _allocInt(1);
              var _reg1661 = _index(_reg1662, _reg1663);
              add(_reg1659, _reg1661);
              var _reg1666 = _allocString("\"");
              var _reg1668 = expr;
              var _reg1669 = _allocInt(2);
              var _reg1667 = _index(_reg1668, _reg1669);
              var _reg1665 = $plus(_reg1666, _reg1667);
              var _reg1670 = _allocString("\"");
              var _reg1664 = $plus(_reg1665, _reg1670);
              add(_reg1659, _reg1664);
              var _reg1658 = ssaExpr(_reg1659);
              return _reg1658;
            } else {
              var _reg1672 = head;
              var _reg1673 = _allocString("js_asm");
              var _reg1671 = $eq$eq(_reg1672, _reg1673);
              var _reg1674 = _loadBool(_reg1671);
              free(_reg1671);
              if (_reg1674) {
                var _reg1677 = expr;
                var _reg1676 = assign(_reg1677);
                var _reg1675 = exprWith(_reg1676);
                return _reg1675;
              } else {
                var _reg1679 = ssaExpr;
                var _reg1681 = expr;
                var _reg1682 = _allocInt(1);
                var _reg1680 = sliceFrom(_reg1681, _reg1682);
                var _reg1678 = map(_reg1679, _reg1680);
                var exprs = _reg1678;
                var _reg1684 = function(x) {
                  var _reg1687 = x;
                  var _reg1688 = _allocString("instrs");
                  var _reg1686 = _index(_reg1687, _reg1688);
                  return _reg1686;
                };
                var _reg1689 = exprs;
                var _reg1683 = concatMap(_reg1684, _reg1689);
                var instrs = _reg1683;
                var _reg1691 = function(x) {
                  var _reg1694 = x;
                  var _reg1695 = _allocString("reg");
                  var _reg1693 = _index(_reg1694, _reg1695);
                  return _reg1693;
                };
                var _reg1696 = exprs;
                var _reg1690 = map(_reg1691, _reg1696);
                var args = _reg1690;
                var _reg1699 = instrs;
                var _reg1702 = _allocList();
                var _reg1703 = head;
                add(_reg1702, _reg1703);
                var _reg1704 = args;
                var _reg1701 = $plus(_reg1702, _reg1704);
                var _reg1700 = assign(_reg1701);
                var _reg1698 = $plus(_reg1699, _reg1700);
                var _reg1697 = exprWith(_reg1698);
                var ret = _reg1697;
                var _reg1705 = ret;
                return _reg1705;
              }
            }
          }
        }
      }
    }
    var _reg1708 = expr;
    var _reg1707 = assign(_reg1708);
    var _reg1706 = exprWith(_reg1707);
    return _reg1706;
  };
  function allocReg() {
    var _reg1711 = _allocString("_reg");
    var _reg1713 = nextReg;
    var _reg1712 = toStr(_reg1713);
    var _reg1710 = $plus(_reg1711, _reg1712);
    var result = _reg1710;
    var _reg1715 = nextReg;
    var _reg1716 = _allocInt(1);
    var _reg1714 = $plus(_reg1715, _reg1716);
    decref(nextReg);
    incref(_reg1714);
    nextReg = _reg1714;
    var _reg1717 = result;
    return _reg1717;
  };
};
function jsPreambleText() {
  var _reg1719 = _allocString("// Shiv JS Preamble\r\n  // 512MB should be enough for anyone...\r\n  var _memoryLimit = 1024 * 1024 * 1024;\r\n  var _memory = new ArrayBuffer(_memoryLimit);\r\n  var _u8M = new Uint8Array(_memory);\r\n  var _i32M = new Int32Array(_memory);\r\n\r\n  var _types = {\r\n    int: 0,\r\n    list: 1,\r\n    string: 2,\r\n    dict: 3,\r\n    char: 4,\r\n    bool: 5,\r\n  };\r\n  var _typeNames = {\r\n    0: 'int',\r\n    1: 'list',\r\n    2: 'string',\r\n    3: 'dict',\r\n    4: 'char',\r\n    5: 'bool',\r\n  };\r\n  var _freeListPtr = 0;\r\n  var _allocPtr = 1024;\r\n  // header format: { nextPtr, size, refcount }\r\n  var _allocHeaderSize = 12;\r\n  // End Shiv JS Preamble\n");
  return _reg1719;
};
function alloc(size) {
  var freeCandidate = _freeListPtr;
  while (freeCandidate != 0) {
    var freeSize = _i32M[(freeCandidate >> 2) + 1];
    if (freeSize == size) {
      var ptr = freeCandidate + _allocHeaderSize;
      if (freeCandidate == _freeListPtr) {
        _freeListPtr = 0;
      } else {
        var prev = _freeListPtr;
        while (_i32M[prev >> 2] != freeCandidate) {
          prev = _i32M[prev >> 2];
        }
        _i32M[prev >> 2] = _i32M[freeCandidate >> 2];
      }
      _i32M[freeCandidate >> 2] = 0;
      _i32M[(freeCandidate >> 2) + 1] = size;
      _i32M[(freeCandidate >> 2) + 2] = 1;
      return ptr;
    }
    freeCandidate = _i32M[freeCandidate >> 2];
  }
  var ptr = _allocPtr + _allocHeaderSize;
  _i32M[_allocPtr >> 2] = 0;
  _i32M[(_allocPtr >> 2) + 1] = size;
  _i32M[(_allocPtr >> 2) + 2] = 1;
  _allocPtr += size + _allocHeaderSize;
  _allocPtr = (_allocPtr + 3) & ~3; // 4-byte alignment
  if (_allocPtr > _memoryLimit) {
    throw 'ERROR: Out of Memory';
  }
  return ptr;
};
function free(ptr) {
  var ref = _i32M[(ptr >> 2) - 1];
  if (ref > 1) {
    throw 'ERROR: freeing pointer with refcount=' + ref;
  }
  if (_loadType(ptr) === _types.list) {
    var len = _i32M[(ptr >> 2) + 2];
    for (var i = 0; i < len; ++i) {
      var off = _i32M[(ptr >> 2) + 3] >> 2;
      decref(_i32M[off + i]);
    }
  }
  _i32M[(ptr >> 2) - 1] = 0;
  var block = ptr - _allocHeaderSize;
  if (_freeListPtr == 0) {
    _freeListPtr = block;
    return;
  }
  var freePtr = _freeListPtr;
  while (_i32M[freePtr >> 2] != 0) {
    freePtr = _i32M[freePtr >> 2];
  }
  _i32M[freePtr >> 2] = block;
};
function incref(ptr) {
_i32M[(ptr >> 2) - 1] += 1;
};
function decref(ptr) {
  _i32M[(ptr >> 2) - 1] -= 1;
  if (_i32M[(ptr >> 2) - 1] === 0) {
    free(ptr);
  }
};
function isInt(ptr) {
return _allocBool(_loadType(ptr) === _types.int);
};
function isChar(ptr) {
return _allocBool(_loadType(ptr) === _types.char);
};
function isBool(ptr) {
return _allocBool(_loadType(ptr) === _types.bool);
};
function isList(ptr) {
return _allocBool(_loadType(ptr) === _types.list);
};
function isString(ptr) {
return _allocBool(_loadType(ptr) === _types.string);
};
function _typeCheck(ptr, type, func) {
  if (_loadType(ptr) != type) {
    _error(func + ' - unexpected type: ' + _typeNames[_loadType(ptr)] +
           ' , expected ' + _typeNames[type] + ': alloced memory = ' + _allocPtr);
  }
};
function _typeCheckAny(ptr, types, func) {
  if (types.indexOf(_loadType(ptr)) == -1) {
    _error(func + ' - unexpected type: ' + _loadType(ptr) +
           ' , expected one of ' + types);
  }
};
function _allocInt(val) {
  val = val || 0;
  var ptr = alloc(8);
  var off = ptr >> 2;
  _i32M[off] = _types.int;
  _i32M[off + 1] = val;
  return ptr;
};
function _loadInt(ptr) {
  _typeCheck(ptr, _types.int, '_loadInt');
  return _i32M[(ptr >> 2) + 1];
};
function _allocChar(ch) {
  var ptr = _allocInt(ch.charCodeAt(0));
  _i32M[ptr >> 2] = _types.char;
  return ptr;
};
function _loadChar(ptr) {
  _typeCheck(ptr, _types.char, '_loadChar');
  return String.fromCharCode(_i32M[(ptr >> 2) + 1]);
};
function _allocBool(x) {
  var ptr = _allocInt(x ? 1 : 0);
  _i32M[ptr >> 2] = _types.bool;
  return ptr;
};
function _loadBool(ptr) {
  _typeCheck(ptr, _types.bool, '_loadBool');
  return _i32M[(ptr >> 2) + 1];
};
function _error(msg) {
  console.trace(msg);
  throw msg;
};
function _loadType(ptr) {
return _i32M[ptr >> 2];
};
function _allocList(capacity) {
  capacity = capacity || 16;
  var ptr = alloc(16);
  var off = ptr >> 2;
  _i32M[off] = _types.list;
  _i32M[off + 1] = capacity;
  _i32M[off + 2] = 0;
  _i32M[off + 3] = alloc(4 * capacity);
  return ptr;
};
function add(ptr, item) {
  _typeCheck(ptr, _types.list, 'add');
  var off = ptr >> 2;
  var capacity = _i32M[off + 1];
  var count = _i32M[off + 2];
  if (count > capacity) {
    _error('add count > capacity')
  }
  incref(item);
  if (count == capacity) {
    // resize list
    capacity *= 2;
    var oldDataPtr = _i32M[off + 3];
    var oldDataOff = oldDataPtr >> 2;
    var newDataPtr = alloc(4 * capacity);
    var newDataOff = newDataPtr >> 2;
    _i32M[off + 1] = capacity;
    _i32M[off + 3] = newDataPtr;
    for (var i = 0; i < count; ++i) {
      _i32M[newDataOff + i] = _i32M[oldDataOff + i];
    }
    free(oldDataPtr);
  }
  var dataOff = _i32M[off + 3] >> 2;
  _i32M[dataOff + count] = item;
  _i32M[off + 2] = count + 1;
};
function del(ptr, idxPtr) {
  _typeCheck(ptr, _types.list, '_listRemove');
  var idx = _loadInt(idxPtr);
  var countOff = (ptr >> 2) + 2;
  var count = _i32M[countOff];
  var dataOff = _i32M[countOff + 1] >> 2;
  decref(_i32M[dataOff + idx]);
  for (var i = idx; i < count - 1; ++i) {
    _i32M[dataOff + i] = _i32M[dataOff + i + 1];
  }
  _i32M[countOff] = count - 1;
};
function $at$plus(lhs, rhs) {
  _typeCheck(lhs, _types.list, '_listAdd lhs');
  _typeCheck(rhs, _types.list, '_listAdd rhs');
  var lhsLen = _listLen(lhs);
  var rhsLen = _listLen(rhs);
  var size = lhsLen + rhsLen;
  var ret = _allocList(size);
  var dataStart = _i32M[(ret >> 2) + 3] >> 2;
  var lhsStart = _i32M[(lhs >> 2) + 3] >> 2;
  for (var i = 0; i < lhsLen; ++i) {
    _i32M[dataStart + i] = _i32M[lhsStart + i];
  }
  var rhsStart = _i32M[(rhs >> 2) + 3] >> 2;
  for (var i = 0; i < rhsLen; ++i) {
    _i32M[dataStart + lhsLen + i] = _i32M[rhsStart + i];
  }
  _i32M[(ret >> 2) + 2] = size;
  return ret;
};
function _listLen(ptr) {
  _typeCheck(ptr, _types.list, '_listLen');
  return _i32M[(ptr >> 2) + 2];
};
function _listFind(ptr, item) {
  _typeCheck(ptr, _types.list, _listFind);
  var len = _listLen(ptr);
  for (var i = 0; i < len; ++i) {
    var boxI = _allocInt(i);
    var elem = _index(ptr, boxI);
    var isEqPtr = $eq$eq(item, elem);
    var isEq = _loadBool(isEqPtr);
    free(boxI);
    free(isEqPtr);
    if (isEq) {
      return i;
    }
  }
  return -1;
};
function _allocString(literal) {
  var len = literal.length;
  var ptr = alloc(len + 8);
  var off = ptr >> 2;
  _i32M[off] = _types.string;
  _i32M[off + 1] = len;
  for (var i = 0; i < len; ++i) {
    _u8M[ptr + 8 + i] = literal.charCodeAt(i);
  }
  return ptr;
};
function $dollar$plus(lhs, rhs) {
  var lhsStr = _loadType(lhs) == _types.char ? _loadChar(lhs) : _loadString(lhs);
  var rhsStr = _loadType(rhs) == _types.char ? _loadChar(rhs) : _loadString(rhs);
  return _allocString(lhsStr + rhsStr);
};
function _loadString(ptr) {
  _typeCheck(ptr, _types.string, '_loadString');
  var result = '';
  var len = _i32M[(ptr >> 2) + 1];
  for (var i = 0; i < len; ++i) {
    result += String.fromCharCode(_u8M[ptr + 8 + i]);
  }
  return result;
};
function _allocDict() {
  var ptr = alloc(12);
  var off = ptr >> 2;
  _i32M[off] = _types.dict;
  _i32M[off + 1] = _allocList();
  _i32M[off + 2] = _allocList();
  return ptr;
};
function set(ptr, key, val) {
  _typeCheck(ptr, _types.dict, '_dictSet');
  var off = ptr >> 2;
  var keysPtr = _i32M[off + 1];
  var valsPtr = _i32M[off + 2];
  var idx = _listFind(keysPtr, key);
  if (idx >= 0) {
    _i32M[(_i32M[(valsPtr >> 2) + 3] >> 2) + idx] = val;
  } else {
    add(keysPtr, key);
    add(valsPtr, val);
  }
};
function hasKey(ptr, key) {
  _typeCheck(ptr, _types.dict, '_dictHasKey');
  var keysPtr = _i32M[(ptr >> 2) + 1];
  var idx = _listFind(keysPtr, key);
  return _allocBool(idx >= 0);
};
function dictValues(ptr) {
  _typeCheck(ptr, _types.dict, 'dictValues');
  return _i32M[(ptr >> 2) + 2];
};
function toStr(ptr) {
  var off = ptr >> 2;
  var type = _i32M[off];
  if (type === _types.int) {
    return _allocString(_i32M[off + 1].toString());
  } else if (type === _types.char) {
    return _allocString(_loadChar(ptr));
  } else if (type === _types.bool) {
    return _allocString(_loadBool(ptr) ? 'true' : 'false');
  } else if (type === _types.string) {
    return _allocString(_loadString(ptr));
  } else if (type == _types.list) {
    var result = '[';
    var len = _i32M[(ptr >> 2) + 2];
    var off = _i32M[(ptr >> 2) + 3] >> 2;
    for (var i = 0; i < len; ++i) {
      if (i > 0) { result += ', '; }
      var item = _i32M[off + i];
      var itemStr = toStr(item);
      result += _loadString(itemStr);
      free(itemStr);
    }
    result += ']';
    return _allocString(result);
  } else if (type == _types.dict) {
    return _allocString('{dict}')
  }
  _error('toStr - unknown type: ' + type);
};
function len(ptr) {
  var type = _loadType(ptr);
  if (type === _types.list) {
    return _allocInt(_listLen(ptr));
  } else if (type === _types.string) {
    return _allocInt(_i32M[(ptr >> 2) + 1]);
  }
  _error('_len - unsupported type: ' + type);
};
function _index(ptr, idx) {
  var type = _loadType(ptr);
  if (type === _types.list) {
    var off = _i32M[(ptr >> 2) + 3] >> 2;
    return _i32M[off + _loadInt(idx)];
  } else if (type === _types.string) {
    // returns a char
    return _allocChar(String.fromCharCode(_u8M[ptr + 8 + _loadInt(idx)]));
  } else if (type === _types.dict) {
    var keysPtr = _i32M[(ptr >> 2) + 1];
    var keyIdx = _listFind(keysPtr, idx);
    if (keyIdx < 0) {
      _error('_index - dict key not found: ' + _loadString(_toStr(idx)));
    }
    var valsPtr = _i32M[(ptr >> 2) + 2];
    var boxedKeyIdx = _allocInt(keyIdx);
    var ret = _index(valsPtr, boxedKeyIdx);
    free(boxedKeyIdx);
    return ret;
  }
  _error('_index - unsupported type: ' + type);
};
function $eq$eq(lhs, rhs) {
  var type = _loadType(lhs);
  if (type !== _loadType(rhs)) {
    // Type mismatch, not equal
    return _allocBool(false);
  }
  if (type === _types.int) {
    return _allocBool(_loadInt(lhs) === _loadInt(rhs));
  } else if (type === _types.char) {
    return _allocBool(_loadChar(lhs) === _loadChar(rhs));
  } else if (type === _types.bool) {
    return _allocBool(_loadBool(lhs) === _loadBool(rhs));
  } else if (type === _types.string) {
    return _allocBool(_loadString(lhs) === _loadString(rhs));
  } else if (type === _types.list) {
    var len = _listLen(lhs);
    if (len != _listLen(rhs)) {
      return _allocBool(false);
    }
    var lhsOff = _i32M[(lhs >> 2) + 3] >> 2;
    var rhsOff = _i32M[(rhs >> 2) + 3] >> 2;
    for (var i = 0; i < len; ++i) {
      var isEqPtr = $not$eq(_i32M[lhsOff + i], _i32M[rhsOff + i]);
      var isEq = _loadBool(isEqPtr);
      free(isEqPtr);
      if (isEq) {
        return _allocBool(false);
      }
    }
    return _allocBool(true);
  }
  _error('_eq - unknown type: ' + type);
};
function $not$eq(lhs, rhs) {
  var _reg1759 = lhs;
  var _reg1760 = rhs;
  var _reg1758 = $eq$eq(_reg1759, _reg1760);
  var _reg1757 = not(_reg1758);
  return _reg1757;
};
function not(x) {
return _allocBool(!_loadBool(x));
};
function $num$plus(lhs, rhs) {
  var _reg1764 = lhs;
  var _reg1763 = isChar(_reg1764);
  var _reg1765 = _loadBool(_reg1763);
  free(_reg1763);
  if (_reg1765) {
    var _reg1769 = lhs;
    var _reg1768 = _loadChar(_reg1769);
    var _reg1771 = rhs;
    var _reg1770 = _loadChar(_reg1771);
    var _reg1767 = $colon$plus(_reg1768, _reg1770);
    var _reg1766 = _allocChar(_reg1767);
    return _reg1766;
  } else {
    var _reg1775 = lhs;
    var _reg1774 = _loadInt(_reg1775);
    var _reg1777 = rhs;
    var _reg1776 = _loadInt(_reg1777);
    var _reg1773 = $colon$plus(_reg1774, _reg1776);
    var _reg1772 = _allocInt(_reg1773);
    return _reg1772;
  }
};
function $minus(lhs, rhs) {
  var _reg1780 = lhs;
  var _reg1779 = isChar(_reg1780);
  var _reg1781 = _loadBool(_reg1779);
  free(_reg1779);
  if (_reg1781) {
    var _reg1785 = lhs;
    var _reg1784 = _loadChar(_reg1785);
    var _reg1787 = rhs;
    var _reg1786 = _loadChar(_reg1787);
    var _reg1783 = $colon$minus(_reg1784, _reg1786);
    var _reg1782 = _allocChar(_reg1783);
    return _reg1782;
  } else {
    var _reg1791 = lhs;
    var _reg1790 = _loadInt(_reg1791);
    var _reg1793 = rhs;
    var _reg1792 = _loadInt(_reg1793);
    var _reg1789 = $colon$minus(_reg1790, _reg1792);
    var _reg1788 = _allocInt(_reg1789);
    return _reg1788;
  }
};
function $star(lhs, rhs) {
  var _reg1798 = lhs;
  var _reg1797 = _loadInt(_reg1798);
  var _reg1800 = rhs;
  var _reg1799 = _loadInt(_reg1800);
  var _reg1796 = $colon$star(_reg1797, _reg1799);
  var _reg1795 = _allocInt(_reg1796);
  return _reg1795;
};
function $div(lhs, rhs) {
  var _reg1805 = lhs;
  var _reg1804 = _loadInt(_reg1805);
  var _reg1807 = rhs;
  var _reg1806 = _loadInt(_reg1807);
  var _reg1803 = $colon$div(_reg1804, _reg1806);
  var _reg1802 = _allocInt(_reg1803);
  return _reg1802;
};
function $gt(lhs, rhs) {
  var _reg1810 = lhs;
  var _reg1809 = isChar(_reg1810);
  var _reg1811 = _loadBool(_reg1809);
  free(_reg1809);
  if (_reg1811) {
    var _reg1815 = lhs;
    var _reg1814 = _loadChar(_reg1815);
    var _reg1817 = rhs;
    var _reg1816 = _loadChar(_reg1817);
    var _reg1813 = $colon$gt(_reg1814, _reg1816);
    var _reg1812 = _allocBool(_reg1813);
    return _reg1812;
  } else {
    var _reg1821 = lhs;
    var _reg1820 = _loadInt(_reg1821);
    var _reg1823 = rhs;
    var _reg1822 = _loadInt(_reg1823);
    var _reg1819 = $colon$gt(_reg1820, _reg1822);
    var _reg1818 = _allocBool(_reg1819);
    return _reg1818;
  }
};
function $lt(lhs, rhs) {
  var _reg1826 = lhs;
  var _reg1825 = isChar(_reg1826);
  var _reg1827 = _loadBool(_reg1825);
  free(_reg1825);
  if (_reg1827) {
    var _reg1831 = lhs;
    var _reg1830 = _loadChar(_reg1831);
    var _reg1833 = rhs;
    var _reg1832 = _loadChar(_reg1833);
    var _reg1829 = $colon$lt(_reg1830, _reg1832);
    var _reg1828 = _allocBool(_reg1829);
    return _reg1828;
  } else {
    var _reg1837 = lhs;
    var _reg1836 = _loadInt(_reg1837);
    var _reg1839 = rhs;
    var _reg1838 = _loadInt(_reg1839);
    var _reg1835 = $colon$lt(_reg1836, _reg1838);
    var _reg1834 = _allocBool(_reg1835);
    return _reg1834;
  }
};
function $lt$eq(lhs, rhs) {
  var _reg1842 = lhs;
  var _reg1841 = isChar(_reg1842);
  var _reg1843 = _loadBool(_reg1841);
  free(_reg1841);
  if (_reg1843) {
    var _reg1847 = lhs;
    var _reg1846 = _loadChar(_reg1847);
    var _reg1849 = rhs;
    var _reg1848 = _loadChar(_reg1849);
    var _reg1845 = $colon$lt$eq(_reg1846, _reg1848);
    var _reg1844 = _allocBool(_reg1845);
    return _reg1844;
  } else {
    var _reg1853 = lhs;
    var _reg1852 = _loadInt(_reg1853);
    var _reg1855 = rhs;
    var _reg1854 = _loadInt(_reg1855);
    var _reg1851 = $colon$lt$eq(_reg1852, _reg1854);
    var _reg1850 = _allocBool(_reg1851);
    return _reg1850;
  }
};
function $gt$eq(lhs, rhs) {
  var _reg1858 = lhs;
  var _reg1857 = isChar(_reg1858);
  var _reg1859 = _loadBool(_reg1857);
  free(_reg1857);
  if (_reg1859) {
    var _reg1863 = lhs;
    var _reg1862 = _loadChar(_reg1863);
    var _reg1865 = rhs;
    var _reg1864 = _loadChar(_reg1865);
    var _reg1861 = $colon$gt$eq(_reg1862, _reg1864);
    var _reg1860 = _allocBool(_reg1861);
    return _reg1860;
  } else {
    var _reg1869 = lhs;
    var _reg1868 = _loadInt(_reg1869);
    var _reg1871 = rhs;
    var _reg1870 = _loadInt(_reg1871);
    var _reg1867 = $colon$gt$eq(_reg1868, _reg1870);
    var _reg1866 = _allocBool(_reg1867);
    return _reg1866;
  }
};
function $and(lhs, rhs) {
  var _reg1876 = lhs;
  var _reg1875 = _loadInt(_reg1876);
  var _reg1878 = rhs;
  var _reg1877 = _loadInt(_reg1878);
  var _reg1874 = $colon$and(_reg1875, _reg1877);
  var _reg1873 = _allocInt(_reg1874);
  return _reg1873;
};
function $bar(lhs, rhs) {
  var _reg1883 = lhs;
  var _reg1882 = _loadInt(_reg1883);
  var _reg1885 = rhs;
  var _reg1884 = _loadInt(_reg1885);
  var _reg1881 = $colon$bar(_reg1882, _reg1884);
  var _reg1880 = _allocInt(_reg1881);
  return _reg1880;
};
function $gt$gt(lhs, rhs) {
  var _reg1890 = lhs;
  var _reg1889 = _loadInt(_reg1890);
  var _reg1892 = rhs;
  var _reg1891 = _loadInt(_reg1892);
  var _reg1888 = $colon$gt$gt(_reg1889, _reg1891);
  var _reg1887 = _allocInt(_reg1888);
  return _reg1887;
};
function $lt$lt(lhs, rhs) {
  var _reg1897 = lhs;
  var _reg1896 = _loadInt(_reg1897);
  var _reg1899 = rhs;
  var _reg1898 = _loadInt(_reg1899);
  var _reg1895 = $colon$lt$lt(_reg1896, _reg1898);
  var _reg1894 = _allocInt(_reg1895);
  return _reg1894;
};
function and(lhs, rhs) {
  var _reg1904 = lhs;
  var _reg1903 = _loadBool(_reg1904);
  var _reg1906 = rhs;
  var _reg1905 = _loadBool(_reg1906);
  var _reg1902 = _and(_reg1903, _reg1905);
  var _reg1901 = _allocBool(_reg1902);
  return _reg1901;
};
function or(lhs, rhs) {
  var _reg1911 = lhs;
  var _reg1910 = _loadBool(_reg1911);
  var _reg1913 = rhs;
  var _reg1912 = _loadBool(_reg1913);
  var _reg1909 = _or(_reg1910, _reg1912);
  var _reg1908 = _allocBool(_reg1909);
  return _reg1908;
};
function exit(code) {
console.error('exiting w/ allocptr=', _allocPtr.toLocaleString());
process.exit(_loadInt(code));
};
function strToInt(str) {
return _allocInt(parseInt(_loadString(str)));
};
function charToInt(char) {
  _typeCheck(char, _types.char, 'charToInt');
  return _allocInt(_i32M[(char >> 2) + 1]);
};
function $colon$plus(lhs, rhs) {
return lhs + rhs;
};
function $colon$minus(lhs, rhs) {
return lhs - rhs;
};
function $colon$star(lhs, rhs) {
return lhs * rhs;
};
function $colon$div(lhs, rhs) {
return lhs / rhs;
};
function $colon$gt(lhs, rhs) {
return lhs > rhs;
};
function $colon$lt(lhs, rhs) {
return lhs < rhs;
};
function $colon$lt$eq(lhs, rhs) {
return lhs <= rhs;
};
function $colon$gt$eq(lhs, rhs) {
return lhs >= rhs;
};
function $colon$and(lhs, rhs) {
return lhs & rhs;
};
function $colon$bar(lhs, rhs) {
return lhs | rhs;
};
function $colon$gt$gt(lhs, rhs) {
return lhs >> rhs;
};
function $colon$lt$lt(lhs, rhs) {
return lhs << rhs;
};
function _and(lhs, rhs) {
return lhs && rhs;
};
function _or(lhs, rhs) {
return lhs || rhs;
};
function jsCodeGen(module) {
  var _reg1932 = jsPreambleText();
  var result = _reg1932;
  var _reg1934 = module;
  var _reg1933 = ssaFormConversion(_reg1934);
  var ssa = _reg1933;
  var _reg1935 = _allocInt(0);
  var i11 = _reg1935;
  while(true) {
    var _reg1937 = i11;
    var _reg1939 = ssa;
    var _reg1938 = len(_reg1939);
    var _reg1936 = $lt(_reg1937, _reg1938);
    var _reg1962 = _reg1936;
    var _reg1961 = not(_reg1962);
    var _reg1963 = _loadBool(_reg1961);
    free(_reg1961);
    if (_reg1963) {
      break;
    }
    var _reg1941 = ssa;
    var _reg1942 = i11;
    var _reg1940 = _index(_reg1941, _reg1942);
    var sexp = _reg1940;
    var _reg1945 = sexp;
    var _reg1944 = isList(_reg1945);
    var _reg1948 = sexp;
    var _reg1947 = len(_reg1948);
    var _reg1949 = _allocInt(0);
    var _reg1946 = $gt(_reg1947, _reg1949);
    var _reg1943 = and(_reg1944, _reg1946);
    var _reg1950 = _loadBool(_reg1943);
    free(_reg1943);
    if (_reg1950) {
      var _reg1953 = result;
      var _reg1955 = sexp;
      var _reg1956 = _allocInt(0);
      var _reg1954 = jsStmtStr(_reg1955, _reg1956);
      var _reg1952 = $plus(_reg1953, _reg1954);
      var _reg1957 = _allocString("\n");
      var _reg1951 = $plus(_reg1952, _reg1957);
      decref(result);
      incref(_reg1951);
      result = _reg1951;
    }
    var _reg1959 = i11;
    var _reg1960 = _allocInt(1);
    var _reg1958 = $plus(_reg1959, _reg1960);
    decref(i11);
    incref(_reg1958);
    i11 = _reg1958;
  }
  var _reg1964 = result;
  return _reg1964;
};
function jsStmtStr(sexp, indent) {
  var _reg1967 = _allocString("  ");
  var _reg1968 = indent;
  var _reg1966 = repeat(_reg1967, _reg1968);
  var indentStr = _reg1966;
  var _reg1969 = _allocString("");
  var result = _reg1969;
  var _reg1971 = sexp;
  var _reg1972 = _allocInt(0);
  var _reg1970 = _index(_reg1971, _reg1972);
  var head = _reg1970;
  var _reg1974 = head;
  var _reg1975 = _allocString("if");
  var _reg1973 = $eq$eq(_reg1974, _reg1975);
  var _reg1976 = _loadBool(_reg1973);
  free(_reg1973);
  if (_reg1976) {
    var _reg1979 = sexp;
    var _reg1980 = _allocInt(1);
    var _reg1978 = _index(_reg1979, _reg1980);
    var _reg1977 = legalizeName(_reg1978);
    var cond = _reg1977;
    var _reg1982 = _allocList();
    var _reg1983 = indentStr;
    add(_reg1982, _reg1983);
    var _reg1984 = _allocString("if (");
    add(_reg1982, _reg1984);
    var _reg1985 = cond;
    add(_reg1982, _reg1985);
    var _reg1986 = _allocString(") {\n");
    add(_reg1982, _reg1986);
    var _reg1981 = flatten(_reg1982);
    decref(result);
    incref(_reg1981);
    result = _reg1981;
    var _reg1988 = sexp;
    var _reg1989 = _allocInt(2);
    var _reg1987 = _index(_reg1988, _reg1989);
    var body = _reg1987;
    var _reg1990 = _allocInt(0);
    var i13 = _reg1990;
    while(true) {
      var _reg1992 = i13;
      var _reg1994 = body;
      var _reg1993 = len(_reg1994);
      var _reg1991 = $lt(_reg1992, _reg1993);
      var _reg2011 = _reg1991;
      var _reg2010 = not(_reg2011);
      var _reg2012 = _loadBool(_reg2010);
      free(_reg2010);
      if (_reg2012) {
        break;
      }
      var _reg1996 = body;
      var _reg1997 = i13;
      var _reg1995 = _index(_reg1996, _reg1997);
      var c = _reg1995;
      var _reg2000 = result;
      var _reg2002 = c;
      var _reg2004 = indent;
      var _reg2005 = _allocInt(1);
      var _reg2003 = $plus(_reg2004, _reg2005);
      var _reg2001 = jsStmtStr(_reg2002, _reg2003);
      var _reg1999 = $plus(_reg2000, _reg2001);
      var _reg2006 = _allocString("\n");
      var _reg1998 = $plus(_reg1999, _reg2006);
      decref(result);
      incref(_reg1998);
      result = _reg1998;
      var _reg2008 = i13;
      var _reg2009 = _allocInt(1);
      var _reg2007 = $plus(_reg2008, _reg2009);
      decref(i13);
      incref(_reg2007);
      i13 = _reg2007;
    }
    var _reg2015 = sexp;
    var _reg2014 = len(_reg2015);
    var _reg2016 = _allocInt(3);
    var _reg2013 = $gt(_reg2014, _reg2016);
    var _reg2017 = _loadBool(_reg2013);
    free(_reg2013);
    if (_reg2017) {
      var _reg2021 = sexp;
      var _reg2022 = _allocInt(3);
      var _reg2020 = _index(_reg2021, _reg2022);
      var _reg2023 = _allocString("else");
      var _reg2019 = $eq$eq(_reg2020, _reg2023);
      var _reg2025 = _allocString("Expected else after if body ");
      var _reg2027 = sexp;
      var _reg2026 = toStr(_reg2027);
      var _reg2024 = $plus(_reg2025, _reg2026);
      var _reg2018 = assert(_reg2019, _reg2024);
      var _reg2030 = result;
      var _reg2031 = indentStr;
      var _reg2029 = $plus(_reg2030, _reg2031);
      var _reg2032 = _allocString("} else {\n");
      var _reg2028 = $plus(_reg2029, _reg2032);
      decref(result);
      incref(_reg2028);
      result = _reg2028;
      var _reg2034 = sexp;
      var _reg2035 = _allocInt(4);
      var _reg2033 = _index(_reg2034, _reg2035);
      var elseBody = _reg2033;
      var _reg2036 = _allocInt(0);
      var i12 = _reg2036;
      while(true) {
        var _reg2038 = i12;
        var _reg2040 = elseBody;
        var _reg2039 = len(_reg2040);
        var _reg2037 = $lt(_reg2038, _reg2039);
        var _reg2057 = _reg2037;
        var _reg2056 = not(_reg2057);
        var _reg2058 = _loadBool(_reg2056);
        free(_reg2056);
        if (_reg2058) {
          break;
        }
        var _reg2042 = elseBody;
        var _reg2043 = i12;
        var _reg2041 = _index(_reg2042, _reg2043);
        var c = _reg2041;
        var _reg2046 = result;
        var _reg2048 = c;
        var _reg2050 = indent;
        var _reg2051 = _allocInt(1);
        var _reg2049 = $plus(_reg2050, _reg2051);
        var _reg2047 = jsStmtStr(_reg2048, _reg2049);
        var _reg2045 = $plus(_reg2046, _reg2047);
        var _reg2052 = _allocString("\n");
        var _reg2044 = $plus(_reg2045, _reg2052);
        decref(result);
        incref(_reg2044);
        result = _reg2044;
        var _reg2054 = i12;
        var _reg2055 = _allocInt(1);
        var _reg2053 = $plus(_reg2054, _reg2055);
        decref(i12);
        incref(_reg2053);
        i12 = _reg2053;
      }
    }
    var _reg2061 = result;
    var _reg2062 = indentStr;
    var _reg2060 = $plus(_reg2061, _reg2062);
    var _reg2063 = _allocString("}");
    var _reg2059 = $plus(_reg2060, _reg2063);
    decref(result);
    incref(_reg2059);
    result = _reg2059;
    var _reg2064 = result;
    return _reg2064;
  } else {
    var _reg2066 = head;
    var _reg2067 = _allocString("var");
    var _reg2065 = $eq$eq(_reg2066, _reg2067);
    var _reg2068 = _loadBool(_reg2065);
    free(_reg2065);
    if (_reg2068) {
      var _reg2072 = sexp;
      var _reg2073 = _allocInt(2);
      var _reg2071 = _index(_reg2072, _reg2073);
      var _reg2074 = _allocString("=");
      var _reg2070 = $eq$eq(_reg2071, _reg2074);
      var _reg2076 = _allocString("Expected '=' in var declaration ");
      var _reg2078 = sexp;
      var _reg2077 = toStr(_reg2078);
      var _reg2075 = $plus(_reg2076, _reg2077);
      var _reg2069 = assert(_reg2070, _reg2075);
      var _reg2080 = _allocList();
      var _reg2081 = indentStr;
      add(_reg2080, _reg2081);
      var _reg2082 = _allocString("var ");
      add(_reg2080, _reg2082);
      var _reg2084 = sexp;
      var _reg2085 = _allocInt(1);
      var _reg2083 = _index(_reg2084, _reg2085);
      add(_reg2080, _reg2083);
      var _reg2086 = _allocString(" = ");
      add(_reg2080, _reg2086);
      var _reg2089 = sexp;
      var _reg2090 = _allocInt(3);
      var _reg2088 = _index(_reg2089, _reg2090);
      var _reg2087 = legalizeName(_reg2088);
      add(_reg2080, _reg2087);
      var _reg2091 = _allocString(";");
      add(_reg2080, _reg2091);
      var _reg2079 = flatten(_reg2080);
      return _reg2079;
    } else {
      var _reg2093 = head;
      var _reg2094 = _allocString("register");
      var _reg2092 = $eq$eq(_reg2093, _reg2094);
      var _reg2095 = _loadBool(_reg2092);
      free(_reg2092);
      if (_reg2095) {
        var _reg2097 = _allocList();
        var _reg2098 = indentStr;
        add(_reg2097, _reg2098);
        var _reg2099 = _allocString("var ");
        add(_reg2097, _reg2099);
        var _reg2101 = sexp;
        var _reg2102 = _allocInt(1);
        var _reg2100 = _index(_reg2101, _reg2102);
        add(_reg2097, _reg2100);
        var _reg2103 = _allocString(" = ");
        add(_reg2097, _reg2103);
        var _reg2106 = sexp;
        var _reg2107 = _allocInt(3);
        var _reg2105 = _index(_reg2106, _reg2107);
        var _reg2104 = jsExprStr(_reg2105);
        add(_reg2097, _reg2104);
        var _reg2108 = _allocString(";");
        add(_reg2097, _reg2108);
        var _reg2096 = flatten(_reg2097);
        return _reg2096;
      } else {
        var _reg2110 = head;
        var _reg2111 = _allocString("<-");
        var _reg2109 = $eq$eq(_reg2110, _reg2111);
        var _reg2112 = _loadBool(_reg2109);
        free(_reg2109);
        if (_reg2112) {
          var _reg2114 = _allocList();
          var _reg2115 = indentStr;
          add(_reg2114, _reg2115);
          var _reg2118 = sexp;
          var _reg2119 = _allocInt(1);
          var _reg2117 = _index(_reg2118, _reg2119);
          var _reg2116 = legalizeName(_reg2117);
          add(_reg2114, _reg2116);
          var _reg2120 = _allocString(" = ");
          add(_reg2114, _reg2120);
          var _reg2123 = sexp;
          var _reg2124 = _allocInt(2);
          var _reg2122 = _index(_reg2123, _reg2124);
          var _reg2121 = legalizeName(_reg2122);
          add(_reg2114, _reg2121);
          var _reg2125 = _allocString(";");
          add(_reg2114, _reg2125);
          var _reg2113 = flatten(_reg2114);
          return _reg2113;
        } else {
          var _reg2127 = head;
          var _reg2128 = _allocString("print");
          var _reg2126 = $eq$eq(_reg2127, _reg2128);
          var _reg2129 = _loadBool(_reg2126);
          free(_reg2126);
          if (_reg2129) {
            var _reg2131 = _allocList();
            var _reg2132 = indentStr;
            add(_reg2131, _reg2132);
            var _reg2133 = _allocString("console.log(_loadString(");
            add(_reg2131, _reg2133);
            var _reg2136 = sexp;
            var _reg2137 = _allocInt(1);
            var _reg2135 = _index(_reg2136, _reg2137);
            var _reg2134 = legalizeName(_reg2135);
            add(_reg2131, _reg2134);
            var _reg2138 = _allocString("));");
            add(_reg2131, _reg2138);
            var _reg2130 = flatten(_reg2131);
            return _reg2130;
          } else {
            var _reg2140 = head;
            var _reg2141 = _allocString("loop");
            var _reg2139 = $eq$eq(_reg2140, _reg2141);
            var _reg2142 = _loadBool(_reg2139);
            free(_reg2139);
            if (_reg2142) {
              var _reg2143 = _allocString("");
              var body = _reg2143;
              var _reg2144 = _allocInt(0);
              var i14 = _reg2144;
              while(true) {
                var _reg2146 = i14;
                var _reg2149 = sexp;
                var _reg2150 = _allocInt(1);
                var _reg2148 = _index(_reg2149, _reg2150);
                var _reg2147 = len(_reg2148);
                var _reg2145 = $lt(_reg2146, _reg2147);
                var _reg2169 = _reg2145;
                var _reg2168 = not(_reg2169);
                var _reg2170 = _loadBool(_reg2168);
                free(_reg2168);
                if (_reg2170) {
                  break;
                }
                var _reg2153 = sexp;
                var _reg2154 = _allocInt(1);
                var _reg2152 = _index(_reg2153, _reg2154);
                var _reg2155 = i14;
                var _reg2151 = _index(_reg2152, _reg2155);
                var stmt = _reg2151;
                var _reg2158 = body;
                var _reg2160 = stmt;
                var _reg2162 = indent;
                var _reg2163 = _allocInt(1);
                var _reg2161 = $plus(_reg2162, _reg2163);
                var _reg2159 = jsStmtStr(_reg2160, _reg2161);
                var _reg2157 = $plus(_reg2158, _reg2159);
                var _reg2164 = _allocString("\n");
                var _reg2156 = $plus(_reg2157, _reg2164);
                decref(body);
                incref(_reg2156);
                body = _reg2156;
                var _reg2166 = i14;
                var _reg2167 = _allocInt(1);
                var _reg2165 = $plus(_reg2166, _reg2167);
                decref(i14);
                incref(_reg2165);
                i14 = _reg2165;
              }
              var _reg2172 = _allocList();
              var _reg2173 = indentStr;
              add(_reg2172, _reg2173);
              var _reg2174 = _allocString("while(true) {\n");
              add(_reg2172, _reg2174);
              var _reg2175 = body;
              add(_reg2172, _reg2175);
              var _reg2176 = indentStr;
              add(_reg2172, _reg2176);
              var _reg2177 = _allocString("}");
              add(_reg2172, _reg2177);
              var _reg2171 = flatten(_reg2172);
              return _reg2171;
            } else {
              var _reg2179 = head;
              var _reg2180 = _allocString("break");
              var _reg2178 = $eq$eq(_reg2179, _reg2180);
              var _reg2181 = _loadBool(_reg2178);
              free(_reg2178);
              if (_reg2181) {
                var _reg2183 = indentStr;
                var _reg2184 = _allocString("break;");
                var _reg2182 = $plus(_reg2183, _reg2184);
                return _reg2182;
              } else {
                var _reg2186 = head;
                var _reg2187 = _allocString("return");
                var _reg2185 = $eq$eq(_reg2186, _reg2187);
                var _reg2188 = _loadBool(_reg2185);
                free(_reg2185);
                if (_reg2188) {
                  var _reg2191 = sexp;
                  var _reg2190 = len(_reg2191);
                  var _reg2192 = _allocInt(1);
                  var _reg2189 = $gt(_reg2190, _reg2192);
                  var _reg2193 = _loadBool(_reg2189);
                  free(_reg2189);
                  if (_reg2193) {
                    var _reg2195 = _allocList();
                    var _reg2196 = indentStr;
                    add(_reg2195, _reg2196);
                    var _reg2197 = _allocString("return ");
                    add(_reg2195, _reg2197);
                    var _reg2199 = sexp;
                    var _reg2200 = _allocInt(1);
                    var _reg2198 = _index(_reg2199, _reg2200);
                    add(_reg2195, _reg2198);
                    var _reg2201 = _allocString(";");
                    add(_reg2195, _reg2201);
                    var _reg2194 = flatten(_reg2195);
                    return _reg2194;
                  } else {
                    var _reg2203 = indentStr;
                    var _reg2204 = _allocString("return;");
                    var _reg2202 = $plus(_reg2203, _reg2204);
                    return _reg2202;
                  }
                } else {
                  var _reg2206 = head;
                  var _reg2207 = _allocString("js_asm");
                  var _reg2205 = $eq$eq(_reg2206, _reg2207);
                  var _reg2208 = _loadBool(_reg2205);
                  free(_reg2205);
                  if (_reg2208) {
                    var _reg2210 = sexp;
                    var _reg2211 = _allocInt(1);
                    var _reg2209 = _index(_reg2210, _reg2211);
                    var str = _reg2209;
                    var _reg2214 = str;
                    var _reg2215 = _allocInt(1);
                    var _reg2213 = _index(_reg2214, _reg2215);
                    var _reg2216 = _allocChar("\r");
                    var _reg2212 = $eq$eq(_reg2213, _reg2216);
                    var _reg2217 = _loadBool(_reg2212);
                    free(_reg2212);
                    if (_reg2217) {
                      var _reg2219 = str;
                      var _reg2220 = _allocInt(3);
                      var _reg2223 = str;
                      var _reg2222 = len(_reg2223);
                      var _reg2224 = _allocInt(1);
                      var _reg2221 = $minus(_reg2222, _reg2224);
                      var _reg2218 = slice(_reg2219, _reg2220, _reg2221);
                      return _reg2218;
                    } else {
                      var _reg2227 = str;
                      var _reg2228 = _allocInt(1);
                      var _reg2226 = _index(_reg2227, _reg2228);
                      var _reg2229 = _allocChar("\n");
                      var _reg2225 = $eq$eq(_reg2226, _reg2229);
                      var _reg2230 = _loadBool(_reg2225);
                      free(_reg2225);
                      if (_reg2230) {
                        var _reg2232 = str;
                        var _reg2233 = _allocInt(2);
                        var _reg2236 = str;
                        var _reg2235 = len(_reg2236);
                        var _reg2237 = _allocInt(1);
                        var _reg2234 = $minus(_reg2235, _reg2237);
                        var _reg2231 = slice(_reg2232, _reg2233, _reg2234);
                        return _reg2231;
                      } else {
                        var _reg2239 = str;
                        var _reg2240 = _allocInt(1);
                        var _reg2243 = str;
                        var _reg2242 = len(_reg2243);
                        var _reg2244 = _allocInt(1);
                        var _reg2241 = $minus(_reg2242, _reg2244);
                        var _reg2238 = slice(_reg2239, _reg2240, _reg2241);
                        return _reg2238;
                      }
                    }
                  } else {
                    var _reg2247 = _allocList();
                    var _reg2248 = _allocString("fn");
                    add(_reg2247, _reg2248);
                    var _reg2249 = _allocString("add");
                    add(_reg2247, _reg2249);
                    var _reg2250 = _allocString("del");
                    add(_reg2247, _reg2250);
                    var _reg2251 = _allocString("set");
                    add(_reg2247, _reg2251);
                    var _reg2252 = _allocString("free");
                    add(_reg2247, _reg2252);
                    var _reg2253 = _allocString("incref");
                    add(_reg2247, _reg2253);
                    var _reg2254 = _allocString("decref");
                    add(_reg2247, _reg2254);
                    var _reg2255 = head;
                    var _reg2246 = find(_reg2247, _reg2255);
                    var _reg2256 = _allocInt(-1);
                    var _reg2245 = $not$eq(_reg2246, _reg2256);
                    var _reg2257 = _loadBool(_reg2245);
                    free(_reg2245);
                    if (_reg2257) {
                      var _reg2259 = _allocList();
                      var _reg2260 = indentStr;
                      add(_reg2259, _reg2260);
                      var _reg2262 = sexp;
                      var _reg2261 = jsExprStr(_reg2262);
                      add(_reg2259, _reg2261);
                      var _reg2263 = _allocString(";");
                      add(_reg2259, _reg2263);
                      var _reg2258 = flatten(_reg2259);
                      return _reg2258;
                    } else {
                      var _reg2265 = head;
                      var _reg2266 = _allocString("pass");
                      var _reg2264 = $eq$eq(_reg2265, _reg2266);
                      var _reg2267 = _loadBool(_reg2264);
                      free(_reg2264);
                      if (_reg2267) {
                        var _reg2268 = _allocString("");
                        return _reg2268;
                      } else {
                        var _reg2270 = _allocBool(false);
                        var _reg2272 = _allocString("Unknown statement: ");
                        var _reg2274 = sexp;
                        var _reg2273 = prettySexpr(_reg2274);
                        var _reg2271 = $plus(_reg2272, _reg2273);
                        var _reg2269 = assert(_reg2270, _reg2271);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  function jsExprStr(sexp) {
    var _reg2277 = sexp;
    var _reg2278 = indent;
    var _reg2276 = jsExprStrIndent(_reg2277, _reg2278);
    return _reg2276;
  };
};
function jsExprStrIndent(sexp, indent) {
  var _reg2282 = sexp;
  var _reg2281 = isList(_reg2282);
  var _reg2280 = not(_reg2281);
  var _reg2283 = _loadBool(_reg2280);
  free(_reg2280);
  if (_reg2283) {
    var _reg2287 = sexp;
    var _reg2288 = _allocInt(0);
    var _reg2286 = _index(_reg2287, _reg2288);
    var _reg2285 = isDigit(_reg2286);
    var _reg2292 = sexp;
    var _reg2293 = _allocInt(0);
    var _reg2291 = _index(_reg2292, _reg2293);
    var _reg2294 = _allocChar("-");
    var _reg2290 = $eq$eq(_reg2291, _reg2294);
    var _reg2297 = sexp;
    var _reg2298 = _allocInt(1);
    var _reg2296 = _index(_reg2297, _reg2298);
    var _reg2295 = isDigit(_reg2296);
    var _reg2289 = and(_reg2290, _reg2295);
    var _reg2284 = or(_reg2285, _reg2289);
    var _reg2299 = _loadBool(_reg2284);
    free(_reg2284);
    if (_reg2299) {
      var _reg2302 = _allocString("_allocInt(");
      var _reg2303 = sexp;
      var _reg2301 = $plus(_reg2302, _reg2303);
      var _reg2304 = _allocString(")");
      var _reg2300 = $plus(_reg2301, _reg2304);
      return _reg2300;
    }
    var _reg2306 = sexp;
    var _reg2305 = legalizeName(_reg2306);
    return _reg2305;
  }
  var _reg2308 = sexp;
  var _reg2309 = _allocInt(0);
  var _reg2307 = _index(_reg2308, _reg2309);
  var head = _reg2307;
  var _reg2311 = head;
  var _reg2312 = _allocString("fn");
  var _reg2310 = $eq$eq(_reg2311, _reg2312);
  var _reg2313 = _loadBool(_reg2310);
  free(_reg2310);
  if (_reg2313) {
    var _reg2314 = _allocString("");
    var name = _reg2314;
    var _reg2315 = _allocInt(0);
    var nameOffset = _reg2315;
    var _reg2317 = _allocString("  ");
    var _reg2318 = indent;
    var _reg2316 = repeat(_reg2317, _reg2318);
    var indentStr = _reg2316;
    var _reg2321 = sexp;
    var _reg2322 = _allocInt(1);
    var _reg2320 = _index(_reg2321, _reg2322);
    var _reg2319 = isList(_reg2320);
    var isAnonymous = _reg2319;
    var _reg2324 = isAnonymous;
    var _reg2323 = not(_reg2324);
    var _reg2325 = _loadBool(_reg2323);
    free(_reg2323);
    if (_reg2325) {
      var _reg2327 = _allocString(" ");
      var _reg2330 = sexp;
      var _reg2331 = _allocInt(1);
      var _reg2329 = _index(_reg2330, _reg2331);
      var _reg2328 = legalizeName(_reg2329);
      var _reg2326 = $plus(_reg2327, _reg2328);
      decref(name);
      incref(_reg2326);
      name = _reg2326;
      var _reg2332 = _allocInt(1);
      decref(nameOffset);
      incref(_reg2332);
      nameOffset = _reg2332;
    }
    var _reg2335 = sexp;
    var _reg2337 = _allocInt(1);
    var _reg2338 = nameOffset;
    var _reg2336 = $plus(_reg2337, _reg2338);
    var _reg2334 = _index(_reg2335, _reg2336);
    var _reg2339 = _allocString(", ");
    var _reg2333 = join(_reg2334, _reg2339);
    var args = _reg2333;
    var _reg2341 = _allocList();
    var _reg2342 = _allocString("function");
    add(_reg2341, _reg2342);
    var _reg2343 = name;
    add(_reg2341, _reg2343);
    var _reg2344 = _allocString("(");
    add(_reg2341, _reg2344);
    var _reg2345 = args;
    add(_reg2341, _reg2345);
    var _reg2346 = _allocString(") {\n");
    add(_reg2341, _reg2346);
    var _reg2340 = flatten(_reg2341);
    decref(result);
    incref(_reg2340);
    result = _reg2340;
    var _reg2348 = sexp;
    var _reg2350 = _allocInt(2);
    var _reg2351 = nameOffset;
    var _reg2349 = $plus(_reg2350, _reg2351);
    var _reg2347 = _index(_reg2348, _reg2349);
    var body = _reg2347;
    var _reg2352 = _allocInt(0);
    var i15 = _reg2352;
    while(true) {
      var _reg2354 = i15;
      var _reg2356 = body;
      var _reg2355 = len(_reg2356);
      var _reg2353 = $lt(_reg2354, _reg2355);
      var _reg2373 = _reg2353;
      var _reg2372 = not(_reg2373);
      var _reg2374 = _loadBool(_reg2372);
      free(_reg2372);
      if (_reg2374) {
        break;
      }
      var _reg2358 = body;
      var _reg2359 = i15;
      var _reg2357 = _index(_reg2358, _reg2359);
      var c = _reg2357;
      var _reg2362 = result;
      var _reg2364 = c;
      var _reg2366 = indent;
      var _reg2367 = _allocInt(1);
      var _reg2365 = $plus(_reg2366, _reg2367);
      var _reg2363 = jsStmtStr(_reg2364, _reg2365);
      var _reg2361 = $plus(_reg2362, _reg2363);
      var _reg2368 = _allocString("\n");
      var _reg2360 = $plus(_reg2361, _reg2368);
      decref(result);
      incref(_reg2360);
      result = _reg2360;
      var _reg2370 = i15;
      var _reg2371 = _allocInt(1);
      var _reg2369 = $plus(_reg2370, _reg2371);
      decref(i15);
      incref(_reg2369);
      i15 = _reg2369;
    }
    var _reg2377 = result;
    var _reg2378 = indentStr;
    var _reg2376 = $plus(_reg2377, _reg2378);
    var _reg2379 = _allocString("}");
    var _reg2375 = $plus(_reg2376, _reg2379);
    decref(result);
    incref(_reg2375);
    result = _reg2375;
    var _reg2380 = result;
    return _reg2380;
  }
  var _reg2382 = head;
  var _reg2383 = _allocString("string");
  var _reg2381 = $eq$eq(_reg2382, _reg2383);
  var _reg2384 = _loadBool(_reg2381);
  free(_reg2381);
  if (_reg2384) {
    var _reg2387 = _allocString("_allocString(");
    var _reg2390 = sexp;
    var _reg2391 = _allocInt(1);
    var _reg2389 = _index(_reg2390, _reg2391);
    var _reg2388 = escapeString(_reg2389);
    var _reg2386 = $plus(_reg2387, _reg2388);
    var _reg2392 = _allocString(")");
    var _reg2385 = $plus(_reg2386, _reg2392);
    return _reg2385;
  } else {
    var _reg2394 = head;
    var _reg2395 = _allocString("char");
    var _reg2393 = $eq$eq(_reg2394, _reg2395);
    var _reg2396 = _loadBool(_reg2393);
    free(_reg2393);
    if (_reg2396) {
      var _reg2399 = _allocString("_allocChar(");
      var _reg2402 = sexp;
      var _reg2403 = _allocInt(1);
      var _reg2401 = _index(_reg2402, _reg2403);
      var _reg2400 = escapeString(_reg2401);
      var _reg2398 = $plus(_reg2399, _reg2400);
      var _reg2404 = _allocString(")");
      var _reg2397 = $plus(_reg2398, _reg2404);
      return _reg2397;
    } else {
      var _reg2406 = head;
      var _reg2407 = _allocString("js_asm");
      var _reg2405 = $eq$eq(_reg2406, _reg2407);
      var _reg2408 = _loadBool(_reg2405);
      free(_reg2405);
      if (_reg2408) {
        var _reg2410 = sexp;
        var _reg2411 = _allocInt(1);
        var _reg2409 = _index(_reg2410, _reg2411);
        var str = _reg2409;
        var _reg2413 = str;
        var _reg2414 = _allocInt(1);
        var _reg2417 = str;
        var _reg2416 = len(_reg2417);
        var _reg2418 = _allocInt(1);
        var _reg2415 = $minus(_reg2416, _reg2418);
        var _reg2412 = slice(_reg2413, _reg2414, _reg2415);
        return _reg2412;
      }
    }
  }
  var _reg2421 = head;
  var _reg2422 = _allocString("object");
  var _reg2420 = $eq$eq(_reg2421, _reg2422);
  var _reg2424 = head;
  var _reg2425 = _allocString("dict");
  var _reg2423 = $eq$eq(_reg2424, _reg2425);
  var _reg2419 = or(_reg2420, _reg2423);
  var _reg2426 = _loadBool(_reg2419);
  free(_reg2419);
  if (_reg2426) {
    var _reg2427 = _allocString("{");
    var result = _reg2427;
    var _reg2429 = sexp;
    var _reg2430 = _allocInt(1);
    var _reg2428 = _index(_reg2429, _reg2430);
    var items = _reg2428;
    var _reg2431 = _allocInt(0);
    var i16 = _reg2431;
    while(true) {
      var _reg2433 = i16;
      var _reg2435 = items;
      var _reg2434 = len(_reg2435);
      var _reg2432 = $lt(_reg2433, _reg2434);
      var _reg2462 = _reg2432;
      var _reg2461 = not(_reg2462);
      var _reg2463 = _loadBool(_reg2461);
      free(_reg2461);
      if (_reg2463) {
        break;
      }
      var _reg2437 = items;
      var _reg2438 = i16;
      var _reg2436 = _index(_reg2437, _reg2438);
      var item = _reg2436;
      var _reg2442 = item;
      var _reg2441 = len(_reg2442);
      var _reg2443 = _allocInt(2);
      var _reg2440 = $eq$eq(_reg2441, _reg2443);
      var _reg2444 = _allocString("Object must be built with key-value pairs");
      var _reg2439 = assert(_reg2440, _reg2444);
      var _reg2446 = _allocList();
      var _reg2447 = result;
      add(_reg2446, _reg2447);
      var _reg2450 = item;
      var _reg2451 = _allocInt(0);
      var _reg2449 = _index(_reg2450, _reg2451);
      var _reg2448 = legalizeName(_reg2449);
      add(_reg2446, _reg2448);
      var _reg2452 = _allocString(": _getVal(");
      add(_reg2446, _reg2452);
      var _reg2455 = item;
      var _reg2456 = _allocInt(1);
      var _reg2454 = _index(_reg2455, _reg2456);
      var _reg2453 = legalizeName(_reg2454);
      add(_reg2446, _reg2453);
      var _reg2457 = _allocString("), ");
      add(_reg2446, _reg2457);
      var _reg2445 = flatten(_reg2446);
      decref(result);
      incref(_reg2445);
      result = _reg2445;
      var _reg2459 = i16;
      var _reg2460 = _allocInt(1);
      var _reg2458 = $plus(_reg2459, _reg2460);
      decref(i16);
      incref(_reg2458);
      i16 = _reg2458;
    }
    var _reg2465 = result;
    var _reg2466 = _allocString("}");
    var _reg2464 = $plus(_reg2465, _reg2466);
    decref(result);
    incref(_reg2464);
    result = _reg2464;
    var _reg2467 = result;
    return _reg2467;
  }
  var _reg2469 = head;
  var _reg2468 = legalizeName(_reg2469);
  var fnName = _reg2468;
  var _reg2471 = legalizeName;
  var _reg2473 = sexp;
  var _reg2474 = _allocInt(1);
  var _reg2472 = sliceFrom(_reg2473, _reg2474);
  var _reg2470 = map(_reg2471, _reg2472);
  var argStr = _reg2470;
  var _reg2476 = argStr;
  var _reg2477 = _allocString(", ");
  var _reg2475 = join(_reg2476, _reg2477);
  var args = _reg2475;
  var _reg2479 = head;
  var _reg2480 = _allocString("__list");
  var _reg2478 = $eq$eq(_reg2479, _reg2480);
  var _reg2481 = _loadBool(_reg2478);
  free(_reg2478);
  if (_reg2481) {
    var _reg2484 = _allocString("[");
    var _reg2485 = args;
    var _reg2483 = $plus(_reg2484, _reg2485);
    var _reg2486 = _allocString("]");
    var _reg2482 = $plus(_reg2483, _reg2486);
    return _reg2482;
  }
  var _reg2490 = fnName;
  var _reg2491 = _allocString("(");
  var _reg2489 = $plus(_reg2490, _reg2491);
  var _reg2492 = args;
  var _reg2488 = $plus(_reg2489, _reg2492);
  var _reg2493 = _allocString(")");
  var _reg2487 = $plus(_reg2488, _reg2493);
  return _reg2487;
};
function legalizeName(name) {
  var _reg2497 = name;
  var _reg2498 = _allocInt(0);
  var _reg2496 = _index(_reg2497, _reg2498);
  var _reg2499 = _allocChar("\"");
  var _reg2495 = $eq$eq(_reg2496, _reg2499);
  var _reg2500 = _loadBool(_reg2495);
  free(_reg2495);
  if (_reg2500) {
    var _reg2502 = name;
    var _reg2501 = escapeString(_reg2502);
    return _reg2501;
  }
  var _reg2505 = name;
  var _reg2506 = _allocInt(0);
  var _reg2504 = _index(_reg2505, _reg2506);
  var _reg2507 = _allocChar("'");
  var _reg2503 = $eq$eq(_reg2504, _reg2507);
  var _reg2508 = _loadBool(_reg2503);
  free(_reg2503);
  if (_reg2508) {
    var _reg2510 = name;
    var _reg2511 = _allocInt(1);
    var _reg2509 = _index(_reg2510, _reg2511);
    var c = _reg2509;
    var _reg2514 = _allocString("\"");
    var _reg2516 = c;
    var _reg2515 = escapeChar(_reg2516);
    var _reg2513 = $plus(_reg2514, _reg2515);
    var _reg2517 = _allocString("\"");
    var _reg2512 = $plus(_reg2513, _reg2517);
    return _reg2512;
  }
  var _reg2518 = _allocDict();
  var _reg2519 = _allocString("__index");
  var _reg2520 = _allocString("_index");
  set(_reg2518, _reg2519, _reg2520);
  var _reg2521 = _allocString("^&");
  var _reg2522 = _allocString("_allocInt");
  set(_reg2518, _reg2521, _reg2522);
  var _reg2523 = _allocString("^*");
  var _reg2524 = _allocString("_loadInt");
  set(_reg2518, _reg2523, _reg2524);
  var _reg2525 = _allocString("true");
  var _reg2526 = _allocString("_allocBool(true)");
  set(_reg2518, _reg2525, _reg2526);
  var _reg2527 = _allocString("false");
  var _reg2528 = _allocString("_allocBool(false)");
  set(_reg2518, _reg2527, _reg2528);
  var libcalls = _reg2518;
  var _reg2530 = libcalls;
  var _reg2531 = name;
  var _reg2529 = hasKey(_reg2530, _reg2531);
  var _reg2532 = _loadBool(_reg2529);
  free(_reg2529);
  if (_reg2532) {
    var _reg2534 = libcalls;
    var _reg2535 = name;
    var _reg2533 = _index(_reg2534, _reg2535);
    return _reg2533;
  }
  var _reg2540 = name;
  var _reg2541 = _allocInt(0);
  var _reg2539 = _index(_reg2540, _reg2541);
  var _reg2542 = _allocChar("-");
  var _reg2538 = $eq$eq(_reg2539, _reg2542);
  var _reg2545 = name;
  var _reg2544 = len(_reg2545);
  var _reg2546 = _allocInt(1);
  var _reg2543 = $gt(_reg2544, _reg2546);
  var _reg2537 = and(_reg2538, _reg2543);
  var _reg2549 = name;
  var _reg2550 = _allocInt(1);
  var _reg2548 = _index(_reg2549, _reg2550);
  var _reg2547 = isDigit(_reg2548);
  var _reg2536 = and(_reg2537, _reg2547);
  var _reg2551 = _loadBool(_reg2536);
  free(_reg2536);
  if (_reg2551) {
    var _reg2552 = name;
    return _reg2552;
  }
  var _reg2553 = _allocString("");
  var result = _reg2553;
  var _reg2554 = _allocDict();
  var _reg2555 = _allocChar(".");
  var _reg2556 = _allocString("$dot");
  set(_reg2554, _reg2555, _reg2556);
  var _reg2557 = _allocChar(",");
  var _reg2558 = _allocString("$comma");
  set(_reg2554, _reg2557, _reg2558);
  var _reg2559 = _allocChar("+");
  var _reg2560 = _allocString("$plus");
  set(_reg2554, _reg2559, _reg2560);
  var _reg2561 = _allocChar("-");
  var _reg2562 = _allocString("$minus");
  set(_reg2554, _reg2561, _reg2562);
  var _reg2563 = _allocChar("*");
  var _reg2564 = _allocString("$star");
  set(_reg2554, _reg2563, _reg2564);
  var _reg2565 = _allocChar("/");
  var _reg2566 = _allocString("$div");
  set(_reg2554, _reg2565, _reg2566);
  var _reg2567 = _allocChar("=");
  var _reg2568 = _allocString("$eq");
  set(_reg2554, _reg2567, _reg2568);
  var _reg2569 = _allocChar("|");
  var _reg2570 = _allocString("$bar");
  set(_reg2554, _reg2569, _reg2570);
  var _reg2571 = _allocChar("!");
  var _reg2572 = _allocString("$not");
  set(_reg2554, _reg2571, _reg2572);
  var _reg2573 = _allocChar("@");
  var _reg2574 = _allocString("$at");
  set(_reg2554, _reg2573, _reg2574);
  var _reg2575 = _allocChar("#");
  var _reg2576 = _allocString("$num");
  set(_reg2554, _reg2575, _reg2576);
  var _reg2577 = _allocChar("$");
  var _reg2578 = _allocString("$dollar");
  set(_reg2554, _reg2577, _reg2578);
  var _reg2579 = _allocChar("%");
  var _reg2580 = _allocString("$pct");
  set(_reg2554, _reg2579, _reg2580);
  var _reg2581 = _allocChar("^");
  var _reg2582 = _allocString("$carat");
  set(_reg2554, _reg2581, _reg2582);
  var _reg2583 = _allocChar("&");
  var _reg2584 = _allocString("$and");
  set(_reg2554, _reg2583, _reg2584);
  var _reg2585 = _allocChar("<");
  var _reg2586 = _allocString("$lt");
  set(_reg2554, _reg2585, _reg2586);
  var _reg2587 = _allocChar(">");
  var _reg2588 = _allocString("$gt");
  set(_reg2554, _reg2587, _reg2588);
  var _reg2589 = _allocChar("`");
  var _reg2590 = _allocString("$tick");
  set(_reg2554, _reg2589, _reg2590);
  var _reg2591 = _allocChar("~");
  var _reg2592 = _allocString("$tilde");
  set(_reg2554, _reg2591, _reg2592);
  var _reg2593 = _allocChar(":");
  var _reg2594 = _allocString("$colon");
  set(_reg2554, _reg2593, _reg2594);
  var illegal = _reg2554;
  var _reg2595 = _allocInt(0);
  var i17 = _reg2595;
  while(true) {
    var _reg2597 = i17;
    var _reg2599 = name;
    var _reg2598 = len(_reg2599);
    var _reg2596 = $lt(_reg2597, _reg2598);
    var _reg2619 = _reg2596;
    var _reg2618 = not(_reg2619);
    var _reg2620 = _loadBool(_reg2618);
    free(_reg2618);
    if (_reg2620) {
      break;
    }
    var _reg2601 = name;
    var _reg2602 = i17;
    var _reg2600 = _index(_reg2601, _reg2602);
    var c = _reg2600;
    var _reg2604 = illegal;
    var _reg2605 = c;
    var _reg2603 = hasKey(_reg2604, _reg2605);
    var _reg2606 = _loadBool(_reg2603);
    free(_reg2603);
    if (_reg2606) {
      var _reg2608 = result;
      var _reg2610 = illegal;
      var _reg2611 = c;
      var _reg2609 = _index(_reg2610, _reg2611);
      var _reg2607 = $plus(_reg2608, _reg2609);
      decref(result);
      incref(_reg2607);
      result = _reg2607;
    } else {
      var _reg2613 = result;
      var _reg2614 = c;
      var _reg2612 = $plus(_reg2613, _reg2614);
      decref(result);
      incref(_reg2612);
      result = _reg2612;
    }
    var _reg2616 = i17;
    var _reg2617 = _allocInt(1);
    var _reg2615 = $plus(_reg2616, _reg2617);
    decref(i17);
    incref(_reg2615);
    i17 = _reg2615;
  }
  var _reg2621 = result;
  return _reg2621;
};
function escapeChar(c) {
  var _reg2624 = c;
  var _reg2625 = _allocChar("\"");
  var _reg2623 = $eq$eq(_reg2624, _reg2625);
  var _reg2626 = _loadBool(_reg2623);
  free(_reg2623);
  if (_reg2626) {
    var _reg2627 = _allocString("\\\"");
    return _reg2627;
  } else {
    var _reg2629 = c;
    var _reg2630 = _allocChar("\\");
    var _reg2628 = $eq$eq(_reg2629, _reg2630);
    var _reg2631 = _loadBool(_reg2628);
    free(_reg2628);
    if (_reg2631) {
      var _reg2632 = _allocString("\\\\");
      return _reg2632;
    } else {
      var _reg2634 = c;
      var _reg2635 = _allocChar("\n");
      var _reg2633 = $eq$eq(_reg2634, _reg2635);
      var _reg2636 = _loadBool(_reg2633);
      free(_reg2633);
      if (_reg2636) {
        var _reg2637 = _allocString("\\n");
        return _reg2637;
      } else {
        var _reg2639 = c;
        var _reg2640 = _allocChar("\r");
        var _reg2638 = $eq$eq(_reg2639, _reg2640);
        var _reg2641 = _loadBool(_reg2638);
        free(_reg2638);
        if (_reg2641) {
          var _reg2642 = _allocString("\\r");
          return _reg2642;
        } else {
          var _reg2643 = c;
          return _reg2643;
        }
      }
    }
  }
};
function escapeString(name) {
  var _reg2645 = _allocString("\"");
  var str = _reg2645;
  var _reg2646 = _allocInt(0);
  var i18 = _reg2646;
  while(true) {
    var _reg2648 = i18;
    var _reg2650 = name;
    var _reg2649 = len(_reg2650);
    var _reg2647 = $lt(_reg2648, _reg2649);
    var _reg2662 = _reg2647;
    var _reg2661 = not(_reg2662);
    var _reg2663 = _loadBool(_reg2661);
    free(_reg2661);
    if (_reg2663) {
      break;
    }
    var _reg2652 = name;
    var _reg2653 = i18;
    var _reg2651 = _index(_reg2652, _reg2653);
    var c = _reg2651;
    var _reg2655 = str;
    var _reg2657 = c;
    var _reg2656 = escapeChar(_reg2657);
    var _reg2654 = $plus(_reg2655, _reg2656);
    decref(str);
    incref(_reg2654);
    str = _reg2654;
    var _reg2659 = i18;
    var _reg2660 = _allocInt(1);
    var _reg2658 = $plus(_reg2659, _reg2660);
    decref(i18);
    incref(_reg2658);
    i18 = _reg2658;
  }
  var _reg2665 = str;
  var _reg2666 = _allocString("\"");
  var _reg2664 = $plus(_reg2665, _reg2666);
  decref(str);
  incref(_reg2664);
  str = _reg2664;
  var _reg2667 = str;
  return _reg2667;
};
var _reg2668 = _allocBool(false);
var debugShivc = _reg2668;
function preprocess(module, sourceFilename, extraIncludeDirs) {
  var _reg2670 = debugShivc;
  var _reg2671 = _loadBool(_reg2670);
  if (_reg2671) {
    var _reg2673 = _allocString("After parsing:\n");
    var _reg2672 = toStr(_reg2673);
    var _reg2676 = module;
    var _reg2675 = prettySexpr(_reg2676);
    var _reg2674 = toStr(_reg2675);
    var _reg2678 = _reg2672;
    var _reg2679 = _reg2674;
    var _reg2677 = $dollar$plus(_reg2678, _reg2679);
    free(_reg2674);
    free(_reg2672);
    console.log(_loadString(_reg2677));
    free(_reg2677);
  }
  var _reg2681 = module;
  var _reg2680 = handleIncludes(_reg2681);
  decref(module);
  incref(_reg2680);
  module = _reg2680;
  var _reg2682 = debugShivc;
  var _reg2683 = _loadBool(_reg2682);
  if (_reg2683) {
    var _reg2685 = _allocString("After includes:\n");
    var _reg2684 = toStr(_reg2685);
    var _reg2688 = module;
    var _reg2687 = prettySexpr(_reg2688);
    var _reg2686 = toStr(_reg2687);
    var _reg2690 = _reg2684;
    var _reg2691 = _reg2686;
    var _reg2689 = $dollar$plus(_reg2690, _reg2691);
    free(_reg2686);
    free(_reg2684);
    console.log(_loadString(_reg2689));
    free(_reg2689);
  }
  var _reg2692 = runPasses();
  var _reg2693 = debugShivc;
  var _reg2694 = _loadBool(_reg2693);
  if (_reg2694) {
    var _reg2696 = _allocString("After passes:\n");
    var _reg2695 = toStr(_reg2696);
    var _reg2699 = module;
    var _reg2698 = prettySexpr(_reg2699);
    var _reg2697 = toStr(_reg2698);
    var _reg2701 = _reg2695;
    var _reg2702 = _reg2697;
    var _reg2700 = $dollar$plus(_reg2701, _reg2702);
    free(_reg2697);
    free(_reg2695);
    console.log(_loadString(_reg2700));
    free(_reg2700);
  }
  var _reg2703 = module;
  return _reg2703;
  function handleIncludes(module) {
    var _reg2705 = _allocDict();
    var included = _reg2705;
    var _reg2706 = currentFile();
    var shivcPath = _reg2706;
    var _reg2709 = shivcPath;
    var _reg2708 = parentDir(_reg2709);
    var _reg2707 = parentDir(_reg2708);
    var shivDir = _reg2707;
    var _reg2711 = shivDir;
    var _reg2712 = _allocString("include");
    var _reg2710 = joinPaths(_reg2711, _reg2712);
    var systemBinInclude = _reg2710;
    var _reg2715 = shivcPath;
    var _reg2714 = parentDir(_reg2715);
    var _reg2716 = _allocString("include");
    var _reg2713 = joinPaths(_reg2714, _reg2716);
    var systemInclude = _reg2713;
    var _reg2718 = sourceFilename;
    var _reg2717 = parentDir(_reg2718);
    var sourceDir = _reg2717;
    var _reg2720 = _allocList();
    var _reg2721 = sourceDir;
    add(_reg2720, _reg2721);
    var _reg2722 = systemBinInclude;
    add(_reg2720, _reg2722);
    var _reg2723 = systemInclude;
    add(_reg2720, _reg2723);
    var _reg2724 = extraIncludeDirs;
    var _reg2719 = $plus(_reg2720, _reg2724);
    var includeDirs = _reg2719;
    var _reg2725 = _allocInt(0);
    var i = _reg2725;
    while(true) {
      var _reg2727 = i;
      var _reg2729 = module;
      var _reg2728 = len(_reg2729);
      var _reg2726 = $lt(_reg2727, _reg2728);
      var _reg2812 = _reg2726;
      var _reg2811 = not(_reg2812);
      var _reg2813 = _loadBool(_reg2811);
      free(_reg2811);
      if (_reg2813) {
        break;
      }
      var _reg2731 = module;
      var _reg2732 = i;
      var _reg2730 = _index(_reg2731, _reg2732);
      var sexp = _reg2730;
      var _reg2734 = i;
      var _reg2735 = _allocInt(1);
      var _reg2733 = $plus(_reg2734, _reg2735);
      decref(i);
      incref(_reg2733);
      i = _reg2733;
      var _reg2737 = sexp;
      var _reg2738 = _allocString("include");
      var _reg2736 = isListStartingWith(_reg2737, _reg2738);
      var _reg2739 = _loadBool(_reg2736);
      free(_reg2736);
      if (_reg2739) {
        var _reg2740 = module;
        var _reg2742 = i;
        var _reg2743 = _allocInt(1);
        var _reg2741 = $minus(_reg2742, _reg2743);
        del(_reg2740, _reg2741);
        var _reg2746 = sexp;
        var _reg2747 = _allocInt(1);
        var _reg2745 = _index(_reg2746, _reg2747);
        var _reg2748 = _allocString(".shv");
        var _reg2744 = $plus(_reg2745, _reg2748);
        var filename = _reg2744;
        var _reg2749 = _allocString("");
        var path = _reg2749;
        var _reg2750 = _allocInt(0);
        var i19 = _reg2750;
        while(true) {
          var _reg2752 = i19;
          var _reg2754 = includeDirs;
          var _reg2753 = len(_reg2754);
          var _reg2751 = $lt(_reg2752, _reg2753);
          var _reg2768 = _reg2751;
          var _reg2767 = not(_reg2768);
          var _reg2769 = _loadBool(_reg2767);
          free(_reg2767);
          if (_reg2769) {
            break;
          }
          var _reg2756 = includeDirs;
          var _reg2757 = i19;
          var _reg2755 = _index(_reg2756, _reg2757);
          var dir = _reg2755;
          var _reg2759 = dir;
          var _reg2760 = filename;
          var _reg2758 = joinPaths(_reg2759, _reg2760);
          decref(path);
          incref(_reg2758);
          path = _reg2758;
          var _reg2762 = path;
          var _reg2761 = fileExists(_reg2762);
          var _reg2763 = _loadBool(_reg2761);
          free(_reg2761);
          if (_reg2763) {
            break;
          }
          var _reg2765 = i19;
          var _reg2766 = _allocInt(1);
          var _reg2764 = $plus(_reg2765, _reg2766);
          decref(i19);
          incref(_reg2764);
          i19 = _reg2764;
        }
        var _reg2772 = path;
        var _reg2771 = fileExists(_reg2772);
        var _reg2776 = _allocString("File ");
        var _reg2777 = filename;
        var _reg2775 = $plus(_reg2776, _reg2777);
        var _reg2778 = _allocString(" not found\n  search paths: ");
        var _reg2774 = $plus(_reg2775, _reg2778);
        var _reg2780 = includeDirs;
        var _reg2779 = toStr(_reg2780);
        var _reg2773 = $plus(_reg2774, _reg2779);
        var _reg2770 = assert(_reg2771, _reg2773);
        var _reg2783 = included;
        var _reg2784 = path;
        var _reg2782 = hasKey(_reg2783, _reg2784);
        var _reg2781 = not(_reg2782);
        var _reg2785 = _loadBool(_reg2781);
        free(_reg2781);
        if (_reg2785) {
          var _reg2787 = included;
          var _reg2788 = path;
          var _reg2789 = _allocBool(true);
          var _reg2786 = set(_reg2787, _reg2788, _reg2789);
          var _reg2791 = path;
          var _reg2790 = readFile(_reg2791);
          var contents = _reg2790;
          var _reg2793 = contents;
          var _reg2792 = parseSexprs(_reg2793);
          var sexprs = _reg2792;
          var _reg2797 = module;
          var _reg2798 = _allocInt(0);
          var _reg2800 = i;
          var _reg2801 = _allocInt(1);
          var _reg2799 = $minus(_reg2800, _reg2801);
          var _reg2796 = slice(_reg2797, _reg2798, _reg2799);
          var _reg2802 = sexprs;
          var _reg2795 = $plus(_reg2796, _reg2802);
          var _reg2804 = module;
          var _reg2806 = i;
          var _reg2807 = _allocInt(1);
          var _reg2805 = $minus(_reg2806, _reg2807);
          var _reg2809 = module;
          var _reg2808 = len(_reg2809);
          var _reg2803 = slice(_reg2804, _reg2805, _reg2808);
          var _reg2794 = $plus(_reg2795, _reg2803);
          decref(module);
          incref(_reg2794);
          module = _reg2794;
        }
        var _reg2810 = _allocInt(0);
        decref(i);
        incref(_reg2810);
        i = _reg2810;
      }
    }
    var _reg2814 = module;
    return _reg2814;
  };
  function runPasses() {
    var _reg2816 = _allocDict();
    var fnDecls = _reg2816;
    var _reg2817 = _allocDict();
    var varDecls = _reg2817;
    var _reg2818 = _allocDict();
    var builtSyms = _reg2818;
    var innereval = eval;
    var geval = function(strPtr) {
      var str = _loadString(strPtr);
      if (_loadBool(debugShivc)) {
        console.log('evalling:\n' + str);
      }
      //update memory handles
      innereval('(x) => { global._allocPtr = x; }')(_allocPtr);
      ret = innereval(str);
      _allocPtr = innereval('_allocPtr');
      return ret;
    };
    var _reg2821 = _allocList();
    var _reg2822 = _allocList();
    var _reg2823 = _allocString("include");
    add(_reg2822, _reg2823);
    var _reg2824 = _allocString("js_builtins");
    add(_reg2822, _reg2824);
    add(_reg2821, _reg2822);
    var _reg2820 = handleIncludes(_reg2821);
    var _reg2819 = jsCodeGen(_reg2820);
    var primordial = _reg2819;
    var _reg2825 = debugShivc;
    var _reg2826 = _loadBool(_reg2825);
    if (_reg2826) {
      var _reg2828 = _allocString("Primordial:\n");
      var _reg2827 = toStr(_reg2828);
      var _reg2830 = primordial;
      var _reg2829 = toStr(_reg2830);
      var _reg2832 = _reg2827;
      var _reg2833 = _reg2829;
      var _reg2831 = $dollar$plus(_reg2832, _reg2833);
      free(_reg2829);
      free(_reg2827);
      console.log(_loadString(_reg2831));
      free(_reg2831);
    }
      innereval('(x) => { global.require = x; }')(require);
      innereval(_loadString(primordial));
      innereval('(x) => { global._memory = x; }')(_memory);
      innereval('(x) => { global._u8M = x; }')(_u8M);
      innereval('(x) => { global._i32M = x; }')(_i32M);
    
    var _reg2835 = module;
    var _reg2834 = findFunctionDeclarations(_reg2835);
    var _reg2836 = _allocInt(0);
    var i = _reg2836;
    while(true) {
      var _reg2838 = i;
      var _reg2840 = module;
      var _reg2839 = len(_reg2840);
      var _reg2837 = $lt(_reg2838, _reg2839);
      var _reg3041 = _reg2837;
      var _reg3040 = not(_reg3041);
      var _reg3042 = _loadBool(_reg3040);
      free(_reg3040);
      if (_reg3042) {
        break;
      }
      var _reg2842 = module;
      var _reg2843 = i;
      var _reg2841 = _index(_reg2842, _reg2843);
      var sexp = _reg2841;
      var _reg2845 = i;
      var _reg2846 = _allocInt(1);
      var _reg2844 = $plus(_reg2845, _reg2846);
      decref(i);
      incref(_reg2844);
      i = _reg2844;
      var _reg2848 = sexp;
      var _reg2849 = _allocString("pass");
      var _reg2847 = isListStartingWith(_reg2848, _reg2849);
      var _reg2850 = _loadBool(_reg2847);
      free(_reg2847);
      if (_reg2850) {
        var _reg2851 = module;
        var _reg2853 = i;
        var _reg2854 = _allocInt(1);
        var _reg2852 = $minus(_reg2853, _reg2854);
        del(_reg2851, _reg2852);
        var _reg2855 = _allocInt(0);
        decref(i);
        incref(_reg2855);
        i = _reg2855;
        var _reg2857 = sexp;
        var _reg2858 = _allocInt(1);
        var _reg2856 = _index(_reg2857, _reg2858);
        var passName = _reg2856;
        var _reg2860 = sexp;
        var _reg2861 = _allocInt(2);
        var _reg2859 = _index(_reg2860, _reg2861);
        var args = _reg2859;
        var _reg2863 = sexp;
        var _reg2864 = _allocInt(3);
        var _reg2862 = _index(_reg2863, _reg2864);
        var body = _reg2862;
        var _reg2865 = debugShivc;
        var _reg2866 = _loadBool(_reg2865);
        if (_reg2866) {
          var _reg2868 = _allocString("Running pass ");
          var _reg2867 = toStr(_reg2868);
          var _reg2870 = passName;
          var _reg2869 = toStr(_reg2870);
          var _reg2872 = _reg2867;
          var _reg2873 = _reg2869;
          var _reg2871 = $dollar$plus(_reg2872, _reg2873);
          free(_reg2869);
          free(_reg2867);
          var _reg2875 = _allocString(" :\n");
          var _reg2874 = toStr(_reg2875);
          var _reg2877 = _reg2871;
          var _reg2878 = _reg2874;
          var _reg2876 = $dollar$plus(_reg2877, _reg2878);
          free(_reg2874);
          free(_reg2871);
          var _reg2881 = sexp;
          var _reg2880 = prettySexpr(_reg2881);
          var _reg2879 = toStr(_reg2880);
          var _reg2883 = _reg2876;
          var _reg2884 = _reg2879;
          var _reg2882 = $dollar$plus(_reg2883, _reg2884);
          free(_reg2879);
          free(_reg2876);
          console.log(_loadString(_reg2882));
          free(_reg2882);
        }
        var _reg2886 = sexp;
        var _reg2885 = buildSymbolsUsedBy(_reg2886);
        var _reg2890 = _allocString("var ");
        var _reg2891 = passName;
        var _reg2889 = $plus(_reg2890, _reg2891);
        var _reg2892 = _allocString(" = _allocDict();");
        var _reg2888 = $plus(_reg2889, _reg2892);
        var _reg2887 = geval(_reg2888);
        var _reg2893 = _allocInt(0);
        var j = _reg2893;
        while(true) {
          var _reg2895 = j;
          var _reg2897 = body;
          var _reg2896 = len(_reg2897);
          var _reg2894 = $lt(_reg2895, _reg2896);
          var _reg2979 = _reg2894;
          var _reg2978 = not(_reg2979);
          var _reg2980 = _loadBool(_reg2978);
          free(_reg2978);
          if (_reg2980) {
            break;
          }
          var _reg2899 = body;
          var _reg2900 = j;
          var _reg2898 = _index(_reg2899, _reg2900);
          var elem = _reg2898;
          var _reg2902 = elem;
          var _reg2903 = _allocString("field");
          var _reg2901 = isListStartingWith(_reg2902, _reg2903);
          var _reg2904 = _loadBool(_reg2901);
          free(_reg2901);
          if (_reg2904) {
            var _reg2907 = _allocString("\"");
            var _reg2909 = elem;
            var _reg2910 = _allocInt(1);
            var _reg2908 = _index(_reg2909, _reg2910);
            var _reg2906 = $plus(_reg2907, _reg2908);
            var _reg2911 = _allocString("\"");
            var _reg2905 = $plus(_reg2906, _reg2911);
            var varName = _reg2905;
            var _reg2913 = elem;
            var _reg2914 = _allocInt(3);
            var _reg2912 = _index(_reg2913, _reg2914);
            var expr = _reg2912;
            var _reg2915 = debugShivc;
            var _reg2916 = _loadBool(_reg2915);
            if (_reg2916) {
              var _reg2918 = _allocString("setting field ");
              var _reg2917 = toStr(_reg2918);
              var _reg2920 = varName;
              var _reg2919 = toStr(_reg2920);
              var _reg2922 = _reg2917;
              var _reg2923 = _reg2919;
              var _reg2921 = $dollar$plus(_reg2922, _reg2923);
              free(_reg2919);
              free(_reg2917);
              var _reg2925 = _allocString(" with expr:\n");
              var _reg2924 = toStr(_reg2925);
              var _reg2927 = _reg2921;
              var _reg2928 = _reg2924;
              var _reg2926 = $dollar$plus(_reg2927, _reg2928);
              free(_reg2924);
              free(_reg2921);
              var _reg2931 = expr;
              var _reg2930 = prettySexpr(_reg2931);
              var _reg2929 = toStr(_reg2930);
              var _reg2933 = _reg2926;
              var _reg2934 = _reg2929;
              var _reg2932 = $dollar$plus(_reg2933, _reg2934);
              free(_reg2929);
              free(_reg2926);
              console.log(_loadString(_reg2932));
              free(_reg2932);
            }
            var _reg2936 = _allocList();
            var _reg2937 = _allocList();
            var _reg2938 = _allocString("set");
            add(_reg2937, _reg2938);
            var _reg2939 = passName;
            add(_reg2937, _reg2939);
            var _reg2940 = varName;
            add(_reg2937, _reg2940);
            var _reg2941 = expr;
            add(_reg2937, _reg2941);
            add(_reg2936, _reg2937);
            var _reg2935 = ssaFormConversion(_reg2936);
            var setter = _reg2935;
            var _reg2942 = debugShivc;
            var _reg2943 = _loadBool(_reg2942);
            if (_reg2943) {
              var _reg2945 = _allocString("field setter ssa'd:\n");
              var _reg2944 = toStr(_reg2945);
              var _reg2948 = setter;
              var _reg2947 = prettySexpr(_reg2948);
              var _reg2946 = toStr(_reg2947);
              var _reg2950 = _reg2944;
              var _reg2951 = _reg2946;
              var _reg2949 = $dollar$plus(_reg2950, _reg2951);
              free(_reg2946);
              free(_reg2944);
              console.log(_loadString(_reg2949));
              free(_reg2949);
            }
            var _reg2952 = _allocInt(0);
            var i20 = _reg2952;
            while(true) {
              var _reg2954 = i20;
              var _reg2956 = setter;
              var _reg2955 = len(_reg2956);
              var _reg2953 = $lt(_reg2954, _reg2955);
              var _reg2968 = _reg2953;
              var _reg2967 = not(_reg2968);
              var _reg2969 = _loadBool(_reg2967);
              free(_reg2967);
              if (_reg2969) {
                break;
              }
              var _reg2958 = setter;
              var _reg2959 = i20;
              var _reg2957 = _index(_reg2958, _reg2959);
              var stmt = _reg2957;
              var _reg2962 = stmt;
              var _reg2963 = _allocInt(0);
              var _reg2961 = jsStmtStr(_reg2962, _reg2963);
              var _reg2960 = geval(_reg2961);
              var _reg2965 = i20;
              var _reg2966 = _allocInt(1);
              var _reg2964 = $plus(_reg2965, _reg2966);
              decref(i20);
              incref(_reg2964);
              i20 = _reg2964;
            }
            var _reg2970 = body;
            var _reg2971 = j;
            del(_reg2970, _reg2971);
            var _reg2973 = j;
            var _reg2974 = _allocInt(1);
            var _reg2972 = $minus(_reg2973, _reg2974);
            decref(j);
            incref(_reg2972);
            j = _reg2972;
          }
          var _reg2976 = j;
          var _reg2977 = _allocInt(1);
          var _reg2975 = $plus(_reg2976, _reg2977);
          decref(j);
          incref(_reg2975);
          j = _reg2975;
        }
        var _reg2982 = body;
        var _reg2981 = ssaFormConversion(_reg2982);
        var ssaBody = _reg2981;
        var _reg2984 = _allocList();
        var _reg2985 = _allocString("fn");
        add(_reg2984, _reg2985);
        var _reg2986 = args;
        add(_reg2984, _reg2986);
        var _reg2987 = ssaBody;
        add(_reg2984, _reg2987);
        var _reg2988 = _allocInt(0);
        var _reg2983 = jsStmtStr(_reg2984, _reg2988);
        var passSexpr = _reg2983;
        var _reg2989 = debugShivc;
        var _reg2990 = _loadBool(_reg2989);
        if (_reg2990) {
          var _reg2992 = _allocString("Built pass ");
          var _reg2991 = toStr(_reg2992);
          var _reg2994 = passName;
          var _reg2993 = toStr(_reg2994);
          var _reg2996 = _reg2991;
          var _reg2997 = _reg2993;
          var _reg2995 = $dollar$plus(_reg2996, _reg2997);
          free(_reg2993);
          free(_reg2991);
          var _reg2999 = _allocString(" :\n");
          var _reg2998 = toStr(_reg2999);
          var _reg3001 = _reg2995;
          var _reg3002 = _reg2998;
          var _reg3000 = $dollar$plus(_reg3001, _reg3002);
          free(_reg2998);
          free(_reg2995);
          var _reg3004 = passSexpr;
          var _reg3003 = toStr(_reg3004);
          var _reg3006 = _reg3000;
          var _reg3007 = _reg3003;
          var _reg3005 = $dollar$plus(_reg3006, _reg3007);
          free(_reg3003);
          free(_reg3000);
          console.log(_loadString(_reg3005));
          free(_reg3005);
        }
        var _reg3011 = _allocString("(function() {return ");
        var _reg3012 = passSexpr;
        var _reg3010 = $plus(_reg3011, _reg3012);
        var _reg3013 = _allocString("})()");
        var _reg3009 = $plus(_reg3010, _reg3013);
        var _reg3008 = geval(_reg3009);
        var passFn = _reg3008;
        var _reg3014 = debugShivc;
        var _reg3015 = _loadBool(_reg3014);
        if (_reg3015) {
console.log('passfn:', passFn);
        }
innereval('(x) => { global._allocPtr = x; }')(_allocPtr);
        var _reg3017 = module;
        var _reg3016 = passFn(_reg3017);
        decref(module);
        incref(_reg3016);
        module = _reg3016;
_allocPtr = innereval('_allocPtr');
        var _reg3018 = debugShivc;
        var _reg3019 = _loadBool(_reg3018);
        if (_reg3019) {
          var _reg3021 = _allocString("after running pass ");
          var _reg3020 = toStr(_reg3021);
          var _reg3023 = passName;
          var _reg3022 = toStr(_reg3023);
          var _reg3025 = _reg3020;
          var _reg3026 = _reg3022;
          var _reg3024 = $dollar$plus(_reg3025, _reg3026);
          free(_reg3022);
          free(_reg3020);
          var _reg3028 = _allocString(" :\n");
          var _reg3027 = toStr(_reg3028);
          var _reg3030 = _reg3024;
          var _reg3031 = _reg3027;
          var _reg3029 = $dollar$plus(_reg3030, _reg3031);
          free(_reg3027);
          free(_reg3024);
          var _reg3034 = module;
          var _reg3033 = prettySexpr(_reg3034);
          var _reg3032 = toStr(_reg3033);
          var _reg3036 = _reg3029;
          var _reg3037 = _reg3032;
          var _reg3035 = $dollar$plus(_reg3036, _reg3037);
          free(_reg3032);
          free(_reg3029);
          console.log(_loadString(_reg3035));
          free(_reg3035);
        }
        var _reg3038 = _allocDict();
        decref(fnDecls);
        incref(_reg3038);
        fnDecls = _reg3038;
        var _reg3039 = findModuleDeclarations();
      }
    }
    function buildSymbolsUsedBy(sexp) {
      var _reg3045 = sexp;
      var _reg3044 = findUsedSymbols(_reg3045);
      var symbols = _reg3044;
      var _reg3046 = debugShivc;
      var _reg3047 = _loadBool(_reg3046);
      if (_reg3047) {
        var _reg3049 = _allocString("Found symbols:");
        var _reg3048 = toStr(_reg3049);
        console.log(_loadString(_reg3048));
        free(_reg3048);
        var _reg3051 = _allocString("  funcs=");
        var _reg3050 = toStr(_reg3051);
        var _reg3055 = symbols;
        var _reg3056 = _allocString("funcs");
        var _reg3054 = _index(_reg3055, _reg3056);
        var _reg3052 = toStr(_reg3054);
        var _reg3058 = _reg3050;
        var _reg3059 = _reg3052;
        var _reg3057 = $dollar$plus(_reg3058, _reg3059);
        free(_reg3052);
        free(_reg3050);
        console.log(_loadString(_reg3057));
        free(_reg3057);
        var _reg3061 = _allocString("  vars=");
        var _reg3060 = toStr(_reg3061);
        var _reg3065 = symbols;
        var _reg3066 = _allocString("vars");
        var _reg3064 = _index(_reg3065, _reg3066);
        var _reg3062 = toStr(_reg3064);
        var _reg3068 = _reg3060;
        var _reg3069 = _reg3062;
        var _reg3067 = $dollar$plus(_reg3068, _reg3069);
        free(_reg3062);
        free(_reg3060);
        console.log(_loadString(_reg3067));
        free(_reg3067);
      }
      var _reg3070 = _allocInt(0);
      var i21 = _reg3070;
      while(true) {
        var _reg3072 = i21;
        var _reg3076 = symbols;
        var _reg3077 = _allocString("funcs");
        var _reg3075 = _index(_reg3076, _reg3077);
        var _reg3073 = len(_reg3075);
        var _reg3071 = $lt(_reg3072, _reg3073);
        var _reg3103 = _reg3071;
        var _reg3102 = not(_reg3103);
        var _reg3104 = _loadBool(_reg3102);
        free(_reg3102);
        if (_reg3104) {
          break;
        }
        var _reg3081 = symbols;
        var _reg3082 = _allocString("funcs");
        var _reg3080 = _index(_reg3081, _reg3082);
        var _reg3083 = i21;
        var _reg3078 = _index(_reg3080, _reg3083);
        var call = _reg3078;
        var _reg3087 = builtSyms;
        var _reg3088 = call;
        var _reg3086 = hasKey(_reg3087, _reg3088);
        var _reg3085 = not(_reg3086);
        var _reg3090 = fnDecls;
        var _reg3091 = call;
        var _reg3089 = hasKey(_reg3090, _reg3091);
        var _reg3084 = and(_reg3085, _reg3089);
        var _reg3092 = _loadBool(_reg3084);
        free(_reg3084);
        if (_reg3092) {
          var _reg3094 = _allocString("func");
          var _reg3095 = call;
          var _reg3097 = fnDecls;
          var _reg3098 = call;
          var _reg3096 = _index(_reg3097, _reg3098);
          var _reg3093 = build(_reg3094, _reg3095, _reg3096);
        }
        var _reg3100 = i21;
        var _reg3101 = _allocInt(1);
        var _reg3099 = $plus(_reg3100, _reg3101);
        decref(i21);
        incref(_reg3099);
        i21 = _reg3099;
      }
      var _reg3105 = _allocInt(0);
      var i22 = _reg3105;
      while(true) {
        var _reg3107 = i22;
        var _reg3111 = symbols;
        var _reg3112 = _allocString("vars");
        var _reg3110 = _index(_reg3111, _reg3112);
        var _reg3108 = len(_reg3110);
        var _reg3106 = $lt(_reg3107, _reg3108);
        var _reg3138 = _reg3106;
        var _reg3137 = not(_reg3138);
        var _reg3139 = _loadBool(_reg3137);
        free(_reg3137);
        if (_reg3139) {
          break;
        }
        var _reg3116 = symbols;
        var _reg3117 = _allocString("vars");
        var _reg3115 = _index(_reg3116, _reg3117);
        var _reg3118 = i22;
        var _reg3113 = _index(_reg3115, _reg3118);
        var decl = _reg3113;
        var _reg3122 = builtSyms;
        var _reg3123 = decl;
        var _reg3121 = hasKey(_reg3122, _reg3123);
        var _reg3120 = not(_reg3121);
        var _reg3125 = varDecls;
        var _reg3126 = decl;
        var _reg3124 = hasKey(_reg3125, _reg3126);
        var _reg3119 = and(_reg3120, _reg3124);
        var _reg3127 = _loadBool(_reg3119);
        free(_reg3119);
        if (_reg3127) {
          var _reg3129 = _allocString("var");
          var _reg3130 = decl;
          var _reg3132 = varDecls;
          var _reg3133 = decl;
          var _reg3131 = _index(_reg3132, _reg3133);
          var _reg3128 = build(_reg3129, _reg3130, _reg3131);
        }
        var _reg3135 = i22;
        var _reg3136 = _allocInt(1);
        var _reg3134 = $plus(_reg3135, _reg3136);
        decref(i22);
        incref(_reg3134);
        i22 = _reg3134;
      }
      function build(kind, name, sexp) {
        var _reg3142 = builtSyms;
        var _reg3143 = name;
        var _reg3144 = _allocBool(true);
        var _reg3141 = set(_reg3142, _reg3143, _reg3144);
        var _reg3145 = debugShivc;
        var _reg3146 = _loadBool(_reg3145);
        if (_reg3146) {
          var _reg3148 = _allocString("Building ");
          var _reg3147 = toStr(_reg3148);
          var _reg3150 = kind;
          var _reg3149 = toStr(_reg3150);
          var _reg3152 = _reg3147;
          var _reg3153 = _reg3149;
          var _reg3151 = $dollar$plus(_reg3152, _reg3153);
          free(_reg3149);
          free(_reg3147);
          var _reg3155 = _allocString(" ");
          var _reg3154 = toStr(_reg3155);
          var _reg3157 = _reg3151;
          var _reg3158 = _reg3154;
          var _reg3156 = $dollar$plus(_reg3157, _reg3158);
          free(_reg3154);
          free(_reg3151);
          var _reg3160 = name;
          var _reg3159 = toStr(_reg3160);
          var _reg3162 = _reg3156;
          var _reg3163 = _reg3159;
          var _reg3161 = $dollar$plus(_reg3162, _reg3163);
          free(_reg3159);
          free(_reg3156);
          var _reg3165 = _allocString(" :\n");
          var _reg3164 = toStr(_reg3165);
          var _reg3167 = _reg3161;
          var _reg3168 = _reg3164;
          var _reg3166 = $dollar$plus(_reg3167, _reg3168);
          free(_reg3164);
          free(_reg3161);
          var _reg3171 = sexp;
          var _reg3170 = prettySexpr(_reg3171);
          var _reg3169 = toStr(_reg3170);
          var _reg3173 = _reg3166;
          var _reg3174 = _reg3169;
          var _reg3172 = $dollar$plus(_reg3173, _reg3174);
          free(_reg3169);
          free(_reg3166);
          console.log(_loadString(_reg3172));
          free(_reg3172);
        }
        var _reg3176 = sexp;
        var _reg3175 = buildSymbolsUsedBy(_reg3176);
        var _reg3179 = _allocList();
        var _reg3180 = sexp;
        add(_reg3179, _reg3180);
        var _reg3178 = ssaFormConversion(_reg3179);
        var _reg3181 = _allocInt(0);
        var _reg3177 = _index(_reg3178, _reg3181);
        var ssaSexp = _reg3177;
        var _reg3182 = debugShivc;
        var _reg3183 = _loadBool(_reg3182);
        if (_reg3183) {
          var _reg3185 = _allocString("  ssa-d: ");
          var _reg3184 = toStr(_reg3185);
          var _reg3188 = ssaSexp;
          var _reg3187 = prettySexpr(_reg3188);
          var _reg3186 = toStr(_reg3187);
          var _reg3190 = _reg3184;
          var _reg3191 = _reg3186;
          var _reg3189 = $dollar$plus(_reg3190, _reg3191);
          free(_reg3186);
          free(_reg3184);
          console.log(_loadString(_reg3189));
          free(_reg3189);
        }
        var _reg3193 = ssaSexp;
        var _reg3194 = _allocInt(0);
        var _reg3192 = jsStmtStr(_reg3193, _reg3194);
        var code = _reg3192;
        var _reg3195 = debugShivc;
        var _reg3196 = _loadBool(_reg3195);
        if (_reg3196) {
          var _reg3198 = _allocString("  ");
          var _reg3197 = toStr(_reg3198);
          var _reg3200 = code;
          var _reg3199 = toStr(_reg3200);
          var _reg3202 = _reg3197;
          var _reg3203 = _reg3199;
          var _reg3201 = $dollar$plus(_reg3202, _reg3203);
          free(_reg3199);
          free(_reg3197);
          console.log(_loadString(_reg3201));
          free(_reg3201);
        }
        var _reg3205 = code;
        var _reg3204 = geval(_reg3205);
      };
    };
    function findUsedSymbols(sexp) {
      var _reg3208 = _allocDict();
      var _reg3209 = _allocString("funcs");
      var _reg3210 = _allocList();
      set(_reg3208, _reg3209, _reg3210);
      var _reg3211 = _allocString("vars");
      var _reg3212 = _allocList();
      set(_reg3208, _reg3211, _reg3212);
      var result = _reg3208;
      var _reg3216 = sexp;
      var _reg3215 = isList(_reg3216);
      var _reg3214 = not(_reg3215);
      var _reg3219 = sexp;
      var _reg3218 = len(_reg3219);
      var _reg3220 = _allocInt(0);
      var _reg3217 = $eq$eq(_reg3218, _reg3220);
      var _reg3213 = or(_reg3214, _reg3217);
      var _reg3221 = _loadBool(_reg3213);
      free(_reg3213);
      if (_reg3221) {
        var _reg3222 = result;
        return _reg3222;
      }
      var _reg3226 = sexp;
      var _reg3227 = _allocInt(0);
      var _reg3225 = _index(_reg3226, _reg3227);
      var _reg3224 = isList(_reg3225);
      var _reg3223 = not(_reg3224);
      var _reg3228 = _loadBool(_reg3223);
      free(_reg3223);
      if (_reg3228) {
        var _reg3231 = result;
        var _reg3232 = _allocString("funcs");
        var _reg3230 = _index(_reg3231, _reg3232);
        var _reg3234 = sexp;
        var _reg3235 = _allocInt(0);
        var _reg3233 = _index(_reg3234, _reg3235);
        add(_reg3230, _reg3233);
      }
      var _reg3236 = _allocInt(0);
      var i23 = _reg3236;
      while(true) {
        var _reg3238 = i23;
        var _reg3240 = sexp;
        var _reg3239 = len(_reg3240);
        var _reg3237 = $lt(_reg3238, _reg3239);
        var _reg3283 = _reg3237;
        var _reg3282 = not(_reg3283);
        var _reg3284 = _loadBool(_reg3282);
        free(_reg3282);
        if (_reg3284) {
          break;
        }
        var _reg3242 = sexp;
        var _reg3243 = i23;
        var _reg3241 = _index(_reg3242, _reg3243);
        var child = _reg3241;
        var _reg3246 = child;
        var _reg3245 = isList(_reg3246);
        var _reg3244 = not(_reg3245);
        var _reg3247 = _loadBool(_reg3244);
        free(_reg3244);
        if (_reg3247) {
          var _reg3250 = result;
          var _reg3251 = _allocString("vars");
          var _reg3249 = _index(_reg3250, _reg3251);
          var _reg3252 = child;
          add(_reg3249, _reg3252);
        }
        var _reg3254 = child;
        var _reg3253 = findUsedSymbols(_reg3254);
        var syms = _reg3253;
        var _reg3256 = result;
        var _reg3257 = _allocString("funcs");
        var _reg3261 = result;
        var _reg3262 = _allocString("funcs");
        var _reg3260 = _index(_reg3261, _reg3262);
        var _reg3265 = syms;
        var _reg3266 = _allocString("funcs");
        var _reg3264 = _index(_reg3265, _reg3266);
        var _reg3258 = $plus(_reg3260, _reg3264);
        var _reg3255 = set(_reg3256, _reg3257, _reg3258);
        var _reg3268 = result;
        var _reg3269 = _allocString("vars");
        var _reg3273 = result;
        var _reg3274 = _allocString("vars");
        var _reg3272 = _index(_reg3273, _reg3274);
        var _reg3277 = syms;
        var _reg3278 = _allocString("vars");
        var _reg3276 = _index(_reg3277, _reg3278);
        var _reg3270 = $plus(_reg3272, _reg3276);
        var _reg3267 = set(_reg3268, _reg3269, _reg3270);
        var _reg3280 = i23;
        var _reg3281 = _allocInt(1);
        var _reg3279 = $plus(_reg3280, _reg3281);
        decref(i23);
        incref(_reg3279);
        i23 = _reg3279;
      }
      var _reg3285 = result;
      return _reg3285;
    };
    function findModuleDeclarations() {
      var _reg3288 = module;
      var _reg3287 = findFunctionDeclarations(_reg3288);
      var _reg3289 = _allocInt(0);
      var i24 = _reg3289;
      while(true) {
        var _reg3291 = i24;
        var _reg3293 = module;
        var _reg3292 = len(_reg3293);
        var _reg3290 = $lt(_reg3291, _reg3292);
        var _reg3321 = _reg3290;
        var _reg3320 = not(_reg3321);
        var _reg3322 = _loadBool(_reg3320);
        free(_reg3320);
        if (_reg3322) {
          break;
        }
        var _reg3295 = module;
        var _reg3296 = i24;
        var _reg3294 = _index(_reg3295, _reg3296);
        var sexp = _reg3294;
        var _reg3298 = sexp;
        var _reg3299 = _allocString("var");
        var _reg3297 = isListStartingWith(_reg3298, _reg3299);
        var _reg3300 = _loadBool(_reg3297);
        free(_reg3297);
        if (_reg3300) {
          var _reg3302 = sexp;
          var _reg3303 = _allocInt(1);
          var _reg3301 = _index(_reg3302, _reg3303);
          var name = _reg3301;
          var _reg3307 = varDecls;
          var _reg3308 = name;
          var _reg3306 = hasKey(_reg3307, _reg3308);
          var _reg3310 = builtSyms;
          var _reg3311 = name;
          var _reg3309 = hasKey(_reg3310, _reg3311);
          var _reg3305 = or(_reg3306, _reg3309);
          var _reg3304 = not(_reg3305);
          var _reg3312 = _loadBool(_reg3304);
          free(_reg3304);
          if (_reg3312) {
            var _reg3314 = varDecls;
            var _reg3315 = name;
            var _reg3316 = sexp;
            var _reg3313 = set(_reg3314, _reg3315, _reg3316);
          }
        }
        var _reg3318 = i24;
        var _reg3319 = _allocInt(1);
        var _reg3317 = $plus(_reg3318, _reg3319);
        decref(i24);
        incref(_reg3317);
        i24 = _reg3317;
      }
    };
    function findFunctionDeclarations(sexp) {
      var _reg3327 = sexp;
      var _reg3326 = isList(_reg3327);
      var _reg3325 = not(_reg3326);
      var _reg3330 = sexp;
      var _reg3329 = len(_reg3330);
      var _reg3331 = _allocInt(0);
      var _reg3328 = $eq$eq(_reg3329, _reg3331);
      var _reg3324 = or(_reg3325, _reg3328);
      var _reg3332 = _loadBool(_reg3324);
      free(_reg3324);
      if (_reg3332) {
        return;
      }
      var _reg3336 = sexp;
      var _reg3337 = _allocInt(0);
      var _reg3335 = _index(_reg3336, _reg3337);
      var _reg3338 = _allocString("fn");
      var _reg3334 = $eq$eq(_reg3335, _reg3338);
      var _reg3342 = sexp;
      var _reg3343 = _allocInt(1);
      var _reg3341 = _index(_reg3342, _reg3343);
      var _reg3340 = isList(_reg3341);
      var _reg3339 = not(_reg3340);
      var _reg3333 = and(_reg3334, _reg3339);
      var _reg3344 = _loadBool(_reg3333);
      free(_reg3333);
      if (_reg3344) {
        var _reg3346 = sexp;
        var _reg3347 = _allocInt(1);
        var _reg3345 = _index(_reg3346, _reg3347);
        var name = _reg3345;
        var _reg3351 = fnDecls;
        var _reg3352 = name;
        var _reg3350 = hasKey(_reg3351, _reg3352);
        var _reg3354 = builtSyms;
        var _reg3355 = name;
        var _reg3353 = hasKey(_reg3354, _reg3355);
        var _reg3349 = or(_reg3350, _reg3353);
        var _reg3348 = not(_reg3349);
        var _reg3356 = _loadBool(_reg3348);
        free(_reg3348);
        if (_reg3356) {
          var _reg3358 = fnDecls;
          var _reg3359 = name;
          var _reg3360 = sexp;
          var _reg3357 = set(_reg3358, _reg3359, _reg3360);
        }
      }
      var _reg3361 = _allocInt(0);
      var i25 = _reg3361;
      while(true) {
        var _reg3363 = i25;
        var _reg3365 = sexp;
        var _reg3364 = len(_reg3365);
        var _reg3362 = $lt(_reg3363, _reg3364);
        var _reg3375 = _reg3362;
        var _reg3374 = not(_reg3375);
        var _reg3376 = _loadBool(_reg3374);
        free(_reg3374);
        if (_reg3376) {
          break;
        }
        var _reg3367 = sexp;
        var _reg3368 = i25;
        var _reg3366 = _index(_reg3367, _reg3368);
        var child = _reg3366;
        var _reg3370 = child;
        var _reg3369 = findFunctionDeclarations(_reg3370);
        var _reg3372 = i25;
        var _reg3373 = _allocInt(1);
        var _reg3371 = $plus(_reg3372, _reg3373);
        decref(i25);
        incref(_reg3371);
        i25 = _reg3371;
      }
    };
  };
};
function main(args) {
  var _reg3379 = args;
  var _reg3380 = _allocInt(0);
  var _reg3378 = _index(_reg3379, _reg3380);
  var filename = _reg3378;
  var _reg3381 = _allocList();
  var includeDirs = _reg3381;
  var _reg3382 = _allocInt(1);
  var argIdx = _reg3382;
  while(true) {
    var _reg3384 = argIdx;
    var _reg3386 = args;
    var _reg3385 = len(_reg3386);
    var _reg3383 = $lt(_reg3384, _reg3385);
    var _reg3429 = _reg3383;
    var _reg3428 = not(_reg3429);
    var _reg3430 = _loadBool(_reg3428);
    free(_reg3428);
    if (_reg3430) {
      break;
    }
    var _reg3389 = args;
    var _reg3390 = argIdx;
    var _reg3388 = _index(_reg3389, _reg3390);
    var _reg3391 = _allocString("--debug");
    var _reg3387 = $eq$eq(_reg3388, _reg3391);
    var _reg3392 = _loadBool(_reg3387);
    free(_reg3387);
    if (_reg3392) {
      var _reg3393 = _allocBool(true);
      decref(debugShivc);
      incref(_reg3393);
      debugShivc = _reg3393;
      var _reg3395 = argIdx;
      var _reg3396 = _allocInt(1);
      var _reg3394 = $plus(_reg3395, _reg3396);
      decref(argIdx);
      incref(_reg3394);
      argIdx = _reg3394;
    } else {
      var _reg3399 = args;
      var _reg3400 = argIdx;
      var _reg3398 = _index(_reg3399, _reg3400);
      var _reg3401 = _allocString("--include");
      var _reg3397 = $eq$eq(_reg3398, _reg3401);
      var _reg3402 = _loadBool(_reg3397);
      free(_reg3397);
      if (_reg3402) {
        var _reg3406 = argIdx;
        var _reg3407 = _allocInt(1);
        var _reg3405 = $plus(_reg3406, _reg3407);
        var _reg3409 = args;
        var _reg3408 = len(_reg3409);
        var _reg3404 = $lt(_reg3405, _reg3408);
        var _reg3410 = _allocString("Assertion failure: [argIdx, +, 1, <, len, args]");
        var _reg3403 = assert(_reg3404, _reg3410);
        var _reg3411 = includeDirs;
        var _reg3413 = args;
        var _reg3415 = argIdx;
        var _reg3416 = _allocInt(1);
        var _reg3414 = $plus(_reg3415, _reg3416);
        var _reg3412 = _index(_reg3413, _reg3414);
        add(_reg3411, _reg3412);
        var _reg3418 = argIdx;
        var _reg3419 = _allocInt(2);
        var _reg3417 = $plus(_reg3418, _reg3419);
        decref(argIdx);
        incref(_reg3417);
        argIdx = _reg3417;
      } else {
        var _reg3421 = _allocBool(false);
        var _reg3423 = _allocString("Unknown arguments: ");
        var _reg3426 = args;
        var _reg3427 = argIdx;
        var _reg3425 = sliceFrom(_reg3426, _reg3427);
        var _reg3424 = toStr(_reg3425);
        var _reg3422 = $plus(_reg3423, _reg3424);
        var _reg3420 = assert(_reg3421, _reg3422);
      }
    }
  }
  var _reg3432 = filename;
  var _reg3431 = readFile(_reg3432);
  var contents = _reg3431;
  var _reg3434 = contents;
  var _reg3433 = parseSexprs(_reg3434);
  var module = _reg3433;
  var _reg3436 = module;
  var _reg3437 = filename;
  var _reg3438 = includeDirs;
  var _reg3435 = preprocess(_reg3436, _reg3437, _reg3438);
  var processed = _reg3435;
  var _reg3440 = processed;
  var _reg3439 = jsCodeGen(_reg3440);
  var jsStr = _reg3439;
  var _reg3441 = debugShivc;
  var _reg3442 = _loadBool(_reg3441);
  if (_reg3442) {
    var _reg3444 = jsStr;
    var _reg3443 = toStr(_reg3444);
    console.log(_loadString(_reg3443));
    free(_reg3443);
  }
eval(_loadString(jsStr));
  var _reg3446 = _allocInt(0);
  var _reg3445 = exit(_reg3446);
};
var argv = _allocList();
for (var i = 0; i < process.argv.length; ++i) {
  add(argv, _allocString(process.argv[i]));
}

var _reg3449 = argv;
var _reg3450 = _allocInt(2);
var _reg3448 = sliceFrom(_reg3449, _reg3450);
var _reg3447 = main(_reg3448);

