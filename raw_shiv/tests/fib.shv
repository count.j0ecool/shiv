; Copyright 2019 Google LLC
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     https://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; Base, raw fibbonaci implementation

include raw_js_builtins

fn fib_naive n {
  register r0
  <- r0 (< n 2)
  if r0 {
    return n
  } else {
    register r1
    <- r1 (- n 1)
    register r2
    <- r2 (fib_naive r1)
    register r3
    <- r3 (- n 2)
    register r4
    <- r4 (fib_naive r3)
    register r5
    <- r5 (+ r2 r4)
    return r5
  }
}

fn fib_iterative n {
  register x
  <- x 0
  register y
  <- y 1
  loop {
    register cond
    <- cond (<= n 0)
    if cond {
      break
    }
    register z
    <- z (+ x y)
    <- x y
    <- y z
    <- n (- n 1)
  }
  return x
}

register i
<- i 0
loop {
  register done
  <- done (> i 10)
  if done {
    break
  }
  register naiveVal
  <- naiveVal (fib_naive i)
  register naiveBox
  <- naiveBox (allocInt naiveVal)
  register naiveStr
  <- naiveStr (toStr naiveBox)
  register iterativeVal
  <- iterativeVal (fib_iterative i)
  register iterativeBox
  <- iterativeBox (allocInt iterativeVal)
  register iterativeStr
  <- iterativeStr (toStr iterativeBox)
  register space
  <- space (allocString " ")
  register str
  <- str ($+ naiveStr space)
  register old
  <- old str
  <- str ($+ str iterativeStr)
  decref old
  print str
  <- i (+ i 1)
  decref naiveBox
  decref naiveStr
  decref iterativeBox
  decref iterativeStr
  decref space
  decref str
}
