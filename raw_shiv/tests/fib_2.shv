; Copyright 2019 Google LLC
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     https://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; After flattenExprs, allows for nested expressions

include basics

fn fib_naive n {
  register cond (< n 2)
  if cond {
    return n
  } else {
    register ret (+
      (fib_naive (- n 1))
      (fib_naive (- n 2))
    )
    return ret
  }
}

fn fib_iterative n {
  register x 0
  register y 1
  loop {
    register cond (<= n 0)
    if cond {
      break
    }
    register z (+ x y)
    <- x y
    <- y z
    <- n (- n 1)
  }
  return x
}

register i 0
loop {
  register done (> i 10)
  if done {
    break
  }
  register naiveStr (toStr (allocInt (fib_naive i)))
  register iterativeStr (toStr (allocInt (fib_iterative i)))
  register str ($+ ($+ naiveStr (allocString " ")) iterativeStr)
  print str
  <- i (+ i 1)
  decref naiveStr
  decref iterativeStr
  decref str
}
