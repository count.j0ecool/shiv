import
  sequtils,
  strformat,
  strutils,
  os,

  debug_log,
  interpreter,
  preprocessor,
  sexpr_parser

proc main(args: seq[string]) =
  var filename: string
  for arg in args:
    if arg == "--debug":
      debugLoggingEnabled = true
    elif arg == "--profile":
      isProfiling = true
    elif arg == "--pretty-sexpr":
      sexprPrettyPrint = true
    else:
      filename = arg
  assert filename != "", "Must specify a filename"

  let contents = open(filename).readAll()
  var module = parseSexprs(contents)
  debugLog "Parsed as:"
  debugLogModule(module)

  debugLog "\n---===---\n"

  preprocess(module, filename)
  debugLog "Preprocessed as:"
  debugLogModule(module)

  debugLog "\n---===---\n"

  debugLog "Output:"
  interpret(module)

  if isProfiling:
    echo "\n---===---\n"
    echo "Profile:"
    printProfileData()

when isMainModule:
  main(commandLineParams())
