import
  algorithm,
  hashes,
  sequtils,
  strutils,
  tables,
  times,

  sexpr_parser

type
  Func* = object
    name*: string
    args*: seq[string]
    body*: seq[Sexpr]
  ValueKind* = enum
    vVoid
    vBool
    vInt
    vChar
    vStr
    vList
    vFunc
    vDict
    vObj
    vVal
  Value* = ref ValueObj
  ValueObj* = object
    case kind*: ValueKind
    of vVoid:
      discard
    of vBool:
      boolVal*: bool
    of vInt:
      intVal*: int
    of vChar:
      charVal*: char
    of vStr:
      strVal*: string
    of vList:
      listVal*: seq[Value]
    of vFunc:
      fn*: Func
    of vDict:
      dictVal*: TableRef[ValueObj, Value]
    of vObj:
      objVal*: Scope
    of vVal:
      valVal*: ptr Value
  Scope* = object
    vals*: TableRef[string, Value]
  Scopes* = seq[Scope]

var
  isProfiling* = false
  profileData: Table[string, float] = initTable[string, float]()

proc hash(x: Value): Hash
proc hash(x: ValueObj): Hash =
  case x.kind
  of vInt: x.intVal.hash
  of vChar: x.charVal.hash
  of vStr: x.strVal.hash
  of vList: x.listVal.hash
  of vVal: x.valVal.hash
  else:
    assert false, "No hash support for value of kind: " & $x.kind
    0.Hash

proc hash(x: Value): Hash =
  hash(x[])

proc `==`(a, b: ValueObj): bool =
  if a.kind != b.kind:
    return false
  return case a.kind
  of vBool: a.boolVal == b.boolVal
  of vInt: a.intVal == b.intVal
  of vChar: a.charVal == b.charVal
  of vStr: a.strVal == b.strVal
  of vVal: a.valVal == b.valVal
  of vList: a.listVal == b.listVal
  else:
    assert false, "Can't compare values of kind: " & $a.kind
    false

proc `$`*(val: Value): string
proc `$`*(val: ValueObj): string =
  case val.kind
  of vInt:
    result = $val.intVal
  of vChar:
    result = $val.charVal
  of vStr:
    result = $val.strVal
  of vList:
    result = $"["
    for i in 0..<val.listVal.len:
      if i > 0:
        result &= $", "
      result &= $val.listVal[i]
    result &= $"]"
  of vBool:
    result = $val.boolVal
  of vFunc:
    result = $val.fn.name
  of vDict:
    result = "{"
    var comma = false
    for k, v in val.dictVal:
      if comma:
        result &= ", "
      result &= $k & ": " & $v
      comma = true
    result &= "}"
  of vVal:
    result = $cast[int](val.valVal)
  else:
    assert false, "Can't print " & $val.kind
proc `$`*(val: Value): string =
  $val[]

func newScope*(): Scope =
  Scope(vals: newTable[string, Value]())

proc printScope*(scopes: Scopes) =
  echo "scopes:"
  for i in 0..<scopes.len:
    var line = ""
    for item in scopes[scopes.len - 1 - i].vals.keys:
      line &= item & ", "
    echo "  [", line, "]"

proc push(scopes: var Scopes) =
  scopes.add newScope()

proc pop(scopes: var Scopes) =
  scopes.del(scopes.len - 1)

proc hasVal(scopes: Scopes, name: string): bool =
  for i in 0..<scopes.len:
    if scopes[scopes.len - i - 1].vals.hasKey(name):
      return true
  false

proc get*(scopes: var Scopes, name: string): var Value =
  for i in 0..<scopes.len:
    if scopes[scopes.len - i - 1].vals.hasKey(name):
      return scopes[scopes.len - i - 1].vals[name]
  printScope(scopes)
  assert false, "Can't get '" & name & "'"

proc set*(scopes: var Scopes, name: string, val: Value) =
  for i in 0..<scopes.len:
    if scopes[scopes.len - i - 1].vals.hasKey(name):
      scopes[scopes.len - i - 1].vals[name] = val
      return
  printScope(scopes)
  assert false, "Can't set '" & name & "'"

proc declare*(scopes: var Scopes, name: string, val: Value) =
  assert(not scopes[scopes.len - 1].vals.hasKey(name), "Can't re-declare " & name)
  scopes[scopes.len - 1].vals[name] = val

func isListWithStart*(sexp: Sexpr, toFind: string): bool =
  sexp.kind == sList and sexp.elems.len > 0 and sexp.elems[0].kind == sAtom and sexp.elems[0].name == toFind

proc sexpToFunc(sexp: Sexpr): Func =
  assert sexp.elems[0].name == "fn"
  let
    hasName = sexp.elems[1].kind == sAtom
    name = if hasName: sexp.elems[1].name else: ""
    nameIdx = if hasName: 1 else: 0
    args = sexp.elems[1 + nameIdx].elems.mapIt(it.name)
    body = sexp.elems[2 + nameIdx].elems
  Func(name: name, args: args, body: body)

proc findFuncs*(module: seq[Sexpr]): Scope =
  result = newScope()
  for sexp in module:
    if sexp.isListWithStart("fn"):
      let fn = sexpToFunc(sexp)
      result.vals[fn.name] = Value(kind: vFunc, fn: fn)

proc printProfileData*() =
  var data: seq[tuple[name: string, time: float]]
  for pair in profileData.pairs:
    data.add pair
  data = data.sortedByIt(-it.time)
  for pair in data:
    echo pair.name, ": ", pair.time, "s"

proc topObjVal*(scopes: Scopes): Value =
  Value(kind: vObj, objVal: scopes[scopes.len - 1])

proc execute*(sexp: Sexpr, scopes: var Scopes): Value

const callRetName = "__retval"
var callstack: seq[string]
proc call*(fn: Func, args: seq[Value], scopes: var Scopes): Value =
  # echo "Calling ", fn.name, " with args = ", args
  var startTime: float
  if isProfiling:
    startTime = cpuTime()
  callstack.add fn.name
  try:
    let funcs = fn.body.findFuncs()
    scopes.add(funcs)
    scopes.declare(callRetName, Value(kind: vVoid))
    assert args.len == fn.args.len, "Mismatched argument lengths for fn: " & fn.name
    for i in 0..<fn.args.len:
      scopes.declare(fn.args[i], args[i])
    for s in fn.body:
      discard execute(s, scopes)
    result = scopes.get(callRetName)
    scopes.pop()
  except Exception as e:
    echo "Exception thrown in function: ", fn.name
    echo e.msg
    echo "Callstack:"
    for i in 1..<callstack.len:
      echo "  ", callstack[callstack.len - i - 1]
    quit 1
  del callstack, callstack.len - 1
  if isProfiling:
    let elapsed = cpuTime() - startTime
    profileData[fn.name] = elapsed + profileData.getOrDefault(fn.name)

var debugTraceExecution = false
proc execute*(sexp: Sexpr, scopes: var Scopes): Value =
  if scopes.hasVal(callRetName) and scopes.get(callRetName).kind != vVoid:
    return
  if sexp.kind == sList and sexp.elems.len == 0:
    return
  if debugTraceExecution:
    echo "Executing ", sexp
  if sexp.kind == sAtom:
    let name = sexp.name
    if name[0] == '\'':
      result = Value(kind: vChar, charVal: name[1])
    elif name[0] == '"':
      result = Value(kind: vStr, strVal: name[1..^2])
    elif name[0].isDigit() or name.len > 1 and name[0] == '-' and name[1].isDigit():
      result = Value(kind: vInt, intVal: parseInt(name))
    elif name == "true":
      result = Value(kind: vBool, boolVal: true)
    elif name == "false":
      result = Value(kind: vBool, boolVal: false)
    else:
      result = scopes.get(name)
  else:
    let elems = sexp.elems
    let name = if elems[0].kind == sAtom: elems[0].name else: ""
    case name
    of "fn":
      result = Value(kind: vFunc, fn: sexpToFunc(sexp))
    of "if":
      let cond = elems[1].execute(scopes)
      assert cond.kind == vBool
      if cond.boolVal:
        let ifTrue = elems[2].elems
        scopes.add ifTrue.findFuncs()
        for s in ifTrue:
          discard execute(s, scopes)
        scopes.pop()
      elif elems.len > 3:
        let ifFalse = elems[4].elems
        scopes.add ifFalse.findFuncs()
        for s in ifFalse:
          discard execute(s, scopes)
        scopes.pop()
    of "while":
      let
        cond = elems[1]
        body = elems[2].elems
      while not (scopes.hasVal(callRetName) and scopes.get(callRetName).kind != vVoid) and cond.execute(scopes).boolVal:
        let funcs = body.findFuncs()
        scopes.add(funcs)
        for s in body:
          discard s.execute(scopes)
        scopes.pop()
    of "var":
      let
        name = elems[1].name
        val = elems[3].execute(scopes)
      scopes.declare(name, val)
    of "<":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left.intVal < right.intVal)
    of ">":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left.intVal > right.intVal)
    of "<=":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left.charVal <= right.charVal)
    of ">=":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left.charVal >= right.charVal)
    of "==":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left[] == right[])
    of "!=":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vBool, boolVal: left[] != right[])
    of "and":
      let left = execute(elems[1], scopes)
      if not left.boolVal:
        result = Value(kind: vBool, boolVal: false)
      else:
        let right = execute(elems[2], scopes)
        result = Value(kind: vBool, boolVal: right.boolVal)
    of "or":
      let left = execute(elems[1], scopes)
      if left.boolVal:
        result = Value(kind: vBool, boolVal: true)
      else:
        let right = execute(elems[2], scopes)
        result = Value(kind: vBool, boolVal: right.boolVal)
    of "not":
      let val = execute(elems[1], scopes)
      result = Value(kind: vBool, boolVal: not val.boolVal)
    of "+":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      if left.kind == vInt:
        result = Value(kind: vInt, intVal: left.intVal + right.intVal)
      elif left.kind == vList:
        result = Value(kind: vList, listVal: left.listVal & right.listVal)
      elif left.kind == vStr:
        let rightStr =
          if right.kind == vStr:
            right.strVal
          elif right.kind == vChar:
            $right.charVal
          else:
            $right
        result = Value(kind: vStr, strVal: left.strVal & rightStr)
    of "-":
      let
        left = execute(elems[1], scopes)
        right = execute(elems[2], scopes)
      result = Value(kind: vInt, intVal: left.intVal - right.intVal)
    of "__list":
      let vals = elems[1..^1].mapIt(it.execute(scopes))
      result = Value(kind: vList, listVal: vals)
    of "dict":
      var dictVal = newTable[ValueObj, Value]()
      for pair in elems[1].elems:
        let
          k = pair.elems[0].execute(scopes)
          v = pair.elems[1].execute(scopes)
        dictVal[k[]] = v
      result = Value(kind: vDict, dictVal: dictVal)
    of "object":
      var objScope = newScope()
      for pair in elems[1].elems:
        let
          k = pair.elems[0].name
          v = pair.elems[1].execute(scopes)
        objScope.vals[k] = v
      result = Value(kind: vObj, objVal: objScope)
    of ".":
      let
        obj = elems[1].execute(scopes)
        prop = elems[2].name
      result = obj.objVal.vals[prop]
    of "isList":
      let val = elems[1].execute(scopes)
      result = Value(kind: vBool, boolVal: val.kind == vList)
    of "len":
      let list = elems[1].execute(scopes)
      if list.kind == vList:
        result = Value(kind: vInt, intVal: list.listVal.len)
      else:
        result = Value(kind: vInt, intVal: list.strVal.len)
    of "add":
      let
        cur = elems[1].execute(scopes)
        val = elems[2].execute(scopes)
      cur.listVal.add val
    of "del":
      let
        cur = elems[1].execute(scopes)
        idx = elems[2].execute(scopes)
      cur.listVal.delete idx.intVal
    of "__index":
      let
        list = elems[1].execute(scopes)
        idx = elems[2].execute(scopes)
      if list.kind == vList:
        result = list.listVal[idx.intVal]
      elif list.kind == vStr:
        result = Value(kind: vChar, charVal: list.strVal[idx.intVal])
      elif list.kind == vDict:
        result = list.dictVal[idx[]]
      else:
        assert false, "Can't index val of kind: " & $list.kind
    of "&":
      let val = elems[1].execute(scopes)
      result = Value(kind: vVal, valVal: cast[ptr Value](val))
    of "*":
      if elems.len == 2:
        # Unary
        let ptrVal = elems[1].execute(scopes)
        result = cast[Value](ptrVal.valVal)
      else:
        let
          left = execute(elems[1], scopes)
          right = execute(elems[2], scopes)
        result = Value(kind: vInt, intVal: left.intVal * right.intVal)
    of "set":
      let
        dict = elems[1].execute(scopes)
        key = elems[2].execute(scopes)
        val = elems[3].execute(scopes)
      dict.dictVal[key[]] = val
    of "hasKey":
      let
        dict = elems[1].execute(scopes)
        key = elems[2].execute(scopes)
      result = Value(kind: vBool, boolVal: dict.dictVal.hasKey(key[]))
    of "<-":
      let
        obj = elems[1].execute(scopes)
        val = elems[2].execute(scopes)
      obj[] = val[]
    of "toStr":
      let val = elems[1].execute(scopes)
      result = Value(kind: vStr, strVal: $val.intVal)
    of "print":
      for i in 1..<elems.len:
        let val = execute(elems[i], scopes)
        write(stdout, $val)
      write(stdout, "\n")
    of "return":
      if elems.len > 1:
        scopes.set(callRetName, elems[1].execute(scopes))
      else:
        scopes.set(callRetName, Value(kind: vStr, strVal: "(void return)"))
    of "exit":
      let val = elems[1].execute(scopes)
      quit(val.intVal)
    of "setDebugTrace":
      let val = elems[1].execute(scopes)
      debugTraceExecution = val.boolVal
    else:
      # Function call
      let val = elems[0].execute(scopes)
      assert val.kind == vFunc, "Failed to execute: " & $sexp
      let
        fn = val.fn
        args = elems[1..^1].mapIt(it.execute(scopes))
      result = fn.call(args, scopes)

proc interpret*(module: seq[Sexpr]) =
  var scopes = @[findFuncs(module)]
  for sexp in module:
    discard sexp.execute(scopes)
