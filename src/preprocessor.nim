import
  os,
  ospaths,
  sequtils,
  sets,
  tables,

  debug_log,
  interpreter,
  sexpr_parser

type
  Rule = object
    name: string
    arg: string
    rules: seq[Sexpr]
    scopes: Scopes
    passVal: Value

proc toValue(sexpr: Sexpr): Value =
  case sexpr.kind
  of sAtom:
    Value(kind: vStr, strVal: sexpr.name)
  of sList:
    Value(kind: vList, listVal: sexpr.elems.mapIt(it.toValue()))

proc toSexpr(val: Value): Sexpr =
  case val.kind
  of vStr:
    return Sexpr(kind: sAtom, name: val.strVal)
  of vList:
    return Sexpr(kind: sList, elems: val.listVal.mapIt(toSexpr(it)))
  else:
    assert false, "Can't rewrite val of kind: " & $val.kind

proc runPass(module: var seq[Value], pass: var Rule, oldPasses: var seq[Rule]) =
  var otherPasses = oldPasses
  proc tryRun(pass: var Rule, rule: Sexpr, sexpr: var Value): bool =
    pass.scopes.set(pass.arg, sexpr)
    let
      cond = rule.elems[1]
      condVal = cond.execute(pass.scopes)
    assert condVal.kind == vBool, "Rule condition must return boolean"
    if condVal.boolVal:
      let
        body = Func(
          name: pass.name & "_body",
          args: @[pass.arg],
          body: rule.elems[2].elems,
        )
        bodyVal = body.call(@[sexpr], pass.scopes)
      if bodyVal.kind == vVoid:
        # Don't modify the sexpr or rerun old passes for a void-return.
        # This signifies the rule is an analysis-only pass.
        return false
      # Rewrite the sexp "in-place" to preserve ref equality.
      sexpr[] = bodyVal[]
    return condVal.boolVal
  proc dfs(pass: var Rule, rule: Sexpr, sexpr: var Value, canRerun: bool) =
    var didApply = false
    if sexpr.kind != vList:
      didApply = pass.tryRun(rule, sexpr)
    else:
      for child in sexpr.listVal.mitems:
        pass.dfs(rule, child, canRerun = canRerun)
      didApply = pass.tryRun(rule, sexpr)
    if didApply and canRerun:
      for old in otherPasses.mitems:
        debugLog "  rerunning ", old.name, " on ", sexpr.toSexpr()
        for r in old.rules:
          old.dfs(r, sexpr, canRerun = false)
  for rule in pass.rules:
    for sexp in module.mitems:
      pass.dfs(rule, sexp, canRerun = true)
  oldPasses = otherPasses

proc preprocess*(module: var seq[Sexpr], sourceFilename: string) =
  # File imports
  var
    i = 0
    included = initSet[string]()
  let
    shivcPath = getAppFilename()
    systemInclude = shivcPath.parentDir().parentDir() / "include"
    sourceDir = sourceFilename.parentDir()
    includeDirs = @[sourceDir, systemInclude]
  while i < module.len:
    let sexp = module[i]
    i += 1
    if sexp.isListWithStart("include"):
      module.delete(i - 1)

      let filename = sexp.elems[1].name & ".shv"
      var filePath: string
      for dir in includeDirs:
        filePath = dir / filename
        if existsFile(filePath):
          break
      assert existsFile(filePath), "File " & filePath & " not found"
      if filePath notin included:
        included.incl(filePath)
        let
          contents = open(filePath).readAll()
          sexprs = parseSexprs(contents)
        module = module[0..<i-1] & sexprs & module[i-1..^1]

        debugLog "After importing ", filePath, ":"
        debugLogModule module
      i = 0

  debugLog "\n---===---\n"

  # Read/execute passes
  var valueModule = module.mapIt(it.toValue())
  var passValues = newTable[string, Value]()
  var allPasses: seq[Rule]
  i = 0
  while i < valueModule.len:
    let sexp = valueModule[i].toSexpr()
    i += 1
    if sexp.isListWithStart("rule"):
      let passName = sexp.elems[1].name
      debugLog "Running rule: ", passName
      valueModule.delete(i - 1)
      i = 0

      let
        argName = sexp.elems[2].elems[0].name
        bodyElems = sexp.elems[3].elems
      var rules: seq[Sexpr]
      var scopes = @[findFuncs(module), findFuncs(bodyElems)]
      scopes.declare(argName, Value())
      var passVal = Value(kind: vObj, objVal: newScope())
      scopes.declare(passName, passVal)
      for child in bodyElems:
        if child.isListWithStart("when"):
          rules.add child
        elif child.isListWithStart("var"):
          let
            name = child.elems[1].name
            val = child.elems[3].execute(scopes)
          passVal.objVal.vals[name] = val
          # scopes.declare(name, val)
        elif child.isListWithStart("require"):
          let name = child.elems[1].name
          for rule in allPasses:
            if rule.name == name:
              scopes.declare(name, rule.passVal)
              break
      var pass = Rule(
        name: passName,
        arg: argName,
        rules: rules,
        scopes: scopes,
        passVal: passVal,
      )
      valueModule.runPass(pass, allPasses)
      allPasses.add pass
      module = valueModule.mapIt(it.toSexpr())
      debugLogModule module

    elif sexp.isListWithStart("pass"):
      debugLog "Running pass: ", sexp.elems[1]
      module.delete(i - 1)
      i = 0

      let
        argNames = sexp.elems[2].elems.mapIt(it.name)
        body = sexp.elems[3].elems
        fn = Func(name: sexp.elems[1].name & "_body", args: argNames, body: body)
        args = @[Sexpr(kind: sList, elems: module).toValue()]
      var scopes = @[findFuncs(module)]
      let ret = fn.call(args, scopes).toSexpr()
      module = ret.elems
      valueModule = module.mapIt(it.toValue())
      debugLogModule(module)
