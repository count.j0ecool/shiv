import
  sequtils,
  strformat,
  strutils,

  debug_log,
  util

type
  SexprKind* = enum
    sAtom
    sList
  Sexpr* = object
    case kind*: SexprKind
    of sAtom:
      name*: string
    of sList:
      elems*: seq[Sexpr]
  ParenKind = enum
    pParen
    pCurly
    pSquare
    pIndex

proc `==`*(a, b: Sexpr): bool =
  if a.kind != b.kind:
    return false
  case a.kind
  of sAtom:
    a.name == b.name
  of sList:
    a.elems == b.elems

proc `*`(str: string, num: int): string =
  for i in 0..<num:
    result &= str
proc `*`(num: int, str: string): string =
  str * num

var sexprPrettyPrint* = false
proc toStr(s: Sexpr, indent: int): string =
  case s.kind
  of sList:
    let sepStr = if not sexprPrettyPrint: " " else: "\n" & indent * "  "
    let contents = s.elems.mapIt(toStr(it, indent + 1)).join(sepStr)
    result = &"({contents})"
  of sAtom:
    if (' ' in s.name and s.name[0] != '"') or s.name.len == 0:
      result = &"`{s.name}`"
    else:
      # Escape any special chars (e.g. non-space whitespace)
      for c in s.name:
        case c
        of '\n':
          result &= "\\n"
        else:
          result &= c
proc `$`*(s: Sexpr): string =
  toStr(s, 0)

func s(elems: seq[Sexpr]): Sexpr =
  Sexpr(kind: sList, elems: elems)
func a(name: string): Sexpr =
  Sexpr(kind: sAtom, name: name)

proc top[T](stack: var seq[T]): var T =
  stack[stack.len - 1]
proc push[T](stack: var seq[T], item: T) =
  stack.add(item)
proc pop[T](stack: var seq[T]): T =
  let t = stack.top
  stack.del(stack.len - 1)
  t

proc parseSexprs*(input: string): seq[Sexpr] =
  var
    i = 0
    stack: seq[seq[Sexpr]] = @[newSeq[Sexpr](), newSeq[Sexpr]()]
    parenStack: seq[ParenKind] = @[]
    str = ""
    isStrLit = false
    commentKind = '\0'
    wasSep = true
    wasSymbol = false

  proc flatTop(): bool =
    parenStack.len == 0 or parenStack.top == pCurly
  proc sep =
    if str.len > 0:
      stack.top.add a(str)
      str = ""
  proc endSexp(): seq[Sexpr] =
    sep()
    stack.pop()
  proc handleNewline() =
    let t = endSexp()
    if t.len == 1 and t[0].kind == sList:
      stack.top.add t[0]
    elif t.len > 0:
      stack.top.add s(t)
  proc readChar(c: char): char =
    if c != '\\':
      return c
    let next = input[i + 1]
    i += 1
    case next
    of '"', '\\':
      result = next
    of 'n':
      result = '\n'
    of 'r':
      result = '\r'
    of '0':
      result = '\0'
    else:
      assert false, "Unknown escape character: \\" & next

  while i < input.len:
    let c = input[i]
    if isStrLit:
      if c != '"':
        str &= readChar(c)
      else:
        isStrLit = false
        str = '"' & str & '"'
        sep()
      i += 1
      continue

    case commentKind
    of ';':
      if c == '\n':
        commentKind = '\0'
      else:
        i += 1
        continue
    of '(':
      if i + 1 < input.len and input[i] == ';' and input[i + 1] == ')':
        commentKind = '\0'
        i += 1
      i += 1
      continue
    of '{':
      if i + 1 < input.len and input[i] == ';' and input[i + 1] == '}':
        commentKind = '\0'
        i += 1
      i += 1
      continue
    else:
      discard

    case c
    of ' ':
      sep()
      wasSep = true
    of '\r':
      discard
    of '\n':
      sep()
      wasSep = true
      if flatTop():
        handleNewline()
        stack.push @[]
    of '"':
      sep()
      isStrLit = true
      wasSep = false
    of '\'':
      sep()
      i += 1
      stack.top.add(a("\'" & readChar(input[i])))
    of ';':
      sep()
      commentKind = ';'
      if i + 1 < input.len:
        if input[i + 1] == '(':
          commentKind = '('
          i += 1
        elif input[i + 1] == '{':
          commentKind = '{'
          i += 1
    of '(':
      sep()
      stack.push @[]
      parenStack.push pParen
      wasSep = true
    of ')':
      assert parenStack.len > 0 and parenStack.top == pParen, "Mismatched parens"
      discard parenStack.pop()
      let t = endSexp()
      stack.top.add s(t)
      wasSep = false
    of '{':
      sep()
      stack.push @[]
      stack.push @[]
      parenStack.push pCurly
      wasSep = true
    of '}':
      assert parenStack.len > 0 and parenStack.top == pCurly, "Mismatched curly braces"
      discard parenStack.pop()
      let t = endSexp()
      if t.len > 0:
        stack.top.add s(t)
      let v = endSexp()
      stack.top.add s(v)
      wasSep = false
    of '[':
      if not wasSep:
        sep()
        parenStack.push pIndex
      else:
        parenStack.push pSquare
      stack.push @[]
      wasSep = true
    of ']':
      assert parenStack.len > 0 and parenStack.top in @[pSquare, pIndex], "Mismatched square brackets"
      let isIndex = parenStack.pop() == pIndex
      let t = endSexp()
      if isIndex:
        let
          prev = stack.top.top
          idx = if t.len == 1: t[0] else: s(t)
        stack.top.top() = s(@[a("__index"), prev, idx])
      else:
        stack.top.add s(@[a("__list")] & t)
      wasSep = false
    else:
      wasSep = false
      let isSymbol = c in "!@#$%^&*~`+-*/?<>=|.,:"
      if str == "" and c == '-' and input[i+1].isDigit():
        # Negative literals need to be handled specially
        str = "-"
        i += 1
        wasSymbol = false
        continue
      if isSymbol != wasSymbol:
        sep()
      str &= c
      wasSymbol = isSymbol
    i += 1
  handleNewline()
  assert stack.len == 1
  stack.top

proc debugLogModule*(module: seq[Sexpr]) =
  for sexpr in module:
    debugLog(sexpr)
