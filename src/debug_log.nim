var debugLoggingEnabled* = false
proc debugLog*(args: varargs[string, `$`]) =
  if debugLoggingEnabled:
    for x in args:
      stdout.write(x)
    stdout.write("\n")
